#include <defs.h>
#include <sys/pages.h>
#include <sys/slob_allocator.h>

void test_page_table() {

	printf("address of pml4 is %lx\n",*((uint64_t*)0xffffFF7FBFDFEFF0));
	printf("address of pdp (pml4 entry 511) %lx\n",*((uint64_t*)0xffffFF7FBFDFEFF8));
	printf("address of pd (pdp entry 510) %lx\n",*((uint64_t*)0xffffFF7FBFDFFFF0));
	printf("address of pt (pd entry 1) %lx\n",*((uint64_t*)0xffffFF7FBFFFE008));
	printf("address of page frame (pt entry 0) %lx\n",*((uint64_t*)0xffffFF7FFFC01000));
	map((uint64_t*)0x0,(uint64_t*)0xffffffff80000000,0);

}

int testPageTable()
{
printf("test page table starts\n");
    test_page_table();
printf("test page table ends\n");
    return NULL;
}

