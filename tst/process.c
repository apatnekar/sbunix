#include <defs.h>
#include <sys/process.h>
#include <sys/kprintf.h>
#include <sys//interrupt.h>
#include <sys/pages.h>
#include <sys/kern_macros.h>
#include <sys/slob_allocator.h>
#include <sys/virt_mem_mgr.h>
#include <syscall.h>
#include <sys/elf.h>
#include <sys/loader.h>
#include <sys/gdt.h>
// The start of the task linked list.
volatile task_t *ready_queue;

// The next available process ID.
uint64_t next_pid = 1;

// Some externs are needed to access members in paging.c...
//extern page_directory_t *kernel_directory;
//extern page_directory_t *current_directory;

struct task_struct *head;
void sched_yield();
void initialise_tasking()
{
    // Rather important stuff happening, no interrupts please!
    asm volatile("cli");

    // Relocate the stack so we know where it is.
    //move_stack((void*)0xE0000000, 0x2000);

    // Initialise the first task (kernel task)
    current_task = ready_queue = (task_t*)get_free_pages(1);
    current_task->id = next_pid++;
    current_task->rsp = current_task->rbp = 0;
    current_task->rsp = (uint64_t)((char *)task + STACK_SIZE);
    current_task->rip = (uint64_t)&f;
    //current_task->page_directory = current_directory;
    current_task->next = 0;
    //current_task->kernel_stack = kmalloc_a(KERNEL_STACK_SIZE);

    // Reenable interrupts.
    asm volatile("sti");
}

struct task_struct* create_process(char* filename){
	struct task_struct* ts = load_elf(filename);
	current_task = ts;
	uint64_t* pml4 = get_zeroed_page();        
	int i;
	for(i=0;i<512;i++)
		pml4[i] = kern_pgdir[i];
	
	uint64_t phys_pml4 = (uint64_t)pa(pml4); // TO DO: cr3 needs to be written
	phys_pml4 = phys_pml4  & ~0xFFF;
	ts->pml4 = phys_pml4;
	printf("physical pml4 is %lx\n",phys_pml4);
	pml4[510] = phys_pml4 | PT_P | PT_W;
        ts->kernel_stack = (uint64_t)get_free_pages(1) + 2*PAGE_SIZE - 16;
	return ts;
}

void switch_to_process(struct task_struct* ts_obj){
	printf("inside switch to \n");
	set_kernel_stack(ts_obj->kernel_stack);
	uint64_t pml4 = ts_obj->pml4 & ~0xFFF;
	printf("pml4 is %lx\n",pml4);
	ts_obj->rsp = ts_obj->rsp - 16;
	__asm__ __volatile__ (
		"movq %2, %%cr3;"	/* load cr3 with current process's pml4 */
                "movq %0, %%rsp;"       /* load next->stack var in rsp */
                "pushq %1;"             /* restore RIP so as to continue execution in next task*/
                "retq;"                 /* Switch to new task */
                :: "r"(&(ts_obj->rsp)), "r"(ts_obj->rip), "r"(pml4)
        );
	printf("reached switch to end");	
}

void switch_to_user_mode(struct task_struct *ts_obj) {

	printf("inside switch to user mode\n");
	set_kernel_stack(ts_obj->kernel_stack);
	uint64_t pml4 = ts_obj->pml4 & ~0xFFF;
	LOG("pml4 is %lx\n",pml4);
	ts_obj->rsp = ts_obj->rsp - 16;
	
	__asm__ __volatile__ (
		"cli;"
		"movq %0, %%cr3;"
		"mov $0x23, %%ax;"
		"mov %%ax, %%ds;"
		"mov %%ax, %%es;"
		"mov %%ax, %%fs;"
		"mov %%ax, %%gs;"

		"movq %1, %%rax;"
		"pushq $0x23;"
		"pushq %%rax;"
		"pushfq;"
		"popq %%rax;"
		"orq $0x200, %%rax;"
		"pushq %%rax;"
		"pushq $0x1B;"
		"pushq %2;"
		"iretq;"
		::"r"(pml4), "r"(&(ts_obj->rsp)), "r"(ts_obj->rip)
	);
}

int getpid(){
    return current_task->id;
}

void dummy() {
    task = (struct task_struct *)get_free_pages(1);          //malloc(sizeof(struct task_struct *), 2);
    head = task;
    //task->fun_addr = &f;
    task->rip = (uint64_t)&f;
    task->rsp = (uint64_t)((char *)task + STACK_SIZE);
    task->next = (struct task_struct *)get_free_pages(1);    //kmalloc(sizeof(struct task_struct *), 2);
    task = task->next;
    //task->fun_addr = &g;
    task->rip = (uint64_t)&g;
    task->rsp = (uint64_t)((char *)task + STACK_SIZE);   //(uint64_t *)&task->ps_stack[STACK_SIZE];   
    task->next = (struct task_struct *)get_free_pages(1);
    task = task->next;
    //task->fun_addr = &h;
    task->rip = (uint64_t)&h;
    task->rsp = (uint64_t)((char *)task + STACK_SIZE);   //(uint64_t *)&task->ps_stack[STACK_SIZE];
    task->next = head;
    task = task->next;
}

void f() {
    printf("hello ");
//    schedule();
    sched_yield();
    printf("-I am fine, thank you\n");
    sched_yield();
//	schedule();
    colorConsole = make_color(COLOR_RED, COLOR_BLACK);
    printf("\n\nSystem Haulting!!");
    colorConsole = make_color(COLOR_LIGHT_GREEN, COLOR_BLACK);
    while(1);
}

void g() {
    printf("world - ");
    sched_yield();
//schedule();
    printf("How do you do?\n");
    sched_yield();
//schedule();
}

void h() {
    printf("how r u ?\n");
    sched_yield();
//schedule();
    printf("Scooby Dooby Doo..   ;p\n");
    sched_yield();
//schedule();
}

void init_context_switch() {
	__asm__ __volatile__ (
		//"movq %%rsp, %0;"
		"movq %0, %%rsp;"
		"pushq %1;"
		"retq;"
		://"=g"(task->ptr_ps_stack)
		:"r"(task->rsp), "r"(task->rip)
	);
}

void switch_to(struct task_struct *next, struct task_struct *prev) {
printf("switch_to\n");
	__asm__ __volatile__ (
		"movq %%rsp, (%1);"     /* save RSP */
		"movq $1f,%0;"          /* save RIP */
		"movq %2, %%rsp;"	/* load next->stack var in rsp */         
                "pushq %3;"             /* restore RIP so as to continue execution in next task*/       
                "retq;"                 /* Switch to new task */
                "1:\t"                  /* first instruction executed after Switch */    		
		:"=g"(prev->rip)
		: "r"(&prev->rsp), "r"(next->rsp), "r"(next->rip)
	);
printf("switch_to - should not be here\n");
}

void sched_yield(){
    __syscall0(0);
}

void schedule() {
printf("inside schedule\n");
 //   struct task_struct *prev = task; 
  //  task = task->next;
//    switch_to(task,prev);
printf("inside schedule - should not be here\n");
}

