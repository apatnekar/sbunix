#include <defs.h>
#include <sys/memory.h>
#include <sys/kprintf.h>
#include <sys/pages.h>
#include <sys/kern_macros.h>

void _x86_64_asm_paging(unsigned long *swapper_page_dir);

uint64_t virt_to_phys_address(uint64_t va){
    return (va - KERNMEM + PHYSBASE);
}

uint64_t phys_to_virt_address(uint64_t pa){
    return (pa + KERNMEM - PHYSBASE);
}

void init_kernel_page_table(void *physbase, void *physfree) {
/*	unsigned int i=0;

	uint64_t *page_table = (uint64_t *)KERN_PT_ADDR_0;
	uint64_t page_address = (uint64_t )physbase;
	for(i=0;i<32;i++) {
		page_table[i] = page_address | PT_W | PT_P;
		page_address += PAGE_SIZE;
	}
	uint64_t *page_middle_dir = (uint64_t *)KERN_PMD_ADDR_0;
	page_middle_dir[1] = PHYS_PT_ADDR | PMD_READ_WRITE | PMD_P;   

	uint64_t *page_upper_dir = (uint64_t *)KERN_PUD_ADDR_0;
	page_upper_dir[0]   = PHYS_PMD_ADDR | PUD_READ_WRITE | PUD_P; 
	page_upper_dir[510] = PHYS_PMD_ADDR | PUD_READ_WRITE | PUD_P; 

	uint64_t *page_global_dir = (uint64_t *)KERN_PGD_ADDR;
	page_global_dir[0] 	 = PHYS_PUD_ADDR | PGD_READ_WRITE | PGD_P; 
	page_global_dir[511] = PHYS_PUD_ADDR | PGD_READ_WRITE | PGD_P; 

	page_middle_dir[0] = VIDEO_BUFFER_PT_ADDR | PMD_READ_WRITE | PMD_P;
	page_table = (uint64_t *)VIDEO_BUFFER_VIRT_PT_ADDR;
	page_address = VIDEO_BUFFER;
	page_table[0xb8] = page_address | PT_W | PT_P;
	
	swapper_page_dir = (uint64_t)PHYS_PGD_ADDR;

	__asm__ volatile ("movq %0, %%cr3" : : "r"(swapper_page_dir));*/
}


// dest = Virtual Address, n = bytes
void memset(void *dest, int init_with, size_t n)
{
	if (n == 0)
		return;
	/* The below stores the value in fill_value n times to the pointer dest*/
	if ((uint64_t)dest%4 == 0 && n%4 == 0) {
		init_with &= 0xFF;
		init_with = (init_with<<24)|(init_with<<16)|(init_with<<8)|init_with;
		asm volatile("cld; rep stosl\n"
			:: "D" (dest), "a" (init_with), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
			:: "D" (dest), "a" (init_with), "c" (n)
			: "cc", "memory");
	return;
}
