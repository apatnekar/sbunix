#include <defs.h>
#include <sys/pages.h>
#include <sys/slob_allocator.h>

void test_slob_assign_blocks_big_slob() {
    uint64_t *page_virt_addr = (uint64_t *)boot_alloc( 1 );
    slob_assign_blocks( page_virt_addr, 3025 );
    if(page_virt_addr == list_128_slob_page->page_virt_addr)
        printf("big_list success\n");
}

void test_slob_assign_512_size()
{
    uint64_t *page_virt_addr = (uint64_t *)boot_alloc( 1 );
    slob_assign_blocks( page_virt_addr, 3025 );
    if(page_virt_addr == list_128_slob_page->page_virt_addr)
        printf("list_128_slob_page success\n");
    if(list_128_slob_page->block_bitmap == 0x0)
        printf("list_128_slob_page bitmap is NULL\n");
}

void test_kmalloc()
{
    uint64_t *phys_addr = (uint64_t *)kmalloc(124,1);
    printf("phys_addr: %lx\n", phys_addr);
    phys_addr = (uint64_t *)kmalloc(100,1);
    printf("phys_addr: %lx\n", phys_addr);
    phys_addr = (uint64_t *)kmalloc(1,1);
    printf("phys_addr: %lx\n", phys_addr);
    phys_addr = (uint64_t *)kmalloc(234,1);
    printf("phys_addr: %lx\n", phys_addr);
    phys_addr = (uint64_t *)kmalloc(1234,1);
    printf("phys_addr: %lx\n", phys_addr);
    phys_addr = (uint64_t *)kmalloc(2234,1);
    printf("phys_addr: %lx\n", phys_addr);
    phys_addr = (uint64_t *)kmalloc(274,1);
    printf("phys_addr: %lx\n", phys_addr);
    phys_addr = (uint64_t *)kmalloc(534,1);
    printf("phys_addr: %lx\n", phys_addr);
    phys_addr = (uint64_t *)kmalloc(234,1);
    printf("phys_addr: %lx\n", phys_addr);
    phys_addr = (uint64_t *)kmalloc(234,1);
    printf("phys_addr: %lx\n", phys_addr);

    int *ptr;
    int j=1;        
    for(j=1;j<36;++j){
        ptr = (int*)kmalloc(sizeof(int),2);
        if(ptr==NULL)
	    printf("kmalloc returned NULL\n");
        else {
	//printf("kmalloc succeeded with %p\n",ptr);
	    *ptr = j+1;
	//printf("read value %lx\n",ptr);	
        }
    }
}

int testSlobAllocator()
{
printf("test slob allocator starts\n");
    //test_slob_assign_blocks_big_slob();
    //test_slob_assign_512_size();
    test_kmalloc();
printf("test slob allocator ends\n");
    return NULL;
}

