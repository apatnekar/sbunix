#include <defs.h>
#include <stdio.h>

void testPrintfBasic() {
    printf("testing printf\n");
}

void testPrintfVoid() {
//    printf();
}

void testPrintfChar() {
    printf("test char: %c\n", 'a');
    printf("test char against int: %c\n", 1);
    printf("test char against string: %c\n", "str");
    printf("test char against hex: %c\n", 0xaf);
    printf("test char against uint:%c\n", 2);
    int a = 3;
    printf("test char against pointer: %c\n", &a);
    printf("--------------------------------\n");
}

void testPrintfInteger() {
    printf("test int: %d\n", 2);
    printf("test int against char: %d\n", 'a');
    printf("test int against string: %d\n", "str");
    printf("test int against hex: %d\n", 0xaf);
    printf("test int against uint: %d\n", 2);
    int p = 3;
    printf("test int against pointer: %d\n", &p);
    printf("--------------------------------\n");
}

void testPrintfString() {
    printf("test str: %s\n", "str");
//  printf("test str against char: %s\n", 'a');     // SEGMENTATION FAULT
//  printf("test str against int: %s\n", 2);        // SEGMENTATION FAULT
//  printf("test str against hex: %s\n", 0xaf);     // SEGMENTATION FAULT
//  printf("test str against uint: %s\n", 2);       // SEGMENTATION FAULT
//  int p = 3;
//  printf("test str against pointer: %s\n", &p);   // -------- should not get SEGMENTATION FAULT here, but getting it -------
    printf("--------------------------------\n");
}

void testPrintfHex() {
    printf("test hex: %x\n", 0xaf);
    printf("test hex against char: %x\n", 'a');
    printf("test hex against string: %x\n", "str");
    printf("test hex against int: %x\n", 2);
    printf("test hex against uint: %x\n", 3);
    int p = 3;
    printf("test hex against pointer: %x\n", &p);
    printf("--------------------------------\n");
}

void testPrintfPointer() {
    int p = 3;
    printf("test pointer: %p\n", &p);
    printf("test pointer against char: %p\n", 'a');
    printf("test pointer against int: %p\n", 3);
    printf("test pointer against string: %p\n", "str");
    printf("test pointer against uint: %p\n", 3);
    printf("test pointer against hex: %p\n", 0xaf);
    printf("--------------------------------\n");
}

void testPrintf() {
    testPrintfBasic();
//    testPrintfVoid();
    testPrintfChar();
    testPrintfInteger();
    testPrintfString();
    testPrintfHex();
    testPrintfPointer();
}
