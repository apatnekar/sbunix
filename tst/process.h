#ifndef _PROCESS_H_
#define _PROCESS_H_

#define STACK_SIZE 8192

/* Flags used by VMA */
#define VM_READ         0x0001      /* currently active flags */
#define VM_WRITE        0x0002
#define VM_EXEC         0x0004
#define VM_SHARED       0x0008

struct file{
    uint64_t   file_start;       /* start address of region */
    uint64_t   vm_pgoff;         /* offset in file or NULL */
    uint64_t   vm_sz;            /* region initialised to here */
};

/*  VMA struct */
struct vm_area_struct{
    struct mm_struct *vm_mm;       /* VM area parameters */
    uint64_t vm_start, vm_end;
    unsigned short vm_flags;
    unsigned short grows_down;
    struct file *vm_file;          /* File we map to (can be NULL). */
    struct vm_area_struct *vm_next;
};

/*  task_struct */
typedef struct task_struct
{
    uint64_t id;                // Process ID.
    uint64_t rsp, rip, rbp;   	// Stack, instruction and base pointers.
    uint64_t pml4; 		// Page directory.
    uint64_t kernel_stack;   	// Kernel stack location.
    struct task_struct *next;   // The next task in a linked list.
    struct mm_struct *mm;
} task_t;
struct task_struct *task;

// The currently running task.
volatile task_t *current_task;

/*  mm_struct */
struct mm_struct{
	struct vm_area_struct *mmap, *vm_last;
	uint64_t start_code, end_code, start_data, end_data;
	uint64_t start_brk, brk, start_stack;
	uint64_t arg_start, arg_end, env_start, env_end;
	uint64_t rss, total_vm, locked_vm;
};

// Initialises the tasking system.
void initialise_tasking();
// Called by the timer hook, this changes the running process.
void task_switch();

// Forks the current process, spawning a new one with a different
// memory space.
int fork();

// Causes the current process' stack to be forcibly moved to a new location.
//void move_stack(void *new_stack_start, u32int size);

// Returns the pid of the current process.
int getpid();

void dummy();
void f();
void g();
void h();

void init_context_switch();
void switch_to(struct task_struct *prev, struct task_struct *next);
void schedule();
void switch_to_user_mode(struct task_struct *ts_obj);
struct task_struct* create_process(char* filename);
void switch_to_process(struct task_struct *ts_obj);

#endif
