#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main(int argc,char* argv[]){
	//printf("argc %d \n",argc);
	if(argc==1){
		printf("Argument expected (no of seconds to sleep)\n");
		return 0;
	}

	int sleep_time = atoi(argv[1]); 
	if(sleep_time==-1){
		printf("Invalid argument to sleep, should be positive integer\n");
		return 0;
	}

	sleep(sleep_time);
	return 0;
}
