#include <stdio.h>  // for printf(), perror()
#include <unistd.h>
#include <string.h>

char PATH[64] = "/bin/";
#define IS_ELF 1
#define IS_SCRIPT 2

int resolve_path(volatile char *buffer, volatile char *path);
char *command[] = {
	"exit",
	"ulimit",
	"cd",
	"PATH",
	"echo"
};

int no_of_cmds = 5;

int is_command(char* cmd){
        int i;
	for(i=0;i<no_of_cmds;i++){
		if(strcmp(cmd,command[i])==0){
			return 1;
		}
	}
	return 0;
}

static int start =0 ,end =0;
volatile char read_buf[1024]={};
int read_wrapper(volatile char* buf, int count){
	int no_of_bytes_read = 0;
	int c=0;
	int i;
	if(start==end){
		c = read(0,(char*)read_buf,1024);
		end = c;
		start = 0;	
	}
	if((end-start) <= 0)
		return c;
    else{
//	printf("found something to read start %d end %d\n",start,end);
		no_of_bytes_read = 0;
		for(i=start;i<end;i++){
			//printf("value is %c ",read_buf[i]);
			buf[no_of_bytes_read++] = read_buf[i];
			if(read_buf[i]=='\n'){
//				printf("shd return now");
				buf[no_of_bytes_read]='\0';
				start = i+1;
				return no_of_bytes_read+1;
			}
		}
//		printf("shd reach here later\n");
		// if we reach here means enter is not yet encountered read again
		 c = read(0,(char*)read_buf,1024);
        end = c;
        start = 0;
		for(i=start;i<end;i++){
            buf[no_of_bytes_read++] = read_buf[i];
            if(read_buf[i]=='\n'){
                start = i+1;
                return no_of_bytes_read;
            }
        }

	}
	return -1;	
}

void set_ulimit(int argc, char* argv[]){
	int lim;
	if(argc<=1) {
		printf("-s : stack limit(kbytes), -u : no of processes\n");
		return;
	}
	if(argc==3) {
		lim = atoi(argv[2]);
		if(lim==-1)
			printf("Invalid limit\n");
	}
	if(strcmp(argv[1],"-s")==0 && argc==3) {
		ulimit(U_STACK_LIMIT,lim);
	}
	else if(strcmp(argv[1],"-u")==0 && argc==3) {
		ulimit(U_PROC_LIMIT,lim);
	}
	else {
		printf("Invalid argument to ulimit\n");
	}
}
void export_path(char *path) {
   strcpy(PATH,path);
   printf("Path: %s\n",path);
}

void execute_command(int argc, char*argv[]){
	if(strcmp(argv[0],"ulimit")==0) {
		set_ulimit(argc,argv);
	}
	else if(strcmp(argv[0],"cd")==0) {
		setpwd(argv[1]);
		//if(argv[1] != NULL)
		//	chdir(argv[1]);
	}
	else if(strcmp(argv[0],"PATH")==0) {
        	if(argv[1] == NULL)
	            printf("Please specify PATH to be set\n");
        	else{ 
			
			export_path(argv[1]);
		}
    	}
	else {
		printf("No such command\n");
	}
}
int file_type(fd){
        char c[5];
        if(read(fd,c,1)>0){
                if(c[0]==0x7f){
                        if(read(fd,c,3)>0 ){
                                if(strcmp(c,"ELF")==0)
                                        return IS_ELF;
                        }
                }
                else if(c[0]=='#'){
                        if(read(fd,c,1)>0){
                                if(c[0]=='!'){
                                        return IS_SCRIPT;
                                }
                        }

                }
        }

        return -1;
}

void scriptname(int fd, char* buffer){
        int i;
        char c[5];
        for(i=0;i<64;i++){
                if(read(fd,c,1)>0){
                        if(c[0]=='\n'){
                                buffer[i] = '\0';
                                return;
                        }
                        buffer[i] = c[0];
                }
                else
                        return;
        }

}

int main( int argc1, char* argv1[] )
{
    volatile char  *argv[ 64 ]={0};
    volatile char  command[ 100 ] = { 0 };
    volatile char buffer[64];
    volatile char script[64];
    int f,nbytes;
    int argc = 0;
    int is_bg = 0;
    int pid=-1;
    int i;
    int n;
    int is_correct;
    int fd;
    if(argc1>1){
	//printf("inside shell script\n");
	f = open(argv1[1],1);
	if(f==-1)
	    exit(0);
        	
	dup(f,0);
	read_wrapper( command, sizeof( command ) );
    }
  //  while(1)
  //  printf("******************** inside SHELL ********************\n");
	for(;;){
  		set_fg(getpid());
		// display this shell's command-prompt
    		char  prompt[] = "\nSBUnix $ ";
		//if(argc1==1)
    		write( 1, prompt, sizeof( prompt ) );

    		// accept the user's command-line
    		nbytes = read_wrapper( command, sizeof( command ) );
		//printf("nbytes is %d \n",nbytes);
    		if ( nbytes <= 0 ) 
		{ //printf( "SBUnix" ); 
		exit(0); }
    
		// printf("command was %s\n",command);
    		// parse the user's command-line
    		argc = strtok( (char*)command, (char**)argv, nbytes );
 		for( i=0;i<argc;i++)
			printf("",i,argv[i]);
    
    		if(argc>1){
			if(strcmp((char*)argv[argc-1],"&")==0){
				//printf("Its a background process\n");
	    			is_bg = 1;
	    			argc--;	
			}
    		}
		
    		if(argc>0 && strcmp((char*)argv[0],"exit")==0)
				return 0;
				
		if(argc==0){
			printf("no argument \n");
			//	continue;
    		}
		else if(is_command((char*)argv[0])){
			//printf("its a command \n");
			if(strcmp((char*)argv[0],"PATH")==0 && argv[1]!=NULL){
				strcpy((char*)PATH,(char*)argv[1]);
			//	continue;
			}
			else
			execute_command(argc,(char**)argv);
		}
    		else {
				 
    			argv[argc] = NULL;
    			//printf("before fork\n");
    			// fork a child-process
			is_correct = resolve_path( buffer, argv[0] );	
			
			fd = open((char*)buffer,1);
			n = file_type(fd);  
			//printf("value of n is %d\n",n); 
			if(is_correct<0){
				printf("File not found\n");
				continue;
			}
 			if(n==IS_ELF){
				//printf("its ELF\n");
				//strcpy(buffer,(char*)argv[0]);
				
			}
			else if(n==IS_SCRIPT){
				//printf("its a script");
				strcpy((char*)script,(char*)buffer);
				scriptname(fd,(char*)buffer);
				
				argv[1] = script;
				argv[2] = NULL;
			}
			else{
				printf("Not an Executable\n");
				close(fd);
				continue;
			}	
			close(fd);	
			pid = fork();
//    			printf("Common to both pid: %d, %d\n",pid, getpid());
    			if(pid < 0) {
				//printf("Shell unable to fork\n");
				}
    			else if ( pid == 0 ) // child process
    			{
				
      				// try to execute the user's command  
				//printf("In Child Process \n");	
				if( execve( (char*)buffer, (char**)argv ) < 0 )
					printf( "shell: bad command\n" );
				exit(0);
      			}

		}

    			// parent-process waits for child-process to exit
		    	//int status = 0;
		if(is_bg==0){
		      //printf("now parent is waiting\n");
 		      waitpid(pid);
		}
    	}
}

int resolve_path(volatile char *buffer, volatile char *path) {
    int f;
    if(path[0] == '/') { // absolute path
        strcpy( (char*)buffer, (char*)path );
        parse_buffer( (char*)buffer );
        f =  open( (char*)buffer, 1 );
	if(f<0)
	    return f;
	close(f);
	return f;
    }
    else {              // relative path
        char pwd[64];
        getpwd(pwd);    // check against pwd
        int i=0;
        while(pwd[i]) i++;
        strcpy(pwd+i, (char*)path);
        strcpy((char*)buffer, pwd);
        parse_buffer( (char*)buffer );
        int fd = open( (char*)buffer, 1 );
        if( fd < 0 ) {  // check against path
            i=0;
            while(PATH[i]) i++;
            strcpy(PATH+i, (char*)path);
            strcpy((char*)buffer, PATH);
            PATH[i] = '\0';
            parse_buffer( (char*)buffer );
            f=  open( (char*)buffer, 1 );
	if(f<0)
             return f;
         close(f);
         return f;

        }
        else {
	close(fd);
	return fd;
	}
    }
}


