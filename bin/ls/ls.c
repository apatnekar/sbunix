#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>

struct dirent *dip;

void ls(char *dir) {
	int dd;
	dd = opendir(dir);
	if( dd >= 0 ) {
		while( ( dip = readdir(dd, dip) ) != NULL )
			printf("%s\t", dip->name);
	}
	else printf("no such directory\n");
	closedir(dd);
}

int main(int argc,char* argv[])
{
	dip = (struct dirent *)malloc(sizeof(struct dirent));
	if(argc == 1) {
		char pwd[64];
		getpwd(pwd);
		ls(pwd);
	}
	else if(argc == 2) {
		if(argv[1][0] == '/') { // absolute path
			char buffer[64];
			strcpy( buffer, argv[1] );
			parse_buffer( buffer );
			ls( buffer );
		}
		else {			// relative path
			char pwd[64];
			getpwd(pwd);
			if(argv[1][0] == '.' && argv[1][1] == '\0') { }
			else if(strcmp(argv[1], "..") == 0) {
				if(pwd[0] == '/' && pwd[1] == '\0') {
				}
				else {
					int i=0;
					while(pwd[i]) i++; i-= 2;
					while(pwd[i] != '/') i--;
					pwd[i+1] = '\0';
				}
			}
			else {
				int i=0;
				while(pwd[i]) i++;
				strcpy(pwd+i, argv[1]);
				parse_buffer( pwd );
			}
			ls(pwd);
		}
	}
  	return 0;
}

