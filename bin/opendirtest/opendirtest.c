#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>

int main(int argc,char* argv[]){
	int fd = opendir("/bin/");
	printf("fd return is %d \n",fd);

	struct dirent *dir = (struct dirent *)malloc(sizeof(struct dirent));
	dir = readdir(fd, dir);
	printf("%s\n", dir->name);
	closedir(fd);
	dir = readdir(fd, dir);
	printf("%s\n", dir->name);
	printf("%x\n", dir);

	return 0;
}

