#include <stdio.h>
#include <dirent.h>
#include <syscall.h>
#include <unistd.h>

int opendir(char *dir) {
    return __syscall1(OPENDIR, (uint64_t)dir);
}

struct dirent *readdir(int fd, struct dirent* DIR ){
    return (struct dirent *)__syscall2(READDIR, (uint64_t)fd, (uint64_t)DIR);
}

int read(int fd, char* buf, size_t count){
    return __syscall3(READ, fd, (uint64_t)buf, count);
}

int write(int fd, char* buf, size_t count){
    return __syscall3(WRITE, fd, (uint64_t)buf, count);
}

int open(char* path, int flags){
    return __syscall2(OPEN, (uint64_t)path, flags);
}

int closedir(int fd){
    return __syscall1(CLOSEDIR, fd);
}

int close(int fd){
    return __syscall1(CLOSE, fd);
}

int dup(int oldfd,int newfd){
    return __syscall2(DUP, oldfd,newfd);
}

int cd(char *path) {
    return __syscall1(CD, (uint64_t)path);
}

int fork() {
    int pid = 0;
    pid = __syscall0(FORK);
    return pid;
}

int execve(char *filename, char *argv[]) {
    return __syscall2(EXECVE, (uint64_t)filename, (uint64_t)argv);
}

int getpid() {
    return __syscall0(GETPID);
}

int getpwd(char *buffer) {
    return __syscall1(GETPWD,(uint64_t)buffer);
}

int setpwd(char *buffer) {
    return __syscall1(SETPWD,(uint64_t)buffer);
}

void exit() {
    __syscall0(EXIT);
}

void sleep(int seconds){
    __syscall1(SLEEP,seconds );
}

int wait() {
    return __syscall0(WAIT);
}

int waitpid(int pid) {
    return __syscall1(WAITPID,pid);
}

void *mmap(void *addr, uint64_t length, int prot, int flags, int fd, uint64_t offset){
    uint64_t ret = __syscall6(MMAP, (uint64_t)addr, length, (uint64_t)prot, (uint64_t)flags, (uint64_t)fd);//, (uint64_t)offset);
    if(ret == NULL)
        return (void *)(~0x0);
    else
        return (void *)ret;
}

int munmap(void *start, uint64_t length){
    return __syscall2(MUNMAP, (uint64_t)start, length);
}

int is_entry_present(uint64_t va){
    return __syscall1(IS_ENTRY_PRESENT, va);
}

int set_fg(int pid){
    return __syscall1(SETFG,pid);
}

int ulimit(uint32_t flag, int64_t limit){
    return __syscall2(ULIMIT, flag, limit);
}

void ps(){
    __syscall0(PS);
}
