#include <defs.h>
#include <stdio.h>
#include <utils.h>
#include <unistd.h>
#include <syscall.h>
#include <stdlib.h>
enum
{
    MAXMALLOC = 1024*1024        /* max size of one allocated chunk */
};

#define PTE_CONTINUED 0x400
#define PGSIZE 0x1000
#define PT_P 0x1 
#define PT_W 0x2
#define MAP_ANONYMOUS 0x0

uint64_t mbegin = 0x08000000;
uint64_t mend   = 0x10000000;
uint64_t mptr = 0x08001000;
uint64_t MPTE =  0x08000000;    // Bit map array to keep track of PTE

int isfree(void *v, size_t n){
    uint64_t va, end_va = (uint64_t) v + n;

    for (va = (uint64_t) v; va < end_va; va += PGSIZE)
        if (va >= (uint64_t) mend || is_entry_present(va))
            return 0;
    
    return 1;
}

void* malloc(size_t n){
    int i, cont;
    int nwrap;
    uint32_t *ref;
    void *v;
    
    if (mptr == 0x0){
        //printf("initialising MPTR \n");
        mptr = mbegin;
    }

    n = ROUNDUP(n, 4);

    if (n >= MAXMALLOC){
        printf("<<<<<<<  Large Memory Allocation Request: Denied!!  >>>>>>>>\n");
        return 0;
    }

    if ((uint64_t) mptr % PGSIZE){
        /*
         * we're in the middle of a partially
         * allocated page - can we add this chunk?
         * the +4 below is for the ref count.
         */
         //pritnf("000000000000000000    %lx %lx\n",mptr, mptr%PGSIZE );
        ref = (uint32_t*) (ROUNDUP(mptr, PGSIZE) - 4);
        if ((uint64_t) mptr / PGSIZE == (uint64_t) (mptr + n - 1 + 4) / PGSIZE) {
            (*ref)++;
            v = (void*)mptr;
            mptr += n;
            return v;
        }
        /*
         * stop working on this page and move on.
         */
        free((void*)mptr);        /* drop reference to this page */
        mptr = ROUNDDOWN(mptr + PGSIZE, PGSIZE);
    }
//printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> %lx\n",mptr );
    /*
     * now we need to find some address space for this chunk.
     * if it's less than a page we leave it open for allocation.
     * runs of more than a page can't have ref counts so we 
     * flag the PTE entries instead.
     */
    nwrap = 0;
    
    while (1) {
            if (isfree((void*)mptr, n + 4))
                    break;
            mptr += PGSIZE;
            if (mptr == mend) {
                    mptr = mbegin;
                    if (++nwrap == 2)
                            return 0;        /* out of address space */
            }
    }
//printf("shd reach here \n");
    /*
     * allocate at mptr - the +4 makes sure we allocate a ref count.
     */
    for (i = 0; i < n + 4; i += PGSIZE){
        cont = (i + PGSIZE < n + 4) ? PTE_CONTINUED : 0;

        //mmap(void *addr, size_t length, int prot, int flags,int fd, off_t offset);
        int64_t ans = (uint64_t)mmap((void*)mptr + i, PGSIZE, cont, MAP_ANONYMOUS ,0 , 0);
        //printf("......................................%ld\n",ans );
        if (ans < 0){
            //printf("<<<<<<<<<<<<<<<<<<<BAD\n");
            for (; i >= 0; i -= PGSIZE)
                munmap((void *)(mptr + i), PGSIZE);
            return 0;        /* out of physical memory */
        }
        
        if(cont == PTE_CONTINUED){

            char* byte = (char*)(MPTE + (mptr + i - MPTE)/(PGSIZE*8));
            char bit = ((mptr + i - MPTE)/PGSIZE) % 8;
            *(byte) = *byte | (1<<(bit-1)); 
            //printf(">>byte value after is %p, bit is %d\n",byte,(int)(bit-1));
        }
        
    }

    ref = (uint32_t*) (mptr + i - 4);
    *ref = 2;        /* reference for mptr, reference for returned block */
    v = (void*)mptr;
    mptr += n;
    return v;
}

void free(void *v){
    uint8_t *c;
    uint32_t *ref;
    uint64_t cont = 0;
    if (v == 0)
            return;
    if(mbegin > (uint64_t) v || (uint64_t) v >= mend){
    	printf("Error in free..\n");
    	while(1);
    }
    
    c = ROUNDDOWN(v, PGSIZE);

    char* byte = (char*)(MPTE+ ((uint64_t)c - MPTE)/(PGSIZE*8));
    char bit = ( ((uint64_t)c - MPTE) / PGSIZE) % 8;
    cont = *byte & (1<<(bit-1)); 
    //printf("byte value after is %d, bit is %d\n",(int)*byte,(int)(bit-1));
         
    while(cont>0) {
        munmap((void *)c,PGSIZE);
        c += PGSIZE;
        if(mbegin > (uint64_t)c || (uint64_t)c >= mend){
			printf("Error in malloc.. Haulting..\n");
			while(1);
		}
        byte = (char*)(MPTE+ ((uint64_t)c - MPTE)/(PGSIZE*8));
        bit = ( ((uint64_t)c - MPTE) / PGSIZE) % 8;
        cont = *byte & (1<<(bit-1)); 
        //printf("byte value after is %d, bit is %d\n",(int)*byte,(int)(bit-1));
    }

    // c is just a piece of this page, so dec the ref count
    // and maybe free the page.
    ref = (uint32_t*) (c + PGSIZE - 4);
    if (--(*ref) == 0)
        munmap((void *)c,PGSIZE);        

}
