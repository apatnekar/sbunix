#include <defs.h>
#include <sys/stdarg.h>
#include <sys/kutil.h>
#include <unistd.h>
#include <syscall.h>

#define isspace(c)      (c == ' ' || c == '\t' || c == 10 || c == 13 || c == 0)

char *skip_spaces(const char *str){
       while (isspace(*str))
               ++str;
       return (char *)str;
}

char *skip_n_spaces(const char *str,int n){
        int i=0;
        if(n<0)
            return (char*)str;
       while (isspace(*str) && i<n){
               ++str;
               ++i;
        }
       return (char *)str;
}

int isdigit(char c){
    if(c<'0' || c>'9')
        return 0;

    return 1;
}

int isxdigit(int ch){
        if (isdigit(ch))
                return 1;

        if ((ch >= 'a') && (ch <= 'f'))
                return 1;

        return (ch >= 'A') && (ch <= 'F');
}

int is_input(const char *str,int count){
       int i=0;
       if(count<0)
        return 0;
       while (isspace(*str) && i<count){
            ++str;
            ++i;
        }
        if(i==count)
            return 0;
       return 1;
}

int skip_atoi(const char **s){
    int i = 0;
    while (isdigit(**s))
        i = i*10 + *((*s)++) - '0'; 
        return i;
}

long _parse_integer(const char *s, unsigned int base, unsigned long *p){
    unsigned long res;
    unsigned int rv;
    res = 0;
    rv = 0;
    while (*s) {
        unsigned int val;

        if ('0' <= *s && *s <= '9')
            val = *s - '0';
        else if ('a' <= *s && *s <= 'f')
            val = *s - 'a' + 10;
	else
            break;

        if (val >= base)
            break;
          
        res = res * base + val;
        rv++;
        s++;
      }
      *p = res;
      return rv;
}

int strtol(const char *s, char** endp, long *res, unsigned int base){
    unsigned long tmp;
     int rv = 0;

     if (s[0] == '-') {
             rv = _parse_integer(s + 1, base, &tmp) + 1;
             if (rv < 0)
                     return rv;
             *res = -tmp;
     } else {
             rv = _parse_integer(s, base, &tmp);
             if (rv < 0)
                     return rv;
             *res = tmp;
     }
     s += rv ;
 
        if (endp)
            *endp = (char *)s;
     return 0;
}

int vsscanf(char *buf, const char *fmt, va_list args){
    char *str = buf;
    char *next;
    char digit;
    int num = 0;
    uint8_t qualifier;
    unsigned int base;
    union {
        long s;
        unsigned long u;
    }val;
    int is_sign;
    int count =1024;
    int bytes_read = 0;
    bytes_read = read(STDIN,str,count);
    while( !is_input(str,bytes_read)){
        bytes_read = read(STDIN,str,count);
        //sleep(1);
    }
      //  while(!is_input(str,read(STDIN,str,count)))
                //    sleep(1);
                
    //printf("--------->>> str first >> %s\n",str );
    while (*fmt) {
        /* skip any white space in format */
        /* white space in format matchs any amount of
         * white space, including none, in the input.
         */
        //printf("!!!!!!!!!!!!!  total value in buf %d \n",str-buf);
        if (isspace(*fmt)) {
            fmt = skip_spaces(++fmt);
            str = skip_n_spaces(str,bytes_read);
        }
        //printf("2. !!!!!!!!!!!!!  total value in buf %d \n",str-buf);
        
        /* anything that is not a conversion must match exactly */
        if (*fmt != '%' && *fmt) {
            if (*fmt++ != *str++)
                break;
            continue;
        }

        if (!*fmt)
                break;
        ++fmt;

        /* skip this conversion.
         * advance both strings to next white space
         */
        if (*fmt == '*') {
                if (!*str)
                        break;
                while (!isspace(*fmt) && *fmt != '%' && *fmt)
                        fmt++;
                while (!isspace(*str) && *str)
                        str++;
                continue;
        }

        /* get field width 
        field_width = -1;
        if (isdigit(*fmt)) {
                field_width = skip_atoi(&fmt);
                if (field_width <= 0)
                        break;
        }
*/
        /* get conversion qualifier */
        qualifier = -1;
        if (*fmt == 'l') {
                qualifier = *fmt++;
        }

            if (!*fmt)
                    break;

            if (*fmt == 'n') {
                    /* return number of characters read so far */
                    *va_arg(args, int *) = str - buf;
                    ++fmt;
                    continue;
            }
            str = skip_n_spaces(str,bytes_read);
                if (!*str){
                    //printf("shd come here after every input\n");
                    bytes_read = read(STDIN,str,count);
                    while(!is_input(str,bytes_read)){
                    bytes_read = read(STDIN,str,count);
                    //sleep(1);
                }
            }
            else{
                //printf("str already has some value which is %c , total values in buf %d",*str, str-buf);
            }
                
            base = 10;
            is_sign = 0;

            switch (*fmt++) {
            case 'c':
            {
                    char *s = (char *)va_arg(args, char*);
                    *s = *str++;
                    //printf("--------->>> str second >> %s\n",str );
                    num++;
            }
            continue;
            case 's':
            {
                    char *s = (char *)va_arg(args, char *);
                    /* first, skip leading white space in buffer */
                    str = skip_spaces(str);

                    /* now copy until next white space */
                    while (*str && !isspace(*str))
                            *s++ = *str++;
                    *s = '\0';
                    num++;
            }
            continue;
	    case 'x':
		    base = 16;
	            break;
            case 'd':
                    is_sign = 1;
            case 'u':
                    break;
            case '%':
                    /* looking for '%' in str */
                    if (*str++ != '%')
                            return num;
                    continue;
            default:
                    /* invalid format; stop here */
                    return num;
            }

            /* have some sort of integer conversion.
             * first, skip white space in buffer.
             */
            str = skip_spaces(str);

            digit = *str;
            if (is_sign && digit == '-')
                    digit = *(str + 1);

            if (!digit
		|| (base == 16 && !isxdigit(digit))
                || (base == 10 && !isdigit(digit)))
                break;

            if (is_sign)
                    strtol(str, &next, &val.s, base);
            else
                    strtol(str, &next, (long *)&val.u, base);

            switch (qualifier) {
            case 'l':
                    if (is_sign)
                            *va_arg(args, long *) = val.s;
                    else
                            *va_arg(args, unsigned long *) = val.u;
                    break;
            default:
                    if (is_sign)
                            *va_arg(args, int *) = val.s;
                    else
                            *va_arg(args, unsigned int *) = val.u;
                    break;
            }
            num++;

            if (!next)
                    break;
            str = next;
            //printf("&&&&&&&&&&&&   str now is %s \n",str);
    }

    return num;
}

int scanf(const char *fmt, ...){
    char buf[1024];
    va_list ap;
    int ret;
    int i;
    for(i=0;i<1024;i++)
        buf[i]='\0';
    va_start(ap, fmt);
    ret = vsscanf(buf, fmt, ap);
    va_end(ap);
    return(ret);
}
