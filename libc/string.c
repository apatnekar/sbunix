#include <defs.h>
#include <stdio.h>
#include <unistd.h>
int strlen(const char* str){
    int ret = 0;
    while ( str[ret] != 0 )
        ret++;
    return ret;
}

long pow(int a, int b)
{
  long r;
  if(b==0) return 1;
  r = pow(a,b/2);
  r = r*r;
  if(b%2) r = r*a;
  return r;
}

int strcmp(char *first, char *second)
{
   while(*first==*second)
   {
      if ( *first == '\0' || *second == '\0' )
         break;
      first++;
      second++;
   }
   if( *first == '\0' && *second == '\0' )
      return 0;
   else
      return -1;
}

void strcpy(char *dest, char *src){
    while(*src){
      *dest++ = *src++;     
    }
    *dest = '\0';
}

#define isspace(c)      (c == ' ' || c == '\t' || c == 10 || c == 13 || c == 0)

int strtok(char *s,char* argv[], int nbytes){
    int i;
    int count = 0;       
    for(i=0; i<nbytes;i++){
	 
   	if(isspace(s[i])){
    		s[i] = '\0';
    	}
    	else
    	{
    		argv[count++] = &(s[i]);
    		while(!isspace(s[i])){    		
			i++;
		}
		if(isspace(s[i]))
			s[i] = '\0';
    	}
    }
    return count;
}

int atoi(char *str)
{
    int res = 0; // Initialize result
    int i; 
    // Iterate through all characters of input string and update result
    for (i = 0; str[i] != '\0'; ++i){
	if(str[i]<'0'|| str[i]>'9')
            return -1;

        res = res*10 + str[i] - '0';
    }
 
    // return result.
    return res;
}

void parse_buffer(char *buffer) {
        if(strlen(buffer) < 2)
                return;
        int i=1;
        while(buffer[i]) {              // parse for ./
                if(buffer[i-2] == '/' && buffer[i-1] == '.' && buffer[i] == '/') {
                        int j=i+1;
                        while(buffer[j]) {
                                buffer[j-2] = buffer[j];
                                j++;
                        } buffer[j-2] = '\0'; i -= 2;
                } i++;
        }
        if(strlen(buffer) < 3)
                return;
        i=2;
        while(buffer[i]) {
                if(buffer[i-2] == '.' && buffer[i-1] == '.' && buffer[i] == '/') {
                        if(i-3 == 0) {  // root node
                                int j=i+1;
                                while(buffer[j]) {
                                        buffer[j-3] = buffer[j];
                                        j++;
                                } buffer[j-3] = '\0'; i -= 3;
                        }
                        else {
                                int k=i-4;
                                while(buffer[k] != '/') k--;
                                int j=i+1;
                                while(buffer[j]) {
                                        buffer[k+1] = buffer[j];
                                        k++; j++;
                                } buffer[k+1] = '\0'; i = 2;
                        }
                } i++;
        }
}

/*
void chdir(char *path) {
    if(path[0] == '/') {        // absolute path
        char buffer[64];
        int i=0;
        while(path[i]) {
            buffer[i] = path[i];
            i++;
        }   buffer[i] = '\0';
        parse_buffer( buffer );
        if( opendir( buffer ) > 0 ) {
            printf("%s\n", buffer);
            setpwd( buffer );
        }
        else { printf("wrong path given !!!\n"); }
    }
    else {              // relative path
        char pwd[64];
        getpwd(pwd);
        int i=0;
        while(pwd[i]) i++;
        if(path[0] == '.' && path[1] == '\0') {
        }
        else if(strcmp(path, "..") == 0) {
            if(pwd[0] == '/' && pwd[1] == '\0') ;
            else {
                int j=i-2;
                while(pwd[j] != '/') j--;
                pwd[j+1] = '\0';
            }
        }
        else {
            int j=0;
            while(path[j]) {
                pwd[i+j] = path[j];
                j++;
            }   pwd[i+j] = '\0';
      }
        parse_buffer(pwd);
        if( opendir( pwd) > 0 ) {
            setpwd(pwd);
            printf("%s\n", pwd);
        }
        else printf("no such directory\n");
    }
}
*/
