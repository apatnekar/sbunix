#ifndef _SYSCALL_H
#define _SYSCALL_H
#include <stdio.h>
#include <defs.h>

//#define SYSCALL_PROTO(n) static __inline uint64_t __syscall##n

#define SYSCALL_PROTO(n) static __inline int64_t __syscall##n

SYSCALL_PROTO(0)(uint64_t n) {
    int64_t a;  
    asm volatile("int $0x80" : "=a" (a) : "0" (n));  
    return a;
}

SYSCALL_PROTO(1)(uint64_t n, uint64_t a1) {
    int64_t a; 
    asm volatile("int $0x80" : "=a" (a) : "0" (n), "D" ((uint64_t)a1)); 
    return a;
}

SYSCALL_PROTO(2)(uint64_t n, uint64_t a1, uint64_t a2) {
    int64_t a; 
    asm volatile("int $0x80" : "=a" (a) : "0" (n), "D" ((uint64_t)a1), "S" ((uint64_t)a2)); 
    return a;
}

SYSCALL_PROTO(3)(uint64_t n, uint64_t a1, uint64_t a2, uint64_t a3) {
    int64_t a; 
    asm volatile("int $0x80" : "=a" (a) : "0" (n), "D" ((uint64_t)a1), "S" ((uint64_t)a2), "b"((uint64_t)a3)); 
    return a;
}

SYSCALL_PROTO(4)(uint64_t n, uint64_t a1, uint64_t a2, uint64_t a3, uint64_t a4) {
    uint64_t a;
    //printf(">>>>>>>>>>>>>>%lx %lx %lx %lx %lx %lx\n",n,a1,a2,a3,a4,a5 );
    asm volatile("int $0x80" : "=a" (a) : "0" (n), "D" (a1), "S" (a2), "b" (a3));
    return a;
}

SYSCALL_PROTO(6)(uint64_t n, uint64_t a1, uint64_t a2, uint64_t a3, uint64_t a4, uint64_t a5) {
    uint64_t a;
    //printf(">>>>>>>>>>>>>>%lx %lx %lx %lx %lx %lx\n",n,a1,a2,a3,a4,a5 );
    asm volatile("int $0x80" : "=a" (a) : "0" (n), "D" (a1), "S" (a2), "b" (a3), "c" (a4), "d" (a5));
    return a;
}

int is_entry_present(uint64_t vaddr);

enum {
    SCHED_YIELD = 0,
    PUTS,
    FORK,
    EXECVE,
    EXIT,
    GETPID,
    GETPWD,
    SETPWD,
    CD,
    WAIT,
    WAITPID,
    SLEEP,
    PS,
    SETFG,
    ULIMIT,
    WRITE,
    OPENDIR,
    READDIR,
    CLOSEDIR,
    READ,
    OPEN,
    CLOSE,
    MMAP,
    MUNMAP,
    IS_ENTRY_PRESENT,
    DUP,
    CREATE,
    LINK,
    UNLINK,
    CHDIR,
    TIME,
};

#endif
