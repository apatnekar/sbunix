#include <defs.h>
size_t strlen(const char* str);
long pow(int a, int b);
int strcmp(char *first, char *second);
void strcpy(char *dest, char *src);
char * gets ( char * str );
int strtok(char *s,char* argv[], int nbytes);
void parse_buffer(char *buffer);
//only converts positive integer 
int atoi(char *str);
void chdir(char *path);
