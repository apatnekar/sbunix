#ifndef _UTIL_H
#define _UTIL_H

#include <defs.h>

uint64_t pow(uint32_t a, uint64_t b);
int strcmp(char *first, char *second);
void strcpy(char *dest, char *src);
int is_childfile(char *temp);
char* is_prefix(char *src,  char *str);
#endif