#ifndef _slob_allocator_h_
#define _slob_allocator_h_

#include <defs.h>
#include <sys/kprintf.h>
#include <sys/phys_mem_mgr.h>
#include <sys/pages.h>
#include <sys/kern_macros.h>

/*
 * All partially free slob pages go on these lists.
 */
#define SLOB_BREAK1 256
#define SLOB_BREAK2 1024

#define SLOB_UNIT sizeof(slob_t)
#define SLOB_UNITS(size) (((size) + SLOB_UNIT - 1)/SLOB_UNIT)
#define SLOB_ALIGN L1_CACHE_BYTES

struct slob_page *list_128_slob_page;
struct slob_page *list_256_slob_page;
struct slob_page *list_512_slob_page;
struct slob_page *list_1024_slob_page;
struct slob_page *list_2048_slob_page;

struct slob_page {
	uint8_t flags;			// mandatory 
	uint8_t free;			// number of free blocks
	uint32_t count_free;		// free units left in page 
	uint64_t *page_virt_addr;	// physical address of page
	uint8_t block_bitmap[32];	// first free slob_t in page 
	struct slob_page *next;
};

void init_slob_allocator();
struct slob_page *get_list_for_size(int size);
uint32_t get_block_size_for_size(int size);
void slob_assign_blocks( uint64_t *page_addr, uint32_t size );
void *kmalloc(uint32_t size, int gfp);
void kfree(uint64_t *page_virt_addr, uint32_t size);

#endif

