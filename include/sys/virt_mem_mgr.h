#ifndef _VIRT_MEM_MGR_H
#define _VIRT_MEM_MGR_H

uint64_t* nextfree;
//void navigate_page_hierarchy(uint64_t *pml4, uint64_t flags);
void map_parent_page_hierarchy(uint64_t *child_vpml4);
void free_page_hierarchy(uint64_t *vpml4);
//void map_parent_page_hierarchy(uint64_t *parent_pml4, uint64_t *child_pml4);
void map(uint64_t* pa, uint64_t* va, uint32_t write_perm, uint32_t user_bit);
uint64_t *get_free_page();
uint64_t *get_zeroed_page();
uint64_t *get_free_pages(uint32_t order);
void free_page(uint64_t *virt_addr);
uint64_t pa(uint64_t* va);
void set_pages_readonly(uint64_t* child_vpml4);
void set_write(uint64_t* va);
int is_entry_present(uint64_t* va);
void unmap(uint64_t* va);
#endif

