#ifndef _MM_H_
#define _MM_H_


struct vm_operations_struct {

	//open operation
	//close operation
	//nopage operation
	//populate operation

};

struct vm_area_struct {

	struct mm_struct *vm_mm; 	/*associated mm struct*/
	uint64_t vma_start_addr; 	/*vma start address, inclusive*/
	uint64_t vma_end_addr; 		/*vma end address, exclusive*/
	struct vm_area_struct *next; 	/*points to the next vma*/
/*	
	pgprot_t vm_page_access_permissions;   
	unsigned long vm_flags;      */ 
	//struct vm_operations_struct  *vm_ops;           
};





#endif
