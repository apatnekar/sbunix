#ifndef _ELF_H
#define	_ELF_H 


typedef	int32_t  Elf64_Sword;


/* The ELF file header.  This appears at the start of every ELF file.  */
#define EI_NIDENT (16)
#define PT_NULL   0   /* p_type */
#define PT_LOAD   1
#define PT_DYNAMIC  2
#define PT_INTERP 3
#define PT_NOTE   4
#define PT_SHLIB  5
#define PT_PHDR   6
#define PT_NUM    7
#define GNU_STACK 0x51e5

//Segment Flags
#define PF_X  0x1 //Execute
#define PF_W  0x2 //Write
#define PF_R  0x4 //Read

struct Elf64_Ehdr
{
  unsigned char	e_ident[EI_NIDENT];	/* Magic number and other info */
  uint16_t	e_type;			/* Object file type */
  uint16_t	e_machine;		/* Architecture */
  uint32_t	e_version;		/* Object file version */
  uint64_t	e_entry;		/* Entry point virtual address */
  uint64_t	e_phoff;		/* Program header table file offset */
  uint64_t	e_shoff;		/* Section header table file offset */
  uint32_t	e_flags;		/* Processor-specific flags */
  uint16_t	e_ehsize;		/* ELF header size in bytes */
  uint16_t	e_phentsize;		/* Program header table entry size */
  uint16_t	e_phnum;		/* Program header table entry count */
  uint16_t	e_shentsize;		/* Section header table entry size */
  uint16_t	e_shnum;		/* Section header table entry count */
  uint16_t	e_shstrndx;		/* Section header string table index */
};

struct Elf64_Phdr{
  uint32_t  p_type;
  uint32_t  p_flags;
  uint32_t  p_offset;
  uint64_t  p_vaddr;
  uint64_t  p_paddr;
  uint64_t  p_filesz;
  uint64_t  p_memsz;
  uint64_t  p_align;
};

#endif	/* elf.h */
