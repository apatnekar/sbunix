#ifndef _cpuid_h_
#define _cpuid_h_

void cpuid(unsigned info, unsigned *rax,
			  unsigned *rbx,
			  unsigned *rcx,
			  unsigned *rdx);
void amd_info();
void print_cpu_info();

#endif
