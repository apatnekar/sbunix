#include <sys/interrupt.h>
#include <sys/process.h>
#include <sys/kprintf.h>

#define NUM_SYSCALLS 32	

extern void sys_call();
void init_syscalls();
void syscall_handler(struct regs *r);
void *do_mmap(void *addr, uint64_t length, int prot, int flags, int fd);
int do_munmap(void *start, uint64_t length);
