#ifndef _kern_macros_h_
#define _kern_macros_h_
#include <defs.h>
/****************************************************************
OS ka dimaag

	|		|
	|---------------| <-- 0x7ffe000
	|		|
	|		|
	|		|	AVAILABLE MEMORY - Lets assign this to user space - what say ?
	|		|
	|		|	
	|---------------| <-- 0x300000
	|		|
	|		|	AVAILABLE MEMORY
	|		|
	|		| <-- PGD: 0x220000, PUD: 221000, PMD: 222000, PT:223000, 224000(for video buffer)
	|---------------| <-- 0x220000 physfree
	|///////////////|
	|---------------| <-- 0x200000 physbase
	|		|
	|		|	AVAILABLE MEMORY
	|		|
	|		|
	|---------------| <-- 0x100000 
	|///////////////|
	|///////////////|
	|///////////////|	
	|///////////////|
	|///////////////|
	|---------------| <-- 0x9fc00
	|		|	AVAILABLE MEMORY
	|_______________| <-- 0x0

****************************************************************/


#define VIDEO_BUFFER			0xb8000
#define VIDEO_BUFFER_PT_ADDR		0x224000
#define VIDEO_BUFFER_VADDR		0xffffffff800B8000	

#define PAGE_ENTRY_SIZE 		64
#define PAGE_SIZE 			0x1000

#define PT_P				0x0000000000000001
#define PT_W				0x0000000000000002
#define PT_U   				0x0000000000000004

#define PMD_P	         		0x0000000000000001
#define PMD_READ_WRITE			0x0000000000000002
#define PMD_USER_SUPERVISOR		0x0000000000000004

#define PUD_P		        	0x0000000000000001
#define PUD_READ_WRITE			0x0000000000000002
#define PUD_USER_SUPERVISOR		0x0000000000000004

#define PGD_P   			0x0000000000000001
#define PGD_READ_WRITE			0x0000000000000002
#define PGD_USER_SUPERVISOR		0x0000000000000004
#define ADDRESS_MASK			0xFFFFFFFFFFFFF000 
#define PML4_READ			0xFFFFFF7FBFDFE000
#define PDP_READ			0xFFFFFF7FBFC00000
#define PD_READ				0xFFFFFF7F80000000
#define PT_READ				0xFFFFFF0000000000
// to RESET accessed and permission bits of physical address of page tables

uint64_t swapper_page_dir;
uint64_t npages;
uint64_t PHYSFREE;
uint64_t KERNMEM, PHYSBASE;
// Rounding operations (efficient when n is a power of 2)
// Round down to the nearest multiple of n
#define ROUNDDOWN(a, n)						\
({								\
	uint64_t __a = (uint64_t) (a);				\
	(typeof(a)) (__a - __a % (n));				\
})
// Round up to the nearest multiple of n
#define ROUNDUP(a, n)						\
({								\
	uint64_t __n = (uint64_t) (n);				\
	(typeof(a)) (ROUNDDOWN((uint64_t) (a) + __n - 1, __n));	\
})

#define PML4SHIFT 39
#define PDPSHIFT  30
#define PDSHIFT   21
#define PTSHIFT	  12

#define PML4X(la)	((((uint64_t) (la)) >> PML4SHIFT) & 0x1FF)
#define PDPX(la)         ((((uint64_t) (la)) >> PDPSHIFT) & 0x1FF)
#define PDX(la)         ((((uint64_t) (la)) >> PDSHIFT) & 0x1FF)
#define PTX(la)		((((uint64_t) (la)) >> PTSHIFT) & 0x1FF)

#endif
