#ifndef _FS_H_
#define _FS_H_

#include <defs.h>

#define MAXENTRY        20

#define FS_FILE        0x01
#define FS_DIRECTORY   0x02
#define FS_MOUNTPOINT  0x08 // Is the file an active mountpoint?

struct fs_node;
struct file_descriptor;
// These typedefs define the type of callbacks - called when read/write/open/close
// are called.
typedef uint64_t (*read_type_t)(struct file_descriptor* fd,char* buf, uint64_t count);
typedef uint64_t (*write_type_t)(struct file_descriptor*, char* buf,int64_t count);
typedef void (*open_type_t)(struct fs_node*);
typedef void (*close_type_t)(struct fs_node*);
typedef struct dirent *(*readdir_type_t)(struct file_descriptor* fd,struct dirent*);
typedef struct DIR *(*opendir_type_t)(struct fs_node*,uint64_t);
typedef uint64_t (*closedir_type_t)(struct fs_node*,uint64_t);

typedef struct fs_node
{
    char name[128];       // The filename.
    uint64_t flags;       // Includes the node type. See #defines above.
    uint64_t inode;       // This is device-specific - provides a way for a filesystem to identify files.
    uint64_t start,length;// Size of the file, in bytes.
    uint64_t f_child[MAXENTRY];
    read_type_t read;
    write_type_t write;
    open_type_t open;
    close_type_t close;
    opendir_type_t opendir;
    readdir_type_t readdir;
    closedir_type_t closedir;
    struct fs_node *ptr; // Used by mountpoints and symlinks.
} fs_node_t;

struct file_descriptor
{
    struct fs_node* file;
    uint32_t offset;
    uint32_t ref;
};

struct dirent
{
    char name[128]; // Filename.
    uint64_t ino;     // Inode number. Required by POSIX.
};

extern fs_node_t *fs_root; // The root of the filesystem.

// Standard read/write/open/close functions. Note that these are all suffixed with
// _fs to distinguish them from the read/write/open/close which deal with file descriptors
// , not file nodes.
int dup_fd(int old_fd, int new_fd);
int64_t do_opendir(char *dir);
struct dirent* do_readdir(int fd, struct dirent *DIR);
int64_t do_open(char *path, int flags);
int64_t do_closedir(int fd);
int64_t do_close(int fd);
int64_t do_write(int fd, char *buf, int64_t count);
int64_t do_read(int fd, char *buf, int64_t count);
int64_t tarfs_read(struct file_descriptor *file, char *buf, int64_t count);
uint64_t tarfs_write(fs_node_t *node, uint64_t size, uint8_t *buffer);
struct file_descriptor *tarfs_open(char *path, int flags);
uint64_t tarfs_close(fs_node_t *node);
struct dirent *tarfs_readdir(struct file_descriptor *file, struct dirent *DIR);
struct file_descriptor *tarfs_opendir(char *name);
uint64_t tarfs_closedir(fs_node_t *node);

struct file_descriptor *stdio_open(char *path, int flags);
int64_t stdio_read(struct file_descriptor *file, char *buf, int64_t count);
uint64_t stdio_write(struct file_descriptor *node,  char *buffer, int64_t size);
uint64_t stdio_close(struct file_descriptor *node);

#endif

