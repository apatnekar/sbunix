#ifndef _INTERRUPT_H
#define _INTERRUPT_H

#include <defs.h>

/* adapted from Chris Stones, shovelos */

#define GDT_CS        (0x00180000000000)  /*** code segment descriptor ***/
#define GDT_DS        (0x00100000000000)  /*** data segment descriptor ***/

#define C             (0x00040000000000)  /*** conforming ***/
#define DPL0          (0x00000000000000)  /*** descriptor privilege level 0 ***/
#define DPL1          (0x00200000000000)  /*** descriptor privilege level 1 ***/
#define DPL2          (0x00400000000000)  /*** descriptor privilege level 2 ***/
#define DPL3          (0x00600000000000)  /*** descriptor privilege level 3 ***/
#define P             (0x00800000000000)  /*** present ***/
#define L             (0x20000000000000)  /*** long mode ***/
#define D             (0x40000000000000)  /*** default op size ***/
#define W             (0x00020000000000)  /*** writable data segment ***/

/* This defines what the stack looks like after an ISR/IRQ was running */
struct regs
{
    uint64_t r9, r8, rsi, rbp, rdx, rcx, rbx, rax, rdi;  /* pushed by 'pusha' */
    uint64_t int_no, err_code;    /* our 'push byte #' and ecodes do this */
    uint64_t rip, cs, rflags, userrsp, ss;   /* pushed by the processor automatically */ 
}__attribute__((packed));

uint32_t timer_ticks;

/* gdt functions */
extern uint64_t gdt[];
void reload_gdt();

/* idt functions */
void load_idt();
void idt_set_gate(uint8_t num, uint64_t base, uint16_t selector, uint8_t flags, uint8_t ist);

/* irq functions */
void irq_install_handler(int irq, void (*handler)());
void irq_install();
void timer_install();

/* isrs functions */
void isrs_install();

/* timer functions */
void timer_install();

/* keyboard functions */
void keyboard_install();
int get_kbd_data(char* buf, uint32_t count);

/* page fault handler */
void handle_page_fault(struct regs *r);

#endif
