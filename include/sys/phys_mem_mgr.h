#ifndef _PHYS_MEM_MGR_H_
#define _PHYS_MEM_MGR_H_

void init_free_page_list(uint32_t *modulep, void *physbase, void *physfree);
uint64_t* alloc_phys_page(uint64_t * phys_addr);
void free_phys_page(uint64_t* phys_addr);
uint64_t virt2phys(uint64_t virtaddr);
uint64_t virt_to_phys_address(uint64_t va);
uint64_t phys_to_virt_address(uint64_t pa);
void memset(void *dst, int init_with, size_t len);
void *memcpy(void *dest, const void *src, size_t count);
uint32_t get_reference_count(uint64_t* phys_addr);
#endif

