#ifndef _PROCESS_H_
#define _PROCESS_H_

#include <sys/fs.h>
#include <sys/interrupt.h>

#define STACK_SIZE 8192
#define MAX_FD     0x20
#define VM_STACK   0x1
#define VM_HEAP    0x2
#define VM_OTHER   0x0

enum {
    //RUNNING = 0,
    RUNNABLE = 0,
    STOP,
    WAITING
} task_state;

struct file{
    uint64_t   file_start;       /* start address of region */
    uint64_t   vm_pgoff;         /* offset in file or NULL */
    uint64_t   vm_sz;            /* region initialised to here */
    uint64_t   bss_size;
};

/*  VMA struct */
struct vm_area_struct{
    struct mm_struct *vm_mm;       /* VM area parameters */
    uint64_t vm_start, vm_end;
    unsigned short vm_flags;
    unsigned short grows_down;
    unsigned short vm_type;
    struct file *vm_file;          /* File we map to (can be NULL). */
    struct vm_area_struct *vm_next;
};

/*  task_struct */
struct task_struct
{
    uint64_t pid;               // Process ID.
    uint64_t ppid;      // Parent Process ID.
    uint64_t rsp, rip, rbp;     // Stack, instruction and base pointers.
    uint64_t pml4, *vpml4;  // Page directory.
    uint64_t kernel_stack;      // Kernel stack location.
    uint64_t initial_kernel_stack;
    uint64_t task_state;
    int64_t wait_pid;
    uint64_t sleep_time;
    uint64_t fd[MAX_FD];
    uint64_t stack_limit, running_proc_limit;
    struct task_struct *next;   // The next task in a linked list.
    struct mm_struct *mm;
    char pwd[64];
};

// The currently running task.
volatile struct task_struct *current_task;
volatile struct task_struct *foreground_task;
volatile struct task_struct *stop_task;
volatile struct task_struct *wait_task;
volatile struct task_struct *sleep_task;
volatile struct task_struct *wait_for_read;

/*  mm_struct */
struct mm_struct{
    struct vm_area_struct *mmap, *vm_last;
    uint64_t start_code, end_code, start_data, end_data;
    uint64_t start_brk, brk, start_stack;
    uint64_t arg_start, arg_end, env_start, env_end;
    uint64_t rss, total_vm, locked_vm;
};

void idle_process();
void init_tasking();        // Initialises the tasking system - this creates idle process.
struct task_struct* create_process(char* filename);
struct task_struct *copy_task_struct(struct task_struct *ts_parent);
void switch_to_user_mode(struct task_struct *ts_obj);
void switch_to(struct task_struct *prev, struct task_struct *next);
void schedule();
int do_getpid();
int do_getpwd(char *buffer);
int do_setpwd(char *buffer);
int64_t do_fork();
int64_t do_execve(char *filename, char *argv[]);
struct task_struct *copy_task_struct(struct task_struct *ts_parent);
void flush_TLB();
uint64_t do_exit();
int64_t do_wait();
int64_t do_waitpid(int64_t pid);
void handle_fatal_error(char* err_msg);
void do_sleep(int64_t seconds);
void create_stdio_desc(struct task_struct* ts_obj);
int do_setfg(int pid);
int do_ulimit(uint32_t flag, uint64_t limit);
void do_ps();
struct task_struct *remove_current_task_struct();

#endif

