#ifndef INITTARFS_H
#define INITTARFS_H

#include <defs.h>
#include <sys/fs.h>

typedef struct
{
    uint32_t nfiles; // The number of files in the ramdisk.
} initrd_header_t;

typedef struct
{
    uint8_t magic;     // Magic number, for error checking.
    char name[64];  // Filename.
    uint64_t offset;   // Offset in the initrd that the file starts.
    uint64_t length;   // Length of the file.
} initrd_file_header_t;

// Initialises the initial ramdisk. It gets passed the address of the multiboot module,
// and returns a completed filesystem node.
//fs_node_t *initialise_initrd(uint64_t* location);
void init_tarfs();
void navigate_tarfs();
int do_cd(char *path);
void print_tarfs();
int search_rootnodes(char* name);
fs_node_t *inittarfs_root;             // Our root directory node.
//fs_node_t *inittarfs_dev;              // We also add a directory node for /dev, so we can mount devfs later on.
fs_node_t *root_nodes;              // List of file nodes.
int nroot_nodes;                    // Number of file nodes.

struct dirent dirent;

#endif

