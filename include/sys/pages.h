#ifndef _pages_h_
#define _pages_h_

#include <defs.h>

#define FREE_PAGE	0
#define USED_PAGE	1

uint8_t *free_list;
uint64_t *kern_pgdir;

void* boot_alloc(uint64_t n);
void update_free_list(uint64_t addr, uint32_t n ,uint8_t usecase);
void boot_map_region(uint64_t *pgdir, uint64_t va, uint64_t size, uint64_t pa, uint32_t perm);
uint64_t* pgdir_walk(uint64_t* pml4, const void *va, const void *pa, uint32_t create);
uint64_t table_walk(uint64_t* pgdir, const void *va, const void *pa, uint32_t create);
void mem_init();

#endif
