#ifndef _SCHED_H_
#define _SCHED_H_

/*
*defining the task_struct here
*/

struct task_struct {
	//volatile long state; //indicates the state of the task: runnable, interruptable, etc.
	//long counter; //mentions the dynamic portion of task's goodness
	//long priority; //mentions the static portion of task's goodness
	//unsigned long policy; //SCHED_FIFO, SCHED_RR, SCHED_OTHER
	struct mm_struct *mm; 
	uint64_t *stack_ptr;
	struct task_struct *next;
};

struct task_struct *task_list;


#endif
