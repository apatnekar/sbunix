#ifndef _LOADER_H
#define _LOADER_H
#include <sys/elf.h>
#include <sys/process.h>

#define USER_STACK_ADDR 0xF0000000
#define STACK_LIMIT (4*PAGE_SIZE)

void* search_tarfs(char *filename);
struct task_struct* load_elf(char *filename, struct task_struct* ts);
void load_PT_LOAD(struct Elf64_Ehdr *elfobj, struct Elf64_Phdr *elf_phdr, struct task_struct *ts_obj);
struct vm_area_struct * find_vma(struct mm_struct * mm, unsigned long addr);
void load_heap(struct task_struct *ts_obj);
void load_stack(struct task_struct *ts_obj);
void print_parsed_elf(struct task_struct *ts_obj);
void modify_task_struct(char *filename);
struct vm_area_struct *grow_stack();
int grow_heap(uint64_t size);
int access_ok(uint64_t addr, uint64_t count);
//void readsect(void*, uint32_t);
//void readseg(uint32_t, uint32_t, uint32_t);

#endif
