#include <defs.h>
struct dirent
{
    char name[128]; // Filename.
    uint64_t ino;     // Inode number. Required by POSIX.
};