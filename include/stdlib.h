#ifndef _STDLIB_H
#define _STDLIB_H

int main(int argc, char* argv[]);
void exit(int status);
void* malloc(unsigned int);
void free(void *v);
#endif
