#ifndef _DEFS_H
#define _DEFS_H

#define NULL 0

extern char kernmem, physbase;

typedef unsigned long __uint64_t;
typedef __uint64_t uint64_t;
typedef unsigned int __uint32_t;
typedef __uint32_t uint32_t;
typedef int __int32_t;
typedef __int32_t int32_t;
typedef unsigned char uint8_t;
typedef unsigned short int uint16_t;
typedef uint32_t size_t;
typedef long int int64_t; 

typedef unsigned int DWORD;
typedef unsigned char BYTE;

#endif
