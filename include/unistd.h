#ifndef _UNISTD_H_
#define _UNISTD_H_

#include <defs.h>

#define U_STACK_LIMIT 1
#define U_PROC_LIMIT 2

#define STDIN 0
#define STDOUT 1
#define STDERR 2

int fork();
int execve(char *filename, char *arg[]);
void exit();
int getpid();
int getpwd(char *buffer);
int setpwd(char *buffer);
int cd(char *path);
int waitpid(int pid);
int wait();
void sleep(int seconds);
int opendir(char *dir);
int closedir(int fd);
int close(int fd);
int dup(int oldfd,int newfd);
void *mmap(void *addr, uint64_t length, int prot, int flags, int fd, uint64_t offset);
int munmap(void *start, uint64_t length);
struct dirent * readdir(int fd, struct dirent* DIR);
int read(int fd, char* buf, size_t count);
int open(char* path, int flags);
int write(int fd, char* buf, size_t count);
int set_fg(int pid);
int ulimit(uint32_t flag, int64_t limit);
void ps();

#endif

