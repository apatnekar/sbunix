#ifndef _PRINTF_H
#define _PRINTF_H
#include <stdarg.h>
#include <defs.h>

#define LOG_FLAG 0 

#if LOG_FLAG
	#define LOG printf
#else
	#define LOG(...)
#endif

enum vga_color
{
    COLOR_BLACK = 0,
    COLOR_BLUE = 1,
    COLOR_GREEN = 2,
    COLOR_CYAN = 3,
    COLOR_RED = 4,
    COLOR_MAGENTA = 5,
    COLOR_BROWN = 6,
    COLOR_LIGHT_GREY = 7,
    COLOR_DARK_GREY = 8,
    COLOR_LIGHT_BLUE = 9,
    COLOR_LIGHT_GREEN = 10,
    COLOR_LIGHT_CYAN = 11,
    COLOR_LIGHT_RED = 12,
    COLOR_LIGHT_MAGENTA = 13,
    COLOR_LIGHT_BROWN = 14,
    COLOR_WHITE = 15,
};

size_t rowConsole;
size_t columnConsole;
uint8_t colorConsole;
uint16_t* bufferConsole;


int printf(const char *format, ...);
int vsprintf(char *buf, const char *format, va_list args);
uint8_t make_color(enum vga_color fg, enum vga_color bg);
uint16_t make_vgaentry(char c, uint8_t color);
size_t strlen(const char* str);
void init_console();
void clear_console();
void setcolor_console(uint8_t color);
void putentry_at_console(char c, uint8_t color, size_t x, size_t y);
void putchar(char c);
void puts(const char* data);
void scroll();
char *itoa( uint64_t value, char * str, int base, int sign_flag );
void update_cursor();
void outb(unsigned short _port, unsigned char _data);
unsigned char inb(unsigned short _port);
void print_time(uint32_t time_in_sec);
void print_kbd(int ctrl, char c);
void testPrintf();

#endif

