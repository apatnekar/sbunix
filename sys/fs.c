#include <defs.h>
#include <sys/gdt.h>
#include <sys/kutil.h>
#include <sys/fs.h>
#include <sys/kprintf.h>
#include <sys/tarfs.h>
#include <sys/process.h>
#include <sys/slob_allocator.h>
#include <sys/phys_mem_mgr.h>
#include <sys/loader.h>
#include <sys/inittarfs.h>

extern uint32_t enter_encountered;

int64_t tarfs_read(struct file_descriptor *file_d, char* buf, int64_t count){
    uint64_t bytes_to_read = 0;
    if((file_d->offset + count) <= file_d->file->length)
        bytes_to_read = count;
    else
        bytes_to_read = file_d->file->length - file_d->offset ;
    LOG("buf addr %lx and bytes_to_read %d \n",buf,bytes_to_read);
    memcpy(buf, (void*)(file_d->file->start + file_d->offset), bytes_to_read );
    file_d->offset = file_d->offset + bytes_to_read;
    return bytes_to_read;
}

struct file_descriptor* tarfs_open(char *pathname, int flags) {
    LOG("inside tarfs_open\n");
    int i=search_rootnodes(pathname);
    if(i==-1 || root_nodes[i].flags==FS_DIRECTORY)
        return NULL;
    struct file_descriptor* file_d = kmalloc(sizeof(struct file_descriptor),1);
    file_d->file = &(root_nodes[i]);
    file_d->offset = 0;
    file_d->ref = 1;
    LOG("end of tarfs_open\n");
    return file_d;
}

uint64_t tarfs_close(fs_node_t *node) {
    LOG("inside tarfs_close\n");
    return 0;
}

int create_fd(){
    int i;
    for(i=0;i<MAX_FD;i++)
        if(current_task->fd[i]==0)
            return i;  
    return -1;
}

int dup_fd(int old_fd, int new_fd){
	if(old_fd < 0 || old_fd >= MAX_FD || new_fd < 0 || new_fd >= MAX_FD)
		return -1;

	if(new_fd == old_fd)
		return new_fd;

	do_close(new_fd);	
    current_task->fd[new_fd] = current_task->fd[old_fd];	

	return new_fd;
}

int64_t do_open(char* path, int flags){
    int i;
    struct file_descriptor* fd =NULL;
    if(is_prefix(path, "/dev")){
        fd = stdio_open(path,flags);
        LOG("inside do_open calling stdio_open, fd is %lx\n",fd); 
    }
    else if(is_prefix(path, "/mnt"))
        fd = tarfs_open(path,flags);
    else
        fd = tarfs_open(path,flags);
    i = create_fd();
    if(fd==NULL || i==-1)
        return -1;
    current_task->fd[i] = (uint64_t)fd;
    return i;
}

int64_t do_read(int fd, char *buf, int64_t count){
    if(fd < 0 || fd >= MAX_FD)
        return -1;
    if(count<=0)
        return 0;
    struct file_descriptor *file_d = (struct file_descriptor*)current_task->fd[fd]; 
    //access_ok (buf, count)
    if(file_d!=NULL && file_d->file->read!=NULL)
        return file_d->file->read(file_d, buf, count);
    else
        return -1;  
}

int64_t do_write(int fd, char *buf, int64_t count){
    if(fd < 0 || fd >= MAX_FD)
        return -1;
    if(count<=0)
        return 0;
    struct file_descriptor * file_d = (struct file_descriptor*)current_task->fd[fd]; 
    //access_ok (buf, count)
    //printf("found file_d %lx \n",file_d);
    if(file_d->file->write!=NULL)
        return file_d->file->write(file_d, buf, count);
    else
        return -1;  
}

int64_t do_close(int fd){
    if(fd < 0 || fd >= MAX_FD)
        return -1;
    struct file_descriptor* file_d = (struct file_descriptor*)current_task->fd[fd];
    if(file_d!=NULL && (--file_d->ref) == 0)
        kfree((uint64_t*)file_d,sizeof(struct file_descriptor));
    current_task->fd[fd] = (uint64_t)0x0;
    return 0;
}

int64_t do_opendir(char *dir){
    int i;
    struct file_descriptor *fd =NULL;
    if(is_prefix(dir, "/mnt")){
        //fd = tarfs_opendir(dir);
	return -1;
    }	
    else
        fd = tarfs_opendir(dir);
    i = create_fd();
    if(fd==NULL || i==-1)
        return -1;
    current_task->fd[i] = (uint64_t)fd;
    return i;
}

struct dirent *do_readdir(int fd, struct dirent *DIR){
    LOG("inside do_readdir\n");
    if(fd < 0 || fd >= MAX_FD)
        return NULL;
    LOG("opening file descriptor\n");
    struct file_descriptor* file_d = (struct file_descriptor*)current_task->fd[fd];
    
    if(file_d!=NULL && file_d->file->readdir!=NULL){
        LOG("calling readdir and file_d->file->readdir %lx\n",file_d->file->readdir);
	return file_d->file->readdir(file_d, DIR);
    	
    }	
    else
        return NULL;
}

struct file_descriptor *tarfs_opendir(char *pathname){
    LOG("inside tarfs_opendir\n");
    uint32_t child=0;
    char *ptr;
    int i=search_rootnodes(pathname);
    LOG("value of i %d \n",i);
    if(i==-1 || root_nodes[i].flags==FS_FILE)
        return NULL;
    fs_node_t *fd = &(root_nodes[i]);
    LOG("file readir is %lx, fd addr is %lx \n",fd->readdir, fd);
    for(i=i+1; i<nroot_nodes; ++i ) {
        LOG("object name %s\t",fd->name);
        LOG("file readir is %lx, fd addr is %lx \n",fd->readdir, fd);
	ptr = is_prefix(root_nodes[i].name, pathname); 
        if(!ptr)
	    break;
            //continue;
        LOG("probable child is %s\n",ptr);

        if(is_childfile(ptr) && child < MAXENTRY) {
            LOG("inserted child\n");
            fd->f_child[child++] = (uint64_t)&(root_nodes[i]);
        }
    }
    struct file_descriptor *fddir = kmalloc(sizeof(struct file_descriptor),1);
    fddir->file = fd;
    LOG("file readir is %lx, fd addr is %lx \n",fd->readdir, fd);
    LOG("fddir->readdir is %lx, value of fd %lx\n",(fddir->file)->readdir,fddir->file);
    fddir->offset=0;
    fddir->ref=1;
    LOG("end of tarfs_opendir\n");
    return fddir;
}

struct dirent *tarfs_readdir(struct file_descriptor *file_d, struct dirent *DIR){
    LOG("file->current_child value %d\n",file_d->offset );
    if(file_d->offset == MAXENTRY || file_d->file->f_child[file_d->offset]==NULL)
        return NULL;
    LOG("inside tarfs_readdir \n");
    fs_node_t* child  = (fs_node_t *)((file_d->file->f_child[file_d->offset++]));
    LOG("------------->%p  %s\n",&child->name,child->name );
    strcpy(DIR->name,child->name);
    DIR->ino = child->inode;
    return DIR;
}

int64_t do_closedir(int fd){
    if(fd < 0 || fd >= MAX_FD)
        return -1;
    struct file_descriptor* file_d = (struct file_descriptor*)current_task->fd[fd];
    if(file_d !=NULL && (--file_d->ref) == 0)
        kfree((uint64_t*)file_d,sizeof(struct file_descriptor));
    current_task->fd[fd] = (uint64_t)0x0;
    return 0;
}

uint64_t tarfs_closedir(fs_node_t *dir){
    LOG("inside tarfs_closedir\n");
    if(dir!=NULL){
        //kfree((uint64_t *)dir,sizeof(struct File));
    }
    return 0;
}

int64_t stdio_read(struct file_descriptor *file_d, char *buf, int64_t count){
    volatile struct task_struct *current_task_next = current_task->next;
    volatile struct task_struct *cur_read = remove_current_task_struct();
    //printf("cr3 of next process is %lx\n",current_task_next->pml4);
    if(current_task == foreground_task) {
        if(enter_encountered==0) {
            cur_read->next = (struct task_struct*)wait_for_read;
            wait_for_read = cur_read;
            current_task = current_task_next;
            set_kernel_stack(current_task->initial_kernel_stack);
            switch_to((struct task_struct*)current_task,(struct task_struct *)cur_read);
        }
        int dd = get_kbd_data(buf, count);
        LOG(">>>>>>>>> %s\n",buf);
        return dd;
    }
    return -1;
}

/*      __asm__ __volatile__ (
            "movq %%rsp, (%1);"     // save RSP
            "movq $7f, %0;"         // save RIP
            "movq %2, %%rsp;"       // load next->stack var in rsp
            "movq %4, %%cr3;"       // load next->pml4 into cr3
        //"hlt;"
            "pushq %3;"             // restore RIP so as to continue execution in next task
            "retq;"                 // Switch to new task
            "7:\t"
            :"=g"(cur_read->rip)
            :"r"(&(cur_read->kernel_stack)), "r"(current_task_next->kernel_stack),
             "r"(current_task_next->rip), "r"(current_task_next->pml4)
        );
*/

struct file_descriptor* stdio_open(char *pathname, int flags) {
    LOG("inside stdio_open\n");
    int i;
    struct file_descriptor* file_d = kmalloc(sizeof(struct file_descriptor),1);
    if(strcmp(pathname,"/dev/stdin")==0)
        i=nroot_nodes;
    else if(strcmp(pathname,"/dev/stdout")==0)
        i=nroot_nodes +1;
    else if(strcmp(pathname,"/dev/stderr")==0)
        i=nroot_nodes +2;
    else
        return NULL;
    file_d->file = &(root_nodes[i]);
    file_d->offset = 0;
    file_d->ref = 1;
    LOG("end of stdio_open\n");
    return file_d;
}

uint64_t stdio_write(struct file_descriptor *node,  char *buffer,int64_t size){
    puts(buffer);
    return 0;
}

uint64_t stdio_close(struct file_descriptor *node) {
    LOG("inside tarfs_close\n");
    return 0;
}
