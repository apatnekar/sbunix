#include <defs.h>
#include <sys/kprintf.h>
#include <sys/phys_mem_mgr.h>
#include <sys/pages.h>
#include <sys/kern_macros.h>
#include <sys/slob_allocator.h>
#include <sys/virt_mem_mgr.h>

void init_slob_allocator()
{
    list_128_slob_page = (struct slob_page *)get_free_page();
    list_128_slob_page->next = NULL;
    list_256_slob_page = (struct slob_page *)get_free_page();
    list_256_slob_page->next = NULL;
    list_512_slob_page = (struct slob_page *)get_free_page();
    list_512_slob_page->next = NULL;
    list_1024_slob_page = (struct slob_page *)get_free_page();
    list_1024_slob_page->next = NULL;
    list_2048_slob_page = (struct slob_page *)get_free_page();
    list_2048_slob_page->next = NULL;
    

    slob_assign_blocks(get_free_page(), 128 );
    slob_assign_blocks(get_free_page(), 256 );
    slob_assign_blocks(get_free_page(), 512 );
    slob_assign_blocks(get_free_page(), 1024 );
    slob_assign_blocks(get_free_page(), 2048 );
}

struct slob_page *get_list_for_size(int size)
{
    if(size <= 128)	    return list_128_slob_page;
    else if(size <= 256)    return list_256_slob_page;
    else if(size <= 512)    return list_512_slob_page;
    else if(size <= 1024)   return list_1024_slob_page;
    else                    return list_2048_slob_page;
}

uint32_t get_block_size_for_size(int size) 
{
    if(size <= 128)         return 128;
    else if(size <= 256)    return 256;
    else if(size <= 512)    return 512;
    else if(size <= 1024)   return 1024;
    else 		    return 2048;
}


void *kmalloc(uint32_t size, int gfp)
{
    struct slob_page *list_slob_page = get_list_for_size(size);   // always returns ROOT
    uint32_t block_size = get_block_size_for_size(size);

    int i=0;
    while(list_slob_page!=NULL){	    
	if(list_slob_page->count_free < 1){        // if there is no empty object
	    if(list_slob_page->next!=NULL){
	        list_slob_page = list_slob_page->next;
	        continue;
	    }
	    else
		break;
	}
    	while( list_slob_page->block_bitmap[i] == 1) 
	    i++;
	list_slob_page->block_bitmap[i] = 1;
	list_slob_page->count_free--;
        return (void *)( (uint64_t)list_slob_page->page_virt_addr + (i*block_size) );
    }
    uint64_t *virt_addr = get_free_page();     // get new page and refill the pool
    if(virt_addr == NULL){
	printf("Kernel Panic!!\n");
        return NULL;	
    }
    list_slob_page->next = list_slob_page+sizeof(struct slob_page);
    list_slob_page=list_slob_page+sizeof(struct slob_page);           // create new node(since there is no malloc just increment the pointer)
    list_slob_page->next=NULL;
    slob_assign_blocks(virt_addr, size );
    list_slob_page->block_bitmap[0] = 1;
    list_slob_page->count_free--;
    return (void *)( (uint64_t)list_slob_page->page_virt_addr);
}

void slob_assign_blocks(uint64_t *virt_addr, uint32_t size )
{
    struct slob_page *list = get_list_for_size(size);
    uint32_t block_size = get_block_size_for_size(size);
    while(list->next!=NULL)
	list = list->next;
    list->free = USED_PAGE;
    list->count_free = PAGE_SIZE / block_size;
    list->page_virt_addr = virt_addr;
    int i=0;
    for(i=0;i<32;i++)
        list->block_bitmap[i] = 0x0;
}

void kfree(uint64_t *addr, uint32_t size){
    uint32_t block_size = get_block_size_for_size(size);
    struct slob_page *list = get_list_for_size(size);
    LOG("size is %lx, block_size is %lx\n",size,block_size);
    
    uint64_t find_size = 0;
	uint8_t bitmap_entry = 0;
   	if(list == NULL){
		printf("Trying to free unallocated memory from slob allocator. Panic!!!! \n");				
		return;
	}

	while(list != NULL){ //this loops for each struct pointing to a separate page and checks in which page  the virtual addr lies
        if((uint64_t)addr >= ((uint64_t)list->page_virt_addr) && (uint64_t)addr < ((uint64_t)list->page_virt_addr+PAGE_SIZE)){
			//we are in the correct struct's page
			break;
		}
		else{
			list = list->next;
		}
	}
	if(list == NULL){
		printf("Block not found for kfree in any page\n");
		return;
	}



    while(find_size!=4096){ //this loops for each block within a page
		if(((uint64_t)list->page_virt_addr + find_size) == (uint64_t)addr){ 
			list->block_bitmap[bitmap_entry] = 0x0;
			list->count_free++;
			LOG("kfreed memory from slob allocator successfully for address %lx !! \n",addr);
			return;
		}
		find_size += block_size;
		bitmap_entry++;
    }
	
	printf("Block not found for kfree within the slob page\n");
	return;
}
