#include <sys/interrupt.h>

#define MAX_IDT 256

struct idt_entry{
    uint16_t offset_low;     // offset bits 0...15
    uint16_t selector;       // a code segment selector in GDT or LDT 16...31
    uint8_t ist;             // unused, has to be 0 32...39
    uint8_t type_attr;       // type and attributes 40...43, Gate Type 0...3
    uint16_t offset_middle;  // offset bits 16..31, set at 48...63
    uint32_t offset_high;    // offset bits 32..63, set at 64...95
    uint32_t zero;           // unused set to 0, set at 96...127
}  __attribute__((packed));
 
struct idtr_t
{
    uint16_t size;
    uint64_t addr;
} __attribute__((packed));

struct idt_entry idt[MAX_IDT] = {};

static struct idtr_t idtr = {
    sizeof(idt),
    (uint64_t) idt,
};

void _x86_64_asm_lidt(struct idtr_t* idtr);

void load_idt(){
    _x86_64_asm_lidt(&idtr);
} 

void idt_set_gate(uint8_t num, uint64_t base, uint16_t selector, uint8_t flags, uint8_t ist){
    idt[num].offset_low = (base & 0x0FFFF);
    idt[num].offset_middle = ((base>>16) & 0x0FFFF);
    idt[num].offset_high = ((base>>32) & 0x0FFFFFFFF);

    idt[num].selector = selector;
    idt[num].ist = ist;
    idt[num].type_attr= flags;
}
