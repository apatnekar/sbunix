#include <sys/interrupt.h>
#include <sys/kprintf.h>
#include <sys/kern_macros.h>
#include <sys/virt_mem_mgr.h>
#include <sys/process.h>
#include <sys/loader.h>
#include <sys/phys_mem_mgr.h>
/* These are function prototypes for all of the exception
*  handlers: The first 32 entries in the IDT are reserved
*  by Intel, and are designed to service exceptions! */
extern void isr0();
extern void isr1();
extern void isr2();
extern void isr3();
extern void isr4();
extern void isr5();
extern void isr6();
extern void isr7();
extern void isr8();
extern void isr9();
extern void isr10();
extern void isr11();
extern void isr12();
extern void isr13();
extern void isr14();
extern void isr15();
extern void isr16();
extern void isr17();
extern void isr18();
extern void isr19();
extern void isr20();
extern void isr21();
extern void isr22();
extern void isr23();
extern void isr24();
extern void isr25();
extern void isr26();
extern void isr27();
extern void isr28();
extern void isr29();
extern void isr30();
extern void isr31();

/* This is a very repetitive function... it's not hard, it's
*  just annoying. As you can see, we set the first 32 entries
*  in the IDT to the first 32 ISRs. We can't use a for loop
*  for this, because there is no way to get the function names
*  that correspond to that given entry. We set the access
*  flags to 0x8E. This means that the entry is present, is
*  running in ring 0 (kernel level), and has the lower 5 bits
*  set to the required '14', which is represented by 'E' in
*  hex. */
void isrs_install()
{
    idt_set_gate(0, (uint64_t)isr0, 0x08, 0x8E, 0);
    idt_set_gate(1, (uint64_t)isr1, 0x08, 0x8E, 0);
    idt_set_gate(2, (uint64_t)isr2, 0x08, 0x8E, 0);
    idt_set_gate(3, (uint64_t)isr3, 0x08, 0x8E, 0);
    idt_set_gate(4, (uint64_t)isr4, 0x08, 0x8E, 0);
    idt_set_gate(5, (uint64_t)isr5, 0x08, 0x8E, 0);
    idt_set_gate(6, (uint64_t)isr6, 0x08, 0x8E, 0);
    idt_set_gate(7, (uint64_t)isr7, 0x08, 0x8E, 0);
    idt_set_gate(8, (uint64_t)isr8, 0x08, 0x8E, 0);
    idt_set_gate(9, (uint64_t)isr9, 0x08, 0x8E, 0);
    idt_set_gate(10, (uint64_t)isr10, 0x08, 0x8E, 0);
    idt_set_gate(11, (uint64_t)isr11, 0x08, 0x8E, 0);
    idt_set_gate(12, (uint64_t)isr12, 0x08, 0x8E, 0);
    idt_set_gate(13, (uint64_t)isr13, 0x08, 0x8E, 0);
    idt_set_gate(14, (uint64_t)isr14, 0x08, 0x8E, 0);
    idt_set_gate(15, (uint64_t)isr15, 0x08, 0x8E, 0);
    idt_set_gate(16, (uint64_t)isr16, 0x08, 0x8E, 0);
    idt_set_gate(17, (uint64_t)isr17, 0x08, 0x8E, 0);
    idt_set_gate(18, (uint64_t)isr18, 0x08, 0x8E, 0);
    idt_set_gate(19, (uint64_t)isr19, 0x08, 0x8E, 0);
    idt_set_gate(20, (uint64_t)isr20, 0x08, 0x8E, 0);
    idt_set_gate(21, (uint64_t)isr21, 0x08, 0x8E, 0);
    idt_set_gate(22, (uint64_t)isr22, 0x08, 0x8E, 0);
    idt_set_gate(23, (uint64_t)isr23, 0x08, 0x8E, 0);
    idt_set_gate(24, (uint64_t)isr24, 0x08, 0x8E, 0);
    idt_set_gate(25, (uint64_t)isr25, 0x08, 0x8E, 0);
    idt_set_gate(26, (uint64_t)isr26, 0x08, 0x8E, 0);
    idt_set_gate(27, (uint64_t)isr27, 0x08, 0x8E, 0);
    idt_set_gate(28, (uint64_t)isr28, 0x08, 0x8E, 0);
    idt_set_gate(29, (uint64_t)isr29, 0x08, 0x8E, 0);
    idt_set_gate(30, (uint64_t)isr30, 0x08, 0x8E, 0);
    idt_set_gate(31, (uint64_t)isr31, 0x08, 0x8E, 0);
}

/* This is a simple string array. It contains the message that
*  corresponds to each and every exception. We get the correct
*  message by accessing like:
*  exception_message[interrupt_number] */
char *exception_messages[] =
{
    "Division By Zero Exception",		/* 0 */
    "Debug Exception",				/* 1 */
    "Non Maskable Interrupt Exception",	        /* 2 */ 
    "Breakpoint Exception",			/* 3 */
    "Overflow Exception",			/* 4 */
    "Out of Bounds Exception",			/* 5 */
    "Invalid opcode Exception",			/* 6 */
    "Device not available Exception",		/* 7 */
    "Double fault Exception",			/* 8 */
    "Coprocessor segment overrun Exception",    /* 9 */
    "Invalid TSS Exception",			/* 10 */
    "Segment not present Exception",		/* 11 */
    "Stack fault Exception",			/* 12 */
    "General protection fault Exception",	/* 13 */
    "Page fault Exception",			/* 14 */
    "Reserved",					/* 15 */
    "Floating point error Exception",		/* 16 */
    "Alignment check Exception",		/* 17 */
    "Machine check Exception",			/* 18 */
    "SIMD floating point Exception",		/* 19 */
    "Reserved",			/* 20 */
    "Reserved",			/* 21 */
    "Reserved",			/* 22 */
    "Reserved",			/* 23 */
    "Reserved",			/* 24 */
    "Reserved",			/* 25 */
    "Reserved",			/* 26 */
    "Reserved",			/* 27 */
    "Reserved",			/* 28 */
    "Reserved",			/* 29 */
    "Reserved",			/* 30 */
    "Reserved",			/* 31 */
};

/* All of our Exception handling Interrupt Service Routines will
*  point to this function. This will tell us what exception has
*  happened! Right now, we simply halt the system by hitting an
*  endless loop. All ISRs disable interrupts while they are being
*  serviced as a 'locking' mechanism to prevent an IRQ from
*  happening and messing up kernel data structures */
void fault_handler(struct regs *r)
{
    if(r->int_no == 14){
        handle_page_fault(r);
    }

    /* Is this a fault whose number is from 0 to 31? */
    else if (r->int_no < 32)
    {
        /* Display the description for the Exception that occurred.
        *  In this tutorial, we will simply halt the system using an
        *  infinite loop */
        printf(exception_messages[r->int_no]);
	printf(" with error code: %lx\n", r->err_code);
        printf(" System Halted!\n");
        for (;;);
    }
}

#define PAGEFAULT_W	0x2
#define PAGEFAULT_USER 0x4
#define PAGEFAULT_P	0x1
void handle_page_fault(struct regs *r){
	uint64_t faulting_addr = NULL;
        __asm__ __volatile__ ("movq %%cr2, %0" : "=a" (faulting_addr));
	LOG("Faulting address is : %lx for task pid %d and value of rsp is %lx\n",faulting_addr,current_task->pid,r->userrsp);
	uint64_t err = r->err_code & 0xF; // only last 3 bits are imp
	
    LOG("err code is %lx\n",err); 

        struct vm_area_struct *vma = find_vma(current_task->mm, faulting_addr);
        uint64_t start = ROUNDDOWN(faulting_addr, PAGE_SIZE);//ROUNDDOWN(vma->vm_start, PAGE_SIZE);
        uint64_t* phys_addr;
        uint32_t write_perm = 0;

        if(vma == NULL) {
            //printf("cannot find in VMA list\n");
            if(faulting_addr >= (r->userrsp-0x20) && faulting_addr <= (r->userrsp)){
                LOG("stack is growing \n");
                vma = grow_stack();
                if(vma==NULL){
                    //printf("stack overflow error!!\n");
                    //while(1);
                    handle_fatal_error("Stack OverFlow!!");
                }
            }
            else{
	//	printf("Faulting address is : %lx for task pid %d and value of rsp is %lx\n",faulting_addr,current_task->pid,r->userrsp);        
	        //printf("Illegal access... segment violation");
                //while(1);
                handle_fatal_error("segment violation");
                //while(1);
            }
        }
	LOG(" In Page fault vm's start %lx and end %lx \n",vma->vm_start,vma->vm_end);        
        if(vma->vm_flags & PF_W ){
            LOG("write_perm set \n");
            write_perm =1;
        }
	LOG("is entry present ? %d \n",is_entry_present((uint64_t*)start));        
        if( (err & PAGEFAULT_P) && (err & PAGEFAULT_W)){
            LOG("-------PAGE FAULT ROUTINE & TRYING TO WRITE CALLED------\n");
            if(write_perm){
                uint64_t phys = pa((uint64_t*)start) & ~0XFFF;

                if(get_reference_count((uint64_t*) phys)==1){
                 
                    set_write((uint64_t*)start);
                    flush_TLB();
                    LOG("COW if\n");
                }
                else{
                    LOG("COW else\n");
                    uint64_t* new = get_zeroed_page();
                    phys_addr = (uint64_t*)(pa(new) & ~0XFFF) ;
                    //printf("new page %lx old page %lx start %lx\n",phys_addr,phys,start );
                    memcpy((void*)new,(void*)start,PAGE_SIZE);

                    map(phys_addr, (uint64_t*)(start), write_perm, 1);
                    
                    flush_TLB();
                    //decrement ref count
                    free_phys_page((uint64_t*)phys);
                }
		    LOG("returned from cowww mooo \n");
                return;
            }
            else{
                //printf("User or supervisor Process trying to write on a read only page\n");
                // Raise segv fault. Exit the process to-do 
                while(1);
            }       
        }
        
        LOG("-------PAGE FAULT ROUTINE CALLED & TRYING TO READ or present bit not set------\n");
        if( (err & PAGEFAULT_P) && !(err & PAGEFAULT_W)){
            //uint64_t pt = pa((uint64_t*)start);
            //printf("pt entry in child is %lx\n",pt);
            //printf("User or Supervisor process tried to read or write to a page its not allowed to\n");
            // Raise segv fault. Exit the process to-do 
            while(1);
        }
        
		LOG("pt entry not present present\n");
		LOG("no permissions to read this location");
		LOG("Page not found\n");
		// read vma's and map it
		LOG("Current task pid: %u\n",current_task->pid);

		LOG("vma address is %p\n",vma);
        LOG("start address mapped is %p  write perm:%lx\n",start,write_perm);		
		
        phys_addr = alloc_phys_page(NULL);
        map(phys_addr, (uint64_t*)(start), write_perm, 1);
        memset((void *)start, 0, PAGE_SIZE);
		if( vma->vm_file == NULL){
			//printf("anonymous vma \n ");
			//return;
		}
		else{
			LOG("file backed - vma\n");
			LOG(" file start %lx, file_offset %lx, file_sz %lx\n",vma->vm_file->file_start,vma->vm_file->vm_pgoff, vma->vm_file->vm_sz);
			
            uint64_t diff, copy_sz, end,dest, src;
            end = start+PAGE_SIZE;
            diff =0;

            if(start <= vma->vm_start){
                dest = vma->vm_start;
                src = vma->vm_file->file_start + vma->vm_file->vm_pgoff ;
            }
            else{
                dest = start;
                diff = start - vma->vm_start;
                src = vma->vm_file->file_start + vma->vm_file->vm_pgoff + diff; 
            }
            
            if((vma->vm_file->vm_sz) <= PAGE_SIZE){
                copy_sz = vma->vm_file->vm_sz;
            }
            else if(end <= (vma->vm_end - vma->vm_file->bss_size)){
                copy_sz = PAGE_SIZE;
            }
            else{
                copy_sz = end - (vma->vm_end - vma->vm_file->bss_size);
            }

            memcpy((void*)dest,(void*) src,copy_sz);  
                    
        }
    LOG("Reached End of Page fault handler\n");
}
