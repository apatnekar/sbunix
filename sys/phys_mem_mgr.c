#include <sys/kern_macros.h>
#include <sys/pages.h>
#include <sys/kprintf.h>
#include <sys/tarfs.h>

void init_free_page_list(uint32_t* modulep, void* physbase, void* physfree)
{
    uint64_t cur_page=0,cur_mem_end=0;
    free_list = (uint8_t *)(KERNMEM | (uint64_t)PHYSFREE);
    uint64_t track_mem_end=0,mem_base,mem_end;

    struct smap_t {
        uint64_t base, length;
        uint32_t type;
    }__attribute__((packed)) *smap;

    free_list[cur_page] = USED_PAGE;  /* first page */
    while(modulep[0] != 0x9001) modulep += modulep[1]+2;
    for(smap = (struct smap_t*)(modulep+2); smap < (struct smap_t*)((char*)modulep+modulep[1]+2*4); ++smap)
    {
        if (smap->type == 1 /* memory */ && smap->length != 0) {
            LOG("Available Physical Memory [%x-%x]\n", smap->base, smap->base + smap->length);
            LOG("tarfs in [%p:%p]\n", &_binary_tarfs_start, &_binary_tarfs_end);
            mem_base = ROUNDUP(smap->base,PAGE_SIZE);
            mem_end = ROUNDDOWN(smap->base + smap->length,PAGE_SIZE);
            // If there is HOLE
            if(track_mem_end < mem_base){
        LOG("$$$> Hole memory track_mem_end %lx mem_base %lx \n",track_mem_end,mem_base);
		for(cur_mem_end = track_mem_end; cur_mem_end < mem_base; cur_mem_end += PAGE_SIZE)
                    free_list[cur_page++] = USED_PAGE;
	    }
            //Available Memory
	    LOG("$$$$$$$$$ track_mem_end %lx mem_base %lx \n",mem_base,mem_end);
        for(cur_mem_end = mem_base; cur_mem_end < mem_end; cur_mem_end += PAGE_SIZE){
		    if( cur_mem_end >= (uint64_t)physbase && cur_mem_end < (uint64_t)physfree )
                    free_list[cur_page++] = USED_PAGE;
                else{
                    free_list[cur_page++] = FREE_PAGE;
                }
	   }
            track_mem_end = mem_end;
        }
    }
    npages = cur_page;
    free_list[0] = USED_PAGE;
    boot_alloc(npages);
    /*
    uint64_t addr = 0x2;
    printf("%u %lx\n",free_list[addr-2] ,&free_list[0]);
    printf("%u \n",free_list[addr-1] );
    printf("%u \n",free_list[addr] );
    printf("%u \n",free_list[addr+1] );
    printf("%u \n",free_list[addr+2] );
    while(1);*/
}


uint64_t* alloc_phys_page(uint64_t* physical_addr){
    LOG("alloc_phys_page");
    uint64_t i;
    void* phys_addr;
    if(physical_addr==NULL){
    	for(i=1;i<npages;++i)
    	{
            if(free_list[i]==FREE_PAGE){
            	phys_addr = (uint64_t*) (i * PAGE_SIZE);
            	free_list[i]=USED_PAGE;
        	    LOG("returning phys_addr: %lx\n", phys_addr);
            	return phys_addr;
            }
    	}
    }
    else{
	free_list[(uint64_t)physical_addr/PAGE_SIZE]++;
	return physical_addr;
    }
    LOG("returning phys_addr: NULL\n");
    return NULL;
}

void free_phys_page(uint64_t* phys_addr){
    uint64_t indx;
    indx = (uint64_t)phys_addr / PAGE_SIZE;
    if(free_list[indx]!=FREE_PAGE){
        free_list[indx]--;
    }
    else{
	printf("Nothing to free\n");
    while(1);
    }
}

uint32_t get_reference_count(uint64_t* phys_addr){
    uint64_t indx;
    indx = (uint64_t)phys_addr / PAGE_SIZE;
    return (uint32_t)free_list[indx];
}

uint64_t virt2phys(uint64_t virtaddr)
{
    uint64_t offset = PTX(virtaddr) * 0x8;             // convert to Byte offset
    uint64_t last = ((virtaddr>>9) & 0x7FFFFFFFFF) | offset;   // left shift to make room for new index
    uint64_t final = virtaddr & 0xFFFF000000000000;        // get higher 16 bit
    final = final | 0xFF0000000000 | last;             // add 511th value in address
    
    return 0xFFFFFFFFFFFFE000 & *((uint64_t *)final);      // clear last 13 bits (P, Dirty, R/W..,etc) & return
}

uint64_t virt_to_phys_address(uint64_t va){
    return (va - KERNMEM + PHYSBASE);
}

uint64_t phys_to_virt_address(uint64_t pa){
    return (pa + KERNMEM - PHYSBASE);
}

// dest = Virtual Address, n = bytes
void memset(void *dest, int init_with, size_t n)
{
    if (n == 0)
        return;
    /* The below stores the value in fill_value n times to the pointer dest*/
    if ((uint64_t)dest%4 == 0 && n%4 == 0) {
        init_with &= 0xFF;
        init_with = (init_with<<24)|(init_with<<16)|(init_with<<8)|init_with;
        asm volatile("cld; rep stosl\n"
                        :: "D" (dest), "a" (init_with), "c" (n/4)
                        : "cc", "memory");
    } else
        asm volatile("cld; rep stosb\n"
                        :: "D" (dest), "a" (init_with), "c" (n)
                        : "cc", "memory");
    return;
}

void *memcpy(void *dest, const void *src, size_t count)
{
   const char *sp = (const char *)src;
   char *dp = (char *)dest;
   for(; count != 0; count--) *dp++ = *sp++;
   return dest;
}
