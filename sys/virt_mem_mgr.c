#include <sys/kern_macros.h>
#include <sys/virt_mem_mgr.h>
#include <sys/phys_mem_mgr.h>
#include <sys/pages.h>
#include <sys/kprintf.h>

void map(uint64_t* pa, uint64_t* va, uint32_t write_perm, uint32_t user_bit){
    uint64_t pml4_index = PML4X(va); 
    uint64_t pdp_index = PDPX(va);
    uint64_t pd_index = PDX(va);
    uint64_t pt_index = PTX(va);
    uint64_t* pml4_entry = ((void*)PML4_READ + pml4_index*8);
    LOG(" pml4 index: %lx , PDP index: %lx, PD index: %lx, PT index: %lx\n",pml4_index,pdp_index, pd_index , pt_index);
    LOG("pml4 entry corresponsiding to va %lx is %lx\n",va,*((uint64_t*)pml4_entry));

    if(user_bit)
        user_bit = PT_U;
    if(write_perm)
        write_perm = PT_W;

    if(*(uint64_t*)pml4_entry & PT_P){
        LOG("entry is present in pml4,this means pdp is present\n");
    }
    else{
        LOG(" create new pdp, entry not present and fill in details in pml4 table\n");
        void* pdp = (void*)alloc_phys_page(NULL);
        *(uint64_t*)pml4_entry = (uint64_t)pdp | user_bit | PT_P | PT_W;
        LOG("new pml4 entry corresponsiding to va %lx is %lx\n",va,*((uint64_t*)pml4_entry));
        memset((uint64_t*)((void*)PDP_READ + (pml4_index<<12)), 0, PAGE_SIZE);
    }

    uint64_t* pdp_entry = ((void*)PDP_READ + (pml4_index<<12) + pdp_index*8 );
    LOG("pdp entry corresponsiding to va %lx is %lx\n",va,*((uint64_t*)pdp_entry));
    if(*(uint64_t*)pdp_entry & PT_P){
        LOG("entry is present in pdp,this means pd is present\n");
    }
    else{
        LOG(" create new pd, entry not present and fill in details in pdp table\n");
        void* pd = (void*)alloc_phys_page(NULL);
        *(uint64_t*)pdp_entry = (uint64_t)pd | user_bit | PT_P | PT_W;
        LOG("new pdp entry corresponsiding to va %lx is %lx\n",va,*((uint64_t*)pdp_entry));
        memset((uint64_t*)((void*)PD_READ + (pml4_index<<21) + (pdp_index<<12)), 0, PAGE_SIZE);
    }

    uint64_t* pd_entry = ((void*)PD_READ + (pml4_index<<21) + (pdp_index<<12) + pd_index*8 );
    LOG("pd entry corresponsiding to va %lx is %lx\n",va,*((uint64_t*)pd_entry));
    if(*(uint64_t*)pd_entry & PT_P){
        LOG("entry is present in pdp,this means pt is present\n");
    }
    else{
        LOG(" create new pt, entry not present and fill in details in pd table\n");
        void* pt = (void*)alloc_phys_page(NULL);
        *(uint64_t*)pd_entry = (uint64_t)pt | user_bit | PT_P | PT_W;
        LOG("new pd entry corresponsiding to va %lx is %lx\n",va,*((uint64_t*)pd_entry));
    memset((uint64_t*)((void*)PT_READ + (pml4_index<<30) + (pdp_index<<21) + (pd_index<<12)), 0, PAGE_SIZE);
    }

    uint64_t* pt_entry = ((void*)PT_READ + (pml4_index<<30) + (pdp_index<<21) + (pd_index<<12) + (pt_index*8) );
    LOG("pt entry corresponsiding to va %lx is %lx\n",va,*((uint64_t*)pt_entry));
    *(uint64_t*)pt_entry = (uint64_t)pa | user_bit | PT_P | write_perm;     
}

void set_pages_readonly(uint64_t* child_vpml4){
    uint64_t child_phys_pml4 = (uint64_t)pa(child_vpml4);
    child_phys_pml4 = child_phys_pml4 & ~0xFFF;
    int pml4_index =0;
    int pdp_index = 0;
    int pd_index = 0;
    int pt_index = 0;
    uint64_t phys;
    for(pml4_index=0;pml4_index<510;pml4_index++){
        uint64_t* pml4_entry = (uint64_t*)((void*)PML4_READ + pml4_index*8);
        if(*(uint64_t*)pml4_entry & PT_P){
            LOG("pml4 entry present for index %d",pml4_index);
            uint64_t *child_pdp = get_zeroed_page();
            phys = ((uint64_t)pa(child_pdp) & ~0xFFF) | PT_P | PT_W | PT_U;
            child_vpml4[pml4_index] = phys;
            for(pdp_index=0;pdp_index<512;pdp_index++){
                uint64_t* pdp_entry = ((void*)PDP_READ + (pml4_index<<12) + pdp_index*8 );
                if(*(uint64_t*)pdp_entry & PT_P){
                    uint64_t *child_pd = get_zeroed_page();
                    phys = ((uint64_t)pa(child_pd) & ~0xFFF) | PT_P | PT_W | PT_U;
                    child_pdp[pdp_index] = phys;
                    for(pd_index=0;pd_index<512;pd_index++){
                        uint64_t* pd_entry = ((void*)PD_READ + (pml4_index<<21) + (pdp_index<<12) + pd_index*8 );
                        if(*(uint64_t*)pd_entry & PT_P){
                           uint64_t *child_pt = get_zeroed_page();
                           phys = ((uint64_t)pa(child_pt) & ~0xFFF) | PT_P | PT_W | PT_U;
                            child_pd[pd_index] = phys;
                            for(pt_index=0;pt_index<512;pt_index++){
                                uint64_t* pt_entry = ((void*)PT_READ + (pml4_index<<30) + (pdp_index<<21) + (pd_index<<12) + (pt_index*8) );
                                if(*(uint64_t*)pt_entry & PT_P){
                                    LOG("before value of pt_entry %lx\n", *pt_entry);
                                    *pt_entry = ((uint64_t)(*pt_entry) & ~0xfff) | PT_P | PT_U;
                                    child_pt[pt_index] = *pt_entry;
                                    LOG("child_pt value %lx",child_pt[pt_index]);
                                    alloc_phys_page((uint64_t*)(*pt_entry & ~0xFFF));   
                                
                                }

                            }
                        }
                    }
                }
            }
        }
    }
    child_vpml4[510] = child_phys_pml4 | PT_P | PT_W;
    uint64_t* pml4_entry = (uint64_t*)((void*)PML4_READ + 511*8);
    child_vpml4[511] = *pml4_entry;
}

void map_parent_page_hierarchy(uint64_t *child_vpml4) {
    uint64_t child_phys_pml4 = (uint64_t)pa(child_vpml4);
    child_phys_pml4 = child_phys_pml4 & ~0xFFF;

    int pml4_index =0;
    int pdp_index = 0;
    int pd_index = 0;
    int pt_index = 0;

    for(pml4_index= 0;pml4_index<512;++pml4_index) {
        uint64_t* pml4_entry = ((void*)PML4_READ + pml4_index*8);       
        if(*(uint64_t*)pml4_entry & PT_P) {

            child_vpml4[pml4_index] = *pml4_entry;
            uint64_t *child_pdp = get_zeroed_page();
            for(pdp_index=0;pdp_index<512;++pdp_index) {
                uint64_t* pdp_entry = ((void*)PDP_READ + (pml4_index<<12) + pdp_index*8 );
                if(*(uint64_t*)pdp_entry & PT_P) {
                    child_pdp[pdp_index] = *pdp_entry;
                    uint64_t *child_pd = get_zeroed_page();
                    for(pd_index=0;pd_index<512;++pd_index) {
                        uint64_t* pd_entry = ((void*)PD_READ + (pml4_index<<21) + (pdp_index<<12) + pd_index*8 );
                        if(*(uint64_t*)pd_entry & PT_P) {
                            child_pd[pd_index] = *pd_entry;
                            uint64_t *child_pt = get_zeroed_page();
                            for(pt_index=0;pt_index<512;++pt_index) {
                                uint64_t* pt_entry = ((void*)PT_READ + (pml4_index<<30) + (pdp_index<<21) + (pd_index<<12) + (pt_index*8) );                                
                                if(*(uint64_t*)pt_entry & PT_P) {
                                    child_pt[pt_index] = *pt_entry;
                                    alloc_phys_page((uint64_t*)(*pt_entry & ~0xFFF));   
                                }
                            }
                        }
                    }
               }
            }
        }
    }
    child_vpml4[510] = child_phys_pml4 | PT_P | PT_W |PT_U;
}

void free_page_hierarchy(uint64_t *vpml4) {
    uint64_t phys_pml4 = (uint64_t)pa(vpml4);
    phys_pml4 = phys_pml4 & ~0xFFF;

    int pml4_index =0;
    int pdp_index = 0;
    int pd_index = 0;
    int pt_index = 0;

    for(pml4_index= 0;pml4_index<512;++pml4_index) {
        uint64_t* pml4_entry = ((void*)PML4_READ + pml4_index*8);       
        if(*(uint64_t*)pml4_entry & PT_P & PT_U) {
            for(pdp_index=0;pdp_index<512;++pdp_index) {
                uint64_t* pdp_entry = ((void*)PDP_READ + (pml4_index<<12) + pdp_index*8 );
                if(*(uint64_t*)pdp_entry & PT_P) {
                    for(pd_index=0;pd_index<512;++pd_index) {
                        uint64_t* pd_entry = ((void*)PD_READ + (pml4_index<<21) + (pdp_index<<12) + pd_index*8 );
                        if(*(uint64_t*)pd_entry & PT_P) {
                            for(pt_index=0;pt_index<512;++pt_index) {
                                uint64_t* pt_entry = ((void*)PT_READ + (pml4_index<<30) + (pdp_index<<21) + (pd_index<<12) + (pt_index*8) );
                                if(*(uint64_t*)pt_entry & PT_P) {
                                    free_phys_page((uint64_t*)(*pt_entry & ~0xFFF));    
                                }
                            }
                            free_phys_page((uint64_t *)(*pd_entry & ~0xFFF));
                        }
                    }
                    free_phys_page((uint64_t *)(*pdp_entry & ~0xFFF));
                }
            }
            free_phys_page((uint64_t *)(*pml4_entry & ~0xFFF));
        }
    }
}

uint64_t pa(uint64_t* va){
    uint64_t pml4_index = PML4X(va);
    uint64_t pdp_index = PDPX(va);
    uint64_t pd_index = PDX(va);
    uint64_t pt_index = PTX(va);
    uint64_t* pt_entry = ((void*)PT_READ + (pml4_index<<30) + (pdp_index<<21) + (pd_index<<12) + (pt_index*8) );
    return *(pt_entry) & ~0xFFF;
}

void set_write(uint64_t* va){
    uint64_t pml4_index = PML4X(va);
    uint64_t pdp_index = PDPX(va);
    uint64_t pd_index = PDX(va);
    uint64_t pt_index = PTX(va);
    uint64_t* pt_entry = ((void*)PT_READ + (pml4_index<<30) + (pdp_index<<21) + (pd_index<<12) + (pt_index*8) );
    *(pt_entry) = *pt_entry | PT_W;
}

uint64_t* get_free_page(){
    uint64_t* result=0;
    uint64_t* phys_addr=NULL;
    // Initialize nextfree if this is the first time.
    // 'end' is a magic symbol automatically generated by the linker,
    // which points to the end of the kernel's bss segment:
    // the first virtual address that the linker did *not* assign
    // to any kernel code or global variables.
    if (!nextfree) {
        nextfree = ROUNDUP((uint64_t *)(KERNMEM | (uint64_t)PHYSFREE), PAGE_SIZE);
    }
    //if(n < 0) panic("boot_alloc: attempted to allocate a negative amount of memory.");
    // Allocate ROUNDUP(n/PGSIZE) pages of memory and return the start address
    result = nextfree;
    
    phys_addr = alloc_phys_page(NULL);
//printf("in get free page phys addr is %lx \n",phys_addr); 
    map(phys_addr,result,1,0);
    nextfree = ROUNDUP((void*)nextfree+PAGE_SIZE, PAGE_SIZE);
    // Check to make sure there is still available memory
    //if((int)nextfree > KERNBASE+npages*PAGE_SIZE) panic("boot_alloc: out of memory.");
    LOG("get_free_page(); physical addr %lx, result %lx\n", phys_addr, result);
    return result;
}

void free_page(uint64_t* virt_addr){
    return;
}

uint64_t* get_zeroed_page(){
    uint64_t* new_page = get_free_page();
    //printf("in get zeroed page %lx \n",new_page);
    memset(new_page,0,PAGE_SIZE);
    return new_page;
}


uint64_t *get_free_pages(uint32_t order)
{
    uint64_t* result=0;
    uint64_t* phys_addr=NULL;
    uint32_t  i;
    
    if(order>20){
        printf("Request for get_free_pages denied!!\n");
        return NULL;
    }
    //order = 1<<order;
    if (!nextfree) {
        nextfree = ROUNDUP((uint64_t *)(KERNMEM | (uint64_t)PHYSFREE), PAGE_SIZE);
    }
    
    result = nextfree;
    for(i=0;i<order;++i){
        phys_addr = alloc_phys_page(NULL);
        map(phys_addr,nextfree,1,0);     
        //printf("get_free_pages(); physical addr %lx, result %lx\n", phys_addr, nextfree);
        nextfree = ROUNDUP((void*)nextfree+PAGE_SIZE, PAGE_SIZE);
    }
    return result;   
}

int is_entry_present(uint64_t* va){
    uint64_t pml4_index = PML4X(va); 
    uint64_t pdp_index = PDPX(va);
    uint64_t pd_index = PDX(va);
    uint64_t pt_index = PTX(va);
    uint64_t* pml4_entry = ((void*)PML4_READ + pml4_index*8);
    
    if(*(uint64_t*)pml4_entry & PT_P){
        LOG("entry is present in pml4,this means pdp is present\n");
    }
    else{
        return 0;
    }

    uint64_t* pdp_entry = ((void*)PDP_READ + (pml4_index<<12) + pdp_index*8 );
    if(*(uint64_t*)pdp_entry & PT_P){
        LOG("entry is present in pdp,this means pd is present\n");
    }
    else{
        return 0;   
    }

    uint64_t* pd_entry = ((void*)PD_READ + (pml4_index<<21) + (pdp_index<<12) + pd_index*8 );
    if(*(uint64_t*)pd_entry & PT_P){
        LOG("entry is present in pd,this means pt is present\n");
    }
    else{
        return 0;
    }

    uint64_t* pt_entry = ((void*)PT_READ + (pml4_index<<30) + (pdp_index<<21) + (pd_index<<12) + (pt_index*8) );
    if(*(uint64_t*)pt_entry & PT_P){
    }
    else{
        return 0;
    }
    return 1;
}

void unmap(uint64_t* va){
    uint64_t pml4_index = PML4X(va); 
    uint64_t pdp_index = PDPX(va);
    uint64_t pd_index = PDX(va);
    uint64_t pt_index = PTX(va);
    uint64_t* pml4_entry = ((void*)PML4_READ + pml4_index*8);
    
    if(*(uint64_t*)pml4_entry & PT_P){
        LOG("entry is present in pml4,this means pdp is present\n");
    }
    else{
        return;
    }
    uint64_t* pdp_entry = ((void*)PDP_READ + (pml4_index<<12) + pdp_index*8 );
    if(*(uint64_t*)pdp_entry & PT_P){
        LOG("entry is present in pdp,this means pd is present\n");
    }
    else{
        return;   
    }

    uint64_t* pd_entry = ((void*)PD_READ + (pml4_index<<21) + (pdp_index<<12) + pd_index*8 );
    if(*(uint64_t*)pd_entry & PT_P){
        LOG("entry is present in pd,this means pt is present\n");
    }
    else{
        return;
    }

    uint64_t* pt_entry = ((void*)PT_READ + (pml4_index<<30) + (pdp_index<<21) + (pd_index<<12) + (pt_index*8) );
    if(*(uint64_t*)pt_entry & PT_P){
        *(uint64_t*)pt_entry = (uint64_t)0x0;
    }
    else{
        return;
    }
    return;
}
