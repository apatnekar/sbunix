#include <sys/gdt.h>
#include <sys/interrupt.h>
#include <sys/kprintf.h>
#include <sys/process.h>

void insert_task_in_current_task_list(struct task_struct *ts){
	ts->next = current_task->next;
	current_task->next = ts;
}

void check_sleep_list(){
	LOG("In check list\n");
	struct task_struct *cur = (struct task_struct*)sleep_task;
	struct task_struct *prev = NULL; //sleep_task;
	struct task_struct *ts = NULL;//remove_task_from_sleep_list(cur);
	
	while(cur != NULL){
		if(cur->sleep_time <= timer_ticks){
			if(prev == NULL){
				sleep_task = sleep_task->next;
				ts = cur;
				cur = (struct task_struct*)sleep_task;
				ts->next = NULL;
				ts->next = current_task->next;
				current_task->next = ts;
			}
			else{
				ts = cur;
				prev->next = cur->next;
				cur = cur->next;
				ts->next = current_task->next;
				current_task->next = ts;
			}
		}
		else{
			prev = cur;
			cur = cur->next;
		}
	}
	outb(0x20, 0x20);
	return; //no task to be inserted in the current_task_list
}

void timer_handler() {
    timer_ticks++;
    if (timer_ticks % 18 == 0) {
       	print_time(timer_ticks/18);
    	if(sleep_task != NULL){
    		//printf("Sleep task is not null\n");
    		check_sleep_list();
    	}
	schedule();
	LOG("return from hello time");
    }
}

void timer_install() {
    irq_install_handler(0, timer_handler);
}

/*
void timer_wait(int ticks) {
  uint64_t eticks;

   eticks = timer_ticks + ticks;
    while(timer_ticks < eticks);
}*/

