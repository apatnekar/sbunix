.text
.global irq0
.global irq1
.extern irq_handler

irq_common_stub:
    pushq %rdi
    pushq %rax
    pushq %rbx
    pushq %rcx
    pushq %rdx
    pushq %rbp
    pushq %rsi
    pushq %r8
    pushq %r9
    movq %rsp,%rdi
    call irq_handler
    popq %r9
    popq %r8
    popq %rsi
    popq %rbp
    popq %rdx
    popq %rcx
    popq %rbx
    popq %rax
    popq %rdi
    addq $16 ,%rsp 
    iretq

irq0:
    cli
    pushq $0       # error code
    pushq $32       # IRQ Number
    jmp irq_common_stub

irq1:
    cli
    pushq $0       # error code
    pushq $33       # IRQ Number
    jmp irq_common_stub

