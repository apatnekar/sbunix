#include <defs.h>
#include <sys/process.h>
#include <sys/kprintf.h>
#include <sys/interrupt.h>
#include <sys/pages.h>
#include <sys/kern_macros.h>
#include <sys/slob_allocator.h>
#include <sys/virt_mem_mgr.h>
#include <sys/elf.h>
#include <sys/loader.h>
#include <sys/gdt.h>
#include <sys/kutil.h>
#include <sys/fs.h>
#include <sys/inittarfs.h>

#define TOTAL_NUM_OF_USER_PROCESSES 50

uint64_t next_pid = 0;
volatile uint32_t CURRENT_USER_PROCESSES = 0;

void idle_process() {
// DO NOT COMMENT THIS WHILE LOOP
  while(1){
//	int i;    
   // for(i=0;i<2000;++i)
         //   printf("I am idle, [pid: %d]\n", getpid());
        schedule();
    }
}

void print_current_task_list() {
    struct task_struct *prev =  (struct task_struct*)current_task;
    printf("Current task list: ");
    while(prev->next != current_task) {
        printf("pid: %d, ", prev->pid);
        prev = prev->next;
    }   
    printf("pid: %d.\n", prev->pid);
}

void init_tasking()
{
    // Rather important stuff happening, no interrupts please!
//    asm volatile("cli");
    stop_task = NULL;
    wait_task = NULL;
    if(CURRENT_USER_PROCESSES++ > TOTAL_NUM_OF_USER_PROCESSES){
        LOG("Can't run more than %d processes!!! \n", TOTAL_NUM_OF_USER_PROCESSES);
        while(1);
    }

    // Initialise the first task (kernel task)
    current_task = kmalloc(sizeof(struct task_struct),1);
    //LOG("sizeof kmalloc: %d\n", sizeof(struct task_struct));
    current_task->pid = next_pid++;
    current_task->ppid = 0;
    current_task->task_state = RUNNABLE;
    current_task->rsp = (uint64_t)( (char*)get_free_page() + PAGE_SIZE - 16 );
    current_task->rip = (uint64_t)&idle_process;
    //current_task->page_directory = current_directory;
    current_task->next =  (struct task_struct*)current_task;
    current_task->initial_kernel_stack = current_task->rsp;
    
    current_task->stack_limit = STACK_LIMIT;
    current_task->running_proc_limit = TOTAL_NUM_OF_USER_PROCESSES;
    current_task->kernel_stack = current_task->rsp;
    current_task->vpml4 = kern_pgdir;
    current_task->pml4 = (uint64_t)pa(kern_pgdir);

    strcpy((char *)current_task->pwd,"/");

    sleep_task = NULL;
    wait_task = NULL;
    wait_for_read = NULL;
//    asm volatile("sti");
}

struct task_struct* create_process(char* filename){
    if(CURRENT_USER_PROCESSES++ > TOTAL_NUM_OF_USER_PROCESSES){
        LOG("Can't run more than %d processes!!! \n", TOTAL_NUM_OF_USER_PROCESSES);
        while(1);    
    }

    struct task_struct *ts = load_elf(filename,NULL);
    if(ts == NULL){
        LOG("<<<<<<<<<    Binary file not found !!   >>>>>>>>>\n");
        while(1);
    }

    ts->vpml4 = get_zeroed_page(); 
    memcpy(ts->vpml4,kern_pgdir,PAGE_SIZE);
    uint64_t phys_pml4 = (uint64_t)pa(ts->vpml4); // TO DO: cr3 needs to be written
    phys_pml4 = phys_pml4  & ~0xFFF;
    ts->pml4 = phys_pml4;
    ts->pid = next_pid++;
    ts->sleep_time = 0;
    ts->task_state = RUNNABLE;
    ts->wait_pid = -1;
    LOG("physical pml4 is %lx\n",phys_pml4);
    LOG("task state is %d\n", ts->task_state);
    ts->vpml4[510] = phys_pml4 | PT_P | PT_W;
    ts->kernel_stack = ts->initial_kernel_stack =  ( (uint64_t)get_free_page() + PAGE_SIZE - 16);
    ts->rsp = ts->rsp - 16;
    ts->stack_limit = STACK_LIMIT;
    ts->running_proc_limit = TOTAL_NUM_OF_USER_PROCESSES;
    strcpy((char *)ts->pwd,"/");
    return ts;
}

void switch_to_user_mode(struct task_struct *ts_obj) {
    set_kernel_stack(ts_obj->kernel_stack);
    LOG("inside switch to user mode: %lx\n", ts_obj->rip);
    uint64_t pml4 = ts_obj->pml4 & ~0xFFF;
    LOG("pml4 is %lx\n",pml4);
    
    __asm__ __volatile__ (
        "cli;"
        "movq %0, %%cr3;"
        "mov $0x23, %%ax;"
        "mov %%ax, %%ds;"
        "mov %%ax, %%es;"
        "mov %%ax, %%fs;"
        "mov %%ax, %%gs;"

        "movq %1, %%rax;"
        "pushq $0x23;"
        "pushq %%rax;"
        "pushfq;"
        "popq %%rax;"
        "orq $0x200, %%rax;"
        "pushq %%rax;"
        "pushq $0x1B;"
        "pushq %2;"
        "movq $0x0, %%rdi;"
        "movq $0x0, %%rsi;"
        "iretq;"
        ::"r"(pml4), "r"(ts_obj->rsp), "r"(ts_obj->rip)
    );
}

void switch_to(struct task_struct *next, struct task_struct *prev) {
LOG("switch_to\n");
    //printf("Task now running has pid: %d, rip is %lx, rsp is %lx\n", next->pid,next->rip,next->kernel_stack);
    //print_current_task_list();
    set_kernel_stack(next->initial_kernel_stack);
    __asm__ __volatile__ (
        "cli;"
        "movq %%rsp, (%1);"     // save RSP
        "movq $1f, %0;"         // save RIP
        "movq %2, %%rsp;"       // load next->stack var in rsp
        "movq %4, %%cr3;"       // load next->pml4 into cr3
        "pushq %3;"             // restore RIP so as to continue execution in next task
        "retq;"                 // Switch to new task
        "1:\t"
        :"=g"(prev->rip)
        :"r"(&(prev->kernel_stack)), "r"(next->kernel_stack), "r"(next->rip), "r"(next->pml4)
    );
 //printf("Hello is resuming after idle..\n");
}

void schedule() {
    //printf("inside schedule current_task is %lx \n",current_task->pid);
    struct task_struct *prev =  (struct task_struct*)current_task; 
    current_task = current_task->next;
    if(current_task != prev)
        switch_to( (struct task_struct*)current_task,prev);
    LOG("returing from schedule %lx\n",current_task->pid);
}

int do_getpid() {
    return current_task->pid;
}

int do_getpwd(char *buffer) {
    strcpy(buffer, (char *)current_task->pwd);
    return 1;
}

int do_setpwd(char *path) {
    strcpy((char *)current_task->pwd, path);
    return 1;
}

void handle_fatal_error(char* err_msg){
    printf("%s\n",err_msg);
    do_exit();
}

int64_t do_fork() {
    if(CURRENT_USER_PROCESSES++ >= current_task->running_proc_limit){//TOTAL_NUM_OF_USER_PROCESSES){
       CURRENT_USER_PROCESSES--;
       //printf("Can't run more than %d processes, process's running_proc_limit %d!!! \n", CURRENT_USER_PROCESSES,current_task->running_proc_limit);
       return -1;
    }

    struct task_struct *temp,*parent_ts = NULL;
    uint64_t ps;
    struct task_struct *child_task = copy_task_struct( (struct task_struct*)current_task);
    //	printf("child %d parent %d \n",child_task->pid,child_task->ppid);
    LOG("child->rsp: %lx, current->r->userrsp: %lx\n", child_task->rsp, current_task->r->userrsp);

    temp = current_task->next;
    current_task->next = child_task;
    child_task->next = temp;
    //child_task->task_state = RUNNABLE;
    child_task->initial_kernel_stack = (uint64_t)( (char *)get_free_page() + PAGE_SIZE - 16 );
    child_task->kernel_stack = child_task->initial_kernel_stack;
    memcpy((void*)ROUNDDOWN(child_task->initial_kernel_stack ,PAGE_SIZE), (void*)ROUNDDOWN(current_task->initial_kernel_stack ,PAGE_SIZE),PAGE_SIZE);
    child_task->vpml4 = (uint64_t *)get_zeroed_page();//current_task->pml4;    // UP DATE
    child_task->pml4 = pa(child_task->vpml4) & ~0xFFF;
    set_pages_readonly(child_task->vpml4);
    parent_ts = (struct task_struct *)current_task;
    foreground_task = child_task;
    //printf("$$$$$ parent_task pid is %d child task pid is %d \n",parent_ts->pid , child_task->pid); 
    flush_TLB();
    __asm__ __volatile__ (
        "movq %%rsp, %1;"           //copy RSP
        "movq $2f, %0;"             // restore RIP so as to continue execution in next task
        "2:\t"
        :"=g"(child_task->rip),"=m"(ps)
    );
     
    if(current_task == parent_ts) {
//	printf("parent exiting %lx, addr %lx\n",parent_ts->pid, parent_ts);        
	child_task->kernel_stack = child_task->initial_kernel_stack - (parent_ts->initial_kernel_stack - ps);
        return child_task->pid;
    }
    else {
//	printf("child exiting %lx, addr %lx\n",child_task->pid,child_task);
        outb(0x20, 0x20);
        return 0;
    }
}

void free_task_struct(struct task_struct * exit_task){

    LOG("in free task struct\n");
    int i;
    //Freeing the VMA strcuts
    struct vm_area_struct *mmap_cur = exit_task->mm->mmap;

    for(i=0;i<MAX_FD;i++){
        struct file_descriptor* f = (struct file_descriptor*)exit_task->fd[i];
        if(f!=NULL){
            if(--(f->ref)==0)
                kfree((uint64_t*)f,sizeof(struct file_descriptor));
        }
    }
    
    while(mmap_cur != NULL) {
        if(mmap_cur->vm_file != NULL){
            kfree((uint64_t *)mmap_cur->vm_file,sizeof(struct file));
            mmap_cur->vm_file = NULL;
            LOG("inside file vma freed task struct\n");
        }
        struct vm_area_struct *to_free_vma = mmap_cur; 
        mmap_cur = mmap_cur->vm_next;
            
        kfree((uint64_t *)to_free_vma,sizeof(struct vm_area_struct));
        to_free_vma = NULL;
    }
    //freeing the mm struct
            
    kfree((uint64_t *)exit_task->mm,sizeof(struct mm_struct));
    exit_task->mm = NULL;

    LOG("end of free task struct\n");
}

void free_exec_task_struct(struct task_struct * exec_task){

    LOG("in free exec task struct\n");
    //Freeing the VMA strcuts
    struct vm_area_struct *mmap_cur = exec_task->mm->mmap;

    while(mmap_cur != NULL) {
        if(mmap_cur->vm_file != NULL){
            kfree((uint64_t *)mmap_cur->vm_file,sizeof(struct file));
            mmap_cur->vm_file = NULL;
            LOG("inside file vma freed task struct\n");
        }
        struct vm_area_struct *to_free_vma = mmap_cur; 
        mmap_cur = mmap_cur->vm_next;
            
        kfree((uint64_t *)to_free_vma,sizeof(struct vm_area_struct));
        to_free_vma = NULL;
    }
    //freeing the mm struct
            
    kfree((uint64_t *)exec_task->mm,sizeof(struct mm_struct));
    exec_task->mm = NULL;

    LOG("end of free task struct\n");
}

int64_t do_execve(char *filename, char *argv[]){
    int i;
    //printf("In process.c.... do_execvee: %d\n", current_task->pid);// - filename: %p, argv: %p\n", (uint64_t)filename, (uint64_t)argv);
    //printf("kern_pgdir: %lx, 510: %lx, 511: %lx\n", kern_pgdir, kern_pgdir[510], kern_pgdir[511]);
    //free_task_struct();
    uint64_t argc = 0;

    //printf("size of char* argv[] %lx, argv loc %lx\n",sizeof(argv),argv[0]);
    //printf("value at argv is %s\n",*(argv));
    
    i=search_rootnodes(filename);
    if(i==-1 || root_nodes[i].flags==FS_DIRECTORY)
        return -1;

    //printf("1. ^^^^^^^^^^^^^^^ here \n");
    char copy_argv[5][24];
    for (i = 0; i < 5; ++i)
        copy_argv[i][0] = '\0';

    //strcpy(copy_argv[argc],filename);
    //printf("2. ^^^^^^^^^^^^^^^ here \n");
    
    if(argv != NULL){
        while(argv[argc] != NULL){
               strcpy(copy_argv[argc],argv[argc]);
               argc++;
        }
    }
    
    free_exec_task_struct( (struct task_struct*)current_task);
    free_page_hierarchy((uint64_t*) current_task->vpml4);
     //printf("after freeing structs\n");
    load_elf(filename, (struct task_struct*)current_task);
     //print_parsed_elf((struct task_struct*)current_task);
    memcpy(current_task->vpml4,kern_pgdir,PAGE_SIZE);
    current_task->vpml4[510] = current_task->pml4 | PT_P | PT_W;

    current_task->kernel_stack = current_task->initial_kernel_stack = (uint64_t)get_free_page() + PAGE_SIZE - 16;
    current_task->rsp = current_task->rsp -16;
    //strcpy((char *)current_task->pwd,"/bin/");

    set_kernel_stack(current_task->initial_kernel_stack);
    void *argc_ptr = (void *)(USER_STACK_ADDR - 16 - sizeof(copy_argv));
    __asm__ __volatile__ ( "movq %0, %%cr3;" ::"r"(current_task->pml4));
    memcpy(argc_ptr, (void *)copy_argv, sizeof(copy_argv));
    for(i=argc;i>0;i--){
        *(uint64_t*)(argc_ptr - 8*i) = (uint64_t)argc_ptr + (argc-i)*24;
        LOG("argv in user is %s\n",((char*)(*((uint64_t*)(argc_ptr - 8*i)))));
    }
    argc_ptr = argc_ptr- 8*argc;
    current_task->rsp = (uint64_t)argc_ptr;
    //printf("pml4: %lx, rsp: %lx, rip: %lx, argc_ptr %lx\n", current_task->pml4, current_task->rsp, current_task->rip, argc_ptr);
    __asm__ __volatile__ (
        "cli;"
        "movq %3, %%rsp;"
        "mov $0x23, %%ax;"
        "mov %%ax, %%ds;"
        "mov %%ax, %%es;"
        "mov %%ax, %%fs;"
        "mov %%ax, %%gs;"

        "movq %1, %%rax;"
        "pushq $0x23;"
        "pushq %%rax;"
        "pushfq;"
        "popq %%rax;"
        "orq $0x200, %%rax;"
        "pushq %%rax;"
        "pushq $0x1B;"
        "pushq %2;"
        "movq %4, %%rsi;"
        "movq %5, %%rdi;"
        "iretq;"
        ::"r"(current_task->pml4), "r"(current_task->rsp), "r"(current_task->rip), "r"(current_task->kernel_stack),
      "r"(argc_ptr), "r"(argc) 
    );

    LOG("end of do_execve. Should not come here\n");
    return -1;
}

void flush_TLB(){
    __asm__ __volatile__(
        "movq %%cr3, %%rax;"
    "movq %%rax,%%cr3;"
        ::"r"(current_task->pml4)
    :"rax"
    );
}

struct task_struct *remove_current_task_struct() {
    struct task_struct *prev =  (struct task_struct*)current_task;
    struct task_struct *save_current_task =  (struct task_struct*)current_task;
    while(prev->next != current_task)
        prev = prev->next;
    prev->next = current_task->next;
    current_task->next = NULL;
    return save_current_task;
}

struct task_struct *insert_task_struct_in_list(struct task_struct *task, struct task_struct *list) {
    struct task_struct *save_stop_task_head = list;
    
    if(list == NULL) {
        list = task;
        save_stop_task_head = task;
    }
    else {
        while(list->next != NULL) {
            list = list->next;
        }
        list->next = task;
        list = list->next;
    }
    list->next = NULL;
    return save_stop_task_head;
}

struct task_struct *get_parent_task_from_wait_task_list() {
    volatile struct task_struct *wait_task_head = wait_task;
    volatile struct task_struct *prev = wait_task;
    if(wait_task_head == NULL) { //the wait_task_liat is empty, so the child has no process
        current_task->ppid = 1;
        return NULL;
    }
    else {
        if(wait_task_head->pid == current_task->ppid){ //checking the first node of wait_task list
            if(current_task->pid == wait_task_head->wait_pid || wait_task_head->wait_pid == -1){
                wait_task = wait_task->next;
                wait_task_head->next = NULL;
                return (struct task_struct*)wait_task_head;
            }
            else{
                current_task->ppid = 1;
                return NULL;
            }
        }
        while(wait_task_head != NULL){ //checking the rest of the nodes of the wait task_task list
               if(wait_task_head->pid == current_task->ppid) {
                    if(current_task->pid == wait_task_head->wait_pid || wait_task_head->wait_pid == -1){
                        prev->next = wait_task_head->next;
                        wait_task_head->next = NULL;
                        return (struct task_struct*)wait_task_head;
                    }
                }
                prev = wait_task_head;
                wait_task_head = wait_task_head->next;
            }
        }
    current_task->ppid = 1;
    return NULL; //there was a wait task list but did not find the child's parent
}

struct task_struct *get_child_task_from_stop_task_list() {
    struct task_struct *stop_task_head = (struct task_struct*)stop_task;
    if(stop_task_head == NULL) { //the wait_task_liat is empty, so the child has no process
        return NULL;
    }
    else {
        if(current_task->wait_pid == -1){ //this is the case that handles wait() system call
            if(stop_task_head->ppid == current_task->pid) {//the first node in the wait task list is the parent
                stop_task = stop_task->next;
                stop_task_head->next = NULL;
                return stop_task_head;
            }
            struct task_struct *prev = stop_task_head;
            while(stop_task_head != NULL) {
                if(stop_task_head->ppid == current_task->pid) {
                    prev->next = stop_task_head->next;
                    stop_task_head->next = NULL;
                    return stop_task_head;
                }
                prev = stop_task_head;
                stop_task_head = stop_task_head->next;
            }
        }
        else{ //this is the case that handles waitpid() system call
            if(stop_task_head->ppid == current_task->pid && current_task->wait_pid == stop_task_head->pid){ //the first node in thewait task list is parent
                stop_task = stop_task->next;
                stop_task_head->next = NULL;
                return stop_task_head;
            }
            struct task_struct *prev = stop_task_head;
            while(stop_task_head != NULL) {
                if(stop_task_head->ppid == current_task->pid && current_task->wait_pid == stop_task_head->pid) {
                    prev->next = stop_task_head->next;
                    stop_task_head->next = NULL;
                    return stop_task_head;
                }
                prev = stop_task_head;
                stop_task_head = stop_task_head->next;
            }
        }
    }
    return NULL; //there was a wait task list but did not find the child's parent
}

uint64_t do_exit() {
    //print_current_task_list();
    //printf("Reached do_exit for pid ppid : %d %d \n", current_task->pid, current_task->ppid);
    CURRENT_USER_PROCESSES--;
    //print_current_task_list();
    volatile struct task_struct *current_task_next = current_task->next;
    volatile struct task_struct *exit_task = remove_current_task_struct();
    exit_task->next = (struct task_struct*)stop_task;
    stop_task = exit_task;
    struct task_struct *parent_task = get_parent_task_from_wait_task_list();
    if(parent_task != NULL) {
        LOG("In exit making parent task as current \n");
        parent_task->next = current_task_next->next;
        current_task_next->next = parent_task;
        current_task = parent_task;
	//cur_task_copy = current_task;
    }
    else{
        LOG("In exit making next task as current\n");
        current_task = current_task_next;
	//cur_task_copy = current_task;
    }
//printf("current task address is %lx\n",current_task);
//	print_parsed_elf((struct task_struct*)current_task);
//        print_current_task_list();
//	printf("current task %lx exit_task %lx\n",current_task,exit_task);
//    free_task_struct((struct task_struct*)exit_task);
    //LOG("In exit chaging to current task pid %lx rsp %lx, rip %lx\n",current_task->pid,current_task->kernel_stack,current_task->rip); 
    switch_to((struct task_struct*)current_task,(struct task_struct*)exit_task);
   /* set_kernel_stack(current_task->initial_kernel_stack);
   //printf("shell will resume now\n");
   __asm__ __volatile__ (
	"movq %0, %%rsp;"       // load next->stack var in rsp
        "movq %2, %%cr3;"       // load next->pml4 into cr3
     	"pushq %1;"             // restore RIP so as to continue execution in next task        
	"retq;"                 // Switch to new task
        :: "r"(current_task->kernel_stack), "r"(current_task->rip), "r"(current_task->pml4)
	:"%rax"
    );*/
printf("shd never reach here \n");
    return 0;
}

int64_t do_waitpid(int64_t wait_pid) {

    LOG("inside do_waitpid: %lx for task  %lx\n", current_task->pid, wait_pid);
    int flag = 0;//returns -1 if the call to waitpid is invalid
    struct task_struct *child_process = current_task->next;
    
    if(wait_pid == current_task->pid){ //if the process tries to wait on itself
        LOG("Oh come on! I cannot wait on myself!!\n");
    return -1;
    }

    while(child_process != current_task) { //search for the child process in the current task list
        if(wait_pid == child_process->pid && child_process->ppid == current_task->pid){
            flag = 1;
            break;
        }
        child_process = child_process->next;
    }

    child_process = (struct task_struct*)wait_task;
    while(child_process != NULL) { //search for the child process in the stop task list
        if(wait_pid == child_process->pid && child_process->ppid == current_task->pid){
            flag = 1;
            break;
        }
        child_process = child_process->next;
    }

    child_process = (struct task_struct*)sleep_task;
    while(child_process != NULL) { //search for the child process in the sleep task list
        if(wait_pid == child_process->pid && child_process->ppid == current_task->pid){
            flag = 1;
            break;
        }
        child_process = child_process->next;
    }

    if(flag == 0){ //invalid wait_pid, hence return from do_waitpid
        LOG("%d is not the child of %d, therefore can't waitpid() on it\n",wait_pid, current_task->pid);
        return -1;
    }

    current_task->wait_pid = wait_pid;
    
    volatile struct task_struct *current_task_next = current_task->next;
    volatile struct task_struct *wait_task_local = remove_current_task_struct(); 
    wait_task_local->next = (struct task_struct*)wait_task;
    wait_task = wait_task_local;
    current_task = current_task_next;

    switch_to((struct task_struct*) current_task, (struct task_struct*) wait_task_local);
    struct task_struct *child_task = get_child_task_from_stop_task_list();//clean up the child
    if(child_task != NULL){
        kfree((uint64_t *)child_task, sizeof(struct task_struct));
        child_task = NULL;
    }
    
    return 0;
}

/*
    set_kernel_stack(current_task_next->initial_kernel_stack);
    __asm__ __volatile__ (
        "movq %%rsp, (%1);"     // save RSP
        "movq $5f, %0;"         // save RIP
        "movq %2, %%rsp;"       // load next->stack var in rsp
        "movq %4, %%cr3;"       // load next->pml4 into cr3
        "pushq %3;"             // restore RIP so as to continue execution in next task
        "retq;"                 // Switch to new task
        "5:\t"
	:"=g"(wait_task_local->rip)
        :"r"(&(wait_task_local->kernel_stack)), "r"(current_task_next->kernel_stack), "r"(current_task_next->rip), "r"(current_task_next->pml4)
    );
*/	    //printf("do_waitpid: parent has resumed %p, %p \n",current_task,cur_task_copy);//while(1);

//	current_task = (struct task_struct *)cur_task_copy;
    //printf("do_waitpid: parent has resumed %p, %p ,%d\n",current_task,cur_task_copy,current_task->pid); 

int64_t do_wait(){

    LOG("inside do_wait: %lx \n", current_task->pid);
    int flag = 0;//returns -1 if the call to waitpid is invalid
    struct task_struct *child_process = current_task->next;
    
    while(child_process != current_task) { //search for the child process in the current task list
        if(child_process->ppid == current_task->pid){
            flag = 1;
            break;
        }
        child_process = child_process->next;
    }

    child_process = (struct task_struct*)wait_task;
    while(child_process != NULL) { //search for the child process in the stop task list
        if(child_process->ppid == current_task->pid){
            flag = 1;
            break;
        }
        child_process = child_process->next;
    }

    child_process = (struct task_struct*)sleep_task;
    while(child_process != NULL) { //search for the child process in the sleep task list
        if(child_process->ppid == current_task->pid){
            flag = 1;
            break;
        }
        child_process = child_process->next;
	}

    if(flag == 0){ //invalid wait, hence return from do_waitpid
        LOG("No child to wait on for %d\n",current_task->pid);
        return -1;
    }

    current_task->wait_pid = -1;
    
    volatile struct task_struct *current_task_next = current_task->next;
    volatile struct task_struct *wait_task_local = remove_current_task_struct(); 
    wait_task_local->next = (struct task_struct*)wait_task;
    wait_task = wait_task_local;
    current_task = current_task_next;

    switch_to((struct task_struct*)current_task, (struct task_struct*)wait_task_local);
    LOG("do_wait: parent has resumed %d\n", current_task->pid);
    struct task_struct *child_task = get_child_task_from_stop_task_list();//clean up the child
    if(child_task != NULL){
        kfree((uint64_t *)child_task, sizeof(struct task_struct));
        child_task = NULL;
    }
    
    return 0;
}

/*    set_kernel_stack(current_task_next->initial_kernel_stack);
    __asm__ __volatile__ (
        "movq %%rsp, (%1);"     // save RSP
        "movq $6f, %0;"         // save RIP
        "movq %2, %%rsp;"       // load next->stack var in rsp
        "movq %4, %%cr3;"       // load next->pml4 into cr3
        "pushq %3;"             // restore RIP so as to continue execution in next task
        "retq;"                 // Switch to new task
        "6:\t"
        :"=g"(wait_task_local->rip)
        :"r"(&(wait_task_local->kernel_stack)), "r"(current_task_next->kernel_stack), "r"(current_task_next->rip), "r"(current_task_next->pml4)
    );
*/

void do_sleep(int64_t seconds){
    //printf("In do sleep %d\n", current_task->pid);
    if(seconds < 0){
        printf("Seconds are lesser than 0, cant sleep\n");
    }
    current_task->sleep_time = timer_ticks + 18*seconds;
    volatile struct task_struct *current_task_next = current_task->next;
    volatile struct task_struct *sleep_task_local = remove_current_task_struct(); 
    sleep_task_local->next = (struct task_struct *)sleep_task;
    sleep_task = sleep_task_local;
    current_task = current_task_next;
    switch_to((struct task_struct*)current_task,(struct task_struct*)sleep_task);

    return;
}
/*
    set_kernel_stack(current_task->initial_kernel_stack);
    __asm__ __volatile__ (
        "movq %%rsp, (%1);"     // save RSP
        "movq $3f, %0;"         // save RIP
        "movq %2, %%rsp;"       // load next->stack var in rsp
        "movq %4, %%cr3;"       // load next->pml4 into cr3
        "pushq %3;"             // restore RIP so as to continue execution in next task
        "retq;"                 // Switch to new task
        "3:\t"
        :"=g"(sleep_task_local->rip)
        :"r"(&(sleep_task_local->kernel_stack)), "r"(current_task->kernel_stack),
         "r"(current_task->rip), "r"(current_task->pml4)
    );
*/
    //printf("do_sleep: process has resumed after sleeping%d\n", current_task->pid);
    //print_current_task_list();


void do_ps(){

	char buff[1024];
	char buff_ppid[1024];
	uint64_t count = 0;
	char *str_end = NULL;
	char *str_end_ppid = NULL;
	char *temp = NULL;

	char *newline = "\n";
	char *tab = "\t";
	char *pidstmt = "pid \t ppid \t state";
	char *runstate = "Running";
	char *waitstate = "Waiting";
	char *sleepstate = "Sleeping";
	
	count = strlen(pidstmt);
//	do_write(1,(char *)newline, 1);
	do_write(1,(char *)pidstmt, count);
	do_write(1,(char *)newline, 1);
//	do_write(1,(char *)newline, 1);
	count = 0;


	volatile struct task_struct *cur = current_task;
	while(cur->next != current_task){
		if(cur->pid != 0){
			str_end = itoa(cur->pid, (char *)buff, 10, 0);		
			str_end_ppid = itoa(cur->ppid, (char *)buff_ppid, 10, 0);		
		
			temp = (char *)buff;
			while(temp != str_end){
				temp++;
				count++;
			}
			do_write(1,(char *)buff, count);
			do_write(1,(char *)tab, 1);
			do_write(1,(char *)tab, 1);
		
			temp = (char *)buff_ppid;
			count = 0;
			while(temp != str_end_ppid){
				temp++;
				count++;
			}
			do_write(1,(char *)buff_ppid, count);
			do_write(1,(char *)tab, 1);
			do_write(1,(char *)tab, 1);
		
			do_write(1,(char *)runstate, 7);
			do_write(1,(char *)newline, 1);
			count = 0;
		}
		cur = cur->next;
	}
	str_end = itoa(cur->pid, (char *)buff, 10, 0);		
	str_end_ppid = itoa(cur->ppid, (char *)buff_ppid, 10, 0);		

	temp = (char *)buff;
	while(temp != str_end){
		temp++;
		count++;
	}
	do_write(1,(char *)buff, count);
	do_write(1,(char *)tab, 1);
	do_write(1,(char *)tab, 1);
	
	temp = (char *)buff_ppid;
	count = 0;
	while(temp != str_end_ppid){
		temp++;
		count++;
	}
	do_write(1,(char *)buff_ppid, count);
	do_write(1,(char *)tab, 1);
	do_write(1,(char *)tab, 1);
		
	do_write(1,(char *)runstate, 7);
	do_write(1,(char *)newline, 1);
	count = 0;

	cur = sleep_task;
	while(cur != NULL){
		str_end = itoa(cur->pid, (char *)buff, 10, 0);		
		str_end_ppid = itoa(cur->ppid, (char *)buff_ppid, 10, 0);		

		temp = (char *)buff;
		while(temp != str_end){
			temp++;
			count++;
		}
		do_write(1,(char *)buff, count);
		do_write(1,(char *)tab, 1);
		do_write(1,(char *)tab, 1);
		
		temp = (char *)buff_ppid;
		count = 0;
		while(temp != str_end_ppid){
			temp++;
			count++;
		}
		do_write(1,(char *)buff_ppid, count);
		do_write(1,(char *)tab, 1);
		do_write(1,(char *)tab, 1);
		
		do_write(1,(char *)sleepstate, 7);
		do_write(1,(char *)newline, 1);
		count = 0;
		cur = cur->next;
	}

	cur = wait_task;
	count = 0;
	while(cur != NULL){
		str_end = itoa(cur->pid, (char *)buff, 10, 0);		
		str_end_ppid = itoa(cur->ppid, (char *)buff_ppid, 10, 0);	

		temp = (char *)buff;
		while(temp != str_end){
			temp++;
			count++;
		}
		do_write(1,(char *)buff, count);
		do_write(1,(char *)tab, 1);
		do_write(1,(char *)tab, 1);
		
		temp = (char *)buff_ppid;
		count = 0;
		while(temp != str_end_ppid){
			temp++;
			count++;
		}
		do_write(1,(char *)buff_ppid, count);
		do_write(1,(char *)tab, 1);
		do_write(1,(char *)tab, 1);
		

		do_write(1,(char *)waitstate, 7);
		do_write(1,(char *)newline, 1);
		count = 0;
		
		cur = cur->next;
	}
//	do_write(1,(char *)newline, 1);
}

struct task_struct *copy_task_struct(struct task_struct *ts_parent){
    struct task_struct *ts_obj;
    int i;
    ts_obj = kmalloc(sizeof(struct task_struct),1);   // allocate memory for task_struct
    ts_obj->pid = next_pid++;               // Process ID.
    ts_obj->ppid = ts_parent->pid;      // Parent Process ID.
    ts_obj->running_proc_limit = ts_parent->running_proc_limit;
    ts_obj->stack_limit = ts_parent->stack_limit;
    ts_obj->rsp = NULL;//ts_parent->rsp;
    ts_obj->rip = NULL;
    ts_obj->rbp = NULL;     // Stack, instruction and base pointers.
    ts_obj->wait_pid = -1;
    ts_obj->pml4 = NULL;// ts_parent->pml4;  // Page directory.
    ts_obj->kernel_stack = NULL;      // Kernel stack location.
    strcpy(ts_obj->pwd, ts_parent->pwd);
    memcpy(&(ts_obj->fd[0]),&(ts_parent->fd[0]),(sizeof(ts_parent->fd[0])*MAX_FD));
    for(i=0;i<MAX_FD;i++){
        struct file_descriptor* f = (struct file_descriptor*)ts_parent->fd[i];
        if(f!=NULL)
            f->ref++;
    }
    //ts_obj->r = NULL;
    ts_obj->next = NULL;   // The next task in a linked list.
    ts_obj->mm = kmalloc(sizeof(struct mm_struct),1);     // allocate memory for mm_struct
    //(*ts_obj).mm = mm_obj;                            // add reference of mm_struct in task_struct
    memcpy(ts_obj->mm,ts_parent->mm,sizeof(struct mm_struct));
    struct vm_area_struct * vma = ts_parent->mm->mmap;
    struct vm_area_struct * child_vma;
    if(vma!=NULL){
        child_vma = kmalloc(sizeof(struct vm_area_struct),1);
        ts_obj->mm->mmap = child_vma;         // first vma
        memcpy(child_vma, vma, sizeof(struct vm_area_struct));
        if(vma->vm_file!=NULL){
            child_vma->vm_file = kmalloc(sizeof(struct file),1);
            memcpy(child_vma->vm_file,vma->vm_file,sizeof(struct file));
        }
    }
    else{
        ts_obj->mm->mmap = NULL;
        return ts_obj;
    }
    vma = vma->vm_next;
    while(vma!=NULL){
        child_vma->vm_next = kmalloc(sizeof(struct vm_area_struct),1);
        memcpy(child_vma->vm_next, vma, sizeof(struct vm_area_struct));
        child_vma = child_vma->vm_next;
        if(vma->vm_file!=NULL){
            child_vma->vm_file = kmalloc(sizeof(struct file),1);
            memcpy(child_vma->vm_file,vma->vm_file,sizeof(struct file));
        }
        vma = vma->vm_next;
    }
    child_vma->vm_next = NULL;
    return ts_obj;
}

void create_stdio_desc(struct task_struct* ts_obj){
    do_open("/dev/stdin",1);
    do_open("/dev/stdout",1);
    do_open("/dev/stderr",1);
}

int do_setfg(int pid){
//    if(current_task == foreground_task){
        struct task_struct* temp =  (struct task_struct*)current_task;
        while(temp->next!=current_task){
            if(temp->pid==pid){
                foreground_task = temp;
                return 0;
            }
            temp = temp->next;
        }
        temp = (struct task_struct*)sleep_task;
        while(temp->next!=current_task){
            if(temp->pid==pid){
                foreground_task = temp;
                return 0;
            }
            temp = temp->next;
        }
        temp = (struct task_struct*)wait_task;
        while(temp->next!=current_task){
            if(temp->pid==pid){
                foreground_task = temp;
                return 0;
            }
            temp = temp->next;
        }
  //  }

    return -1;
}

int do_ulimit(uint32_t flag, uint64_t limit){
    //printf("................ flag %x   limit %lx\n",flag,limit );
    
    switch(flag){
        // flag 1 is for stack_limit
        case 1 :
            limit = limit * 1024;
            if(limit >= current_task->stack_limit || limit <=0)
                return -1;
            else
                current_task->stack_limit = ROUNDUP(limit,PAGE_SIZE);
            break;
        // flag 2 is for process_limit
        case 2 :
            if(limit >= current_task->running_proc_limit || limit <=0)
                return -1;
            else
                current_task->running_proc_limit = limit;
                //printf(" setting value %lx for pid %d\n",current_task->running_proc_limit,current_task->pid);
    //            while(1);
            break;
        default:
            return -1;
    }
    return 0;
}
