#include <defs.h>
#include <sys/process.h>
#include <sys/kprintf.h>
#include <sys/kern_macros.h>
#include <sys/slob_allocator.h>
#include <sys/virt_mem_mgr.h>
#include <sys/syscall.h>
#include <sys/elf.h>
#include <sys/loader.h>
#define PTE_CONTINUED 0x400

void *do_mmap(void *addr, uint64_t length, int prot, int flags, int fd){
    LOG("--------in mmap--------------\n");
    LOG("addr %lx %lx Cont = %lx %lx %lx \n",addr,length,prot,flags,fd);

    if(is_entry_present(addr)==1)
        return addr;

    if(grow_heap(length)>0){
        LOG("expanded heap VMA\n");
        return addr;
    }
    else{
        printf("failed to expand\n");
        return NULL;
    }
}

int do_munmap(void *start, uint64_t length){
    uint64_t phys_addr = (uint64_t)(pa(start));
    LOG("phys addr to be freed %lx\n",phys_addr);
    unmap(start);    
    if(phys_addr==0)
        return 0;
    free_phys_page((uint64_t*) phys_addr);
    return 0;
}

