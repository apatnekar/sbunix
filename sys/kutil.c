#include <defs.h>
#include <sys/kutil.h>
#include <sys/kprintf.h>
uint64_t pow(uint32_t a, uint64_t b)
{
  uint64_t r;
  if(b==0) return 1;
  r = pow(a,b/2);
  r = r*r;
  if(b%2) r = r*a;
  return r;
}

int strcmp(char *first, char *second)
{
   while(*first==*second)
   {
      if ( *first == '\0' || *second == '\0' )
         break;
      first++;
      second++;
   }
   if( *first == '\0' && *second == '\0' )
      return 0;
   else
      return -1;
}

void strcpy(char *dest, char *src){
    while(*src){
      *dest++ = *src++;     
    }
    *dest = '\0';
}

int is_childfile(char *temp) // is str in src directory
{   
    if(!temp)
      return 0;
    
    while(*temp!='\0')
    {
    if(*temp=='/' && !(*(temp+1)=='\0'))
      return 0;
    temp++; 
    }
    return 1;
}

char* is_prefix(char *src, char *str)  // str will be searched in src
{
    while(*str!=NULL){
      if(*src==NULL || *src++!=*str++){
        return NULL;
      }
    }
    return src;
}