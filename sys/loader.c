#include <defs.h>
#include <sys/kprintf.h>
#include <sys/tarfs.h>
#include <sys/loader.h>
#include <sys/elf.h>
#include <sys/process.h>
#include <sys/kern_macros.h>
#include <sys/slob_allocator.h>
#include <sys/kutil.h>
#include <sys/interrupt.h>
#include <sys/phys_mem_mgr.h>
#include <sys/fs.h>
#include <sys/inittarfs.h>
#define mmap_begin 0x08000000
#define mmap_end 0x10000000

//#define SECTSIZE        512
//#define ELFHDR                ((struct Elf *) 0x10000) // scratch space

//void readsect(void*, uint32_t);
//void readseg(uint32_t, uint32_t, uint32_t);

void *search_tarfs(char *filename){
    int j,k;
    uint32_t *findertarend;
    uint64_t decimal;
    struct posix_header_ustar *tarfs_obj = (struct posix_header_ustar *)&_binary_tarfs_start;

    if(filename == NULL)
        return NULL;

    findertarend = (uint32_t *)tarfs_obj;
    while(*findertarend++ || *findertarend++ || *findertarend){
        if(strcmp(tarfs_obj->name,filename)!=NULL){
            //printf("object name %s\n",tarfs_obj->name);
            k=0;
            decimal=0;
            for(j=10;j>=0;--j)
                decimal = decimal + (tarfs_obj->size[j]-48) * pow(8,k++);
            if(decimal%512!=0){
                decimal=(decimal/512)*512;
                decimal+=512;   
                }
            tarfs_obj = (struct posix_header_ustar *)((uint64_t)tarfs_obj + decimal + sizeof(struct posix_header_ustar));  
            findertarend = (uint32_t *)tarfs_obj; 
        }
        else
            return (void *)tarfs_obj;
    }
    return NULL;
}

struct task_struct* load_elf(char *filename , struct task_struct* ts)
{
	LOG("LOAD_ELF: \n");
    int i = search_rootnodes(filename); 
    if(i==-1 || root_nodes[i].flags==FS_DIRECTORY)
	return NULL;
    struct posix_header_ustar *file_info= (struct posix_header_ustar *)search_tarfs(&(filename[1]));
    //printf("value of i %d, addr %lx, addr of %lx\n",i,root_nodes[i].start, search_tarfs("bin/init"));
    struct Elf64_Ehdr *elf_obj;
    struct Elf64_Phdr *elf_phdr;
    struct task_struct *ts_obj;
    struct mm_struct *mm_obj;  
 
    if(file_info == NULL)
        return NULL;
    
    elf_obj = (struct Elf64_Ehdr *)(file_info + 1);
    if(ts==NULL){
        ts_obj = kmalloc(sizeof(struct task_struct),1);   // allocate memory for task_struct
	if(ts_obj == NULL)
	{
		printf("[loader.c] : Kmalloc FAILED!!\n");
		return NULL;
	}
    }
    else{
        ts_obj = ts;
    }
    mm_obj = kmalloc(sizeof(struct mm_struct),1);     // allocate memory for mm_struct
    (*ts_obj).mm = mm_obj;                            // add reference of mm_struct in task_struct

    LOG("File name %s\n",filename);

    ts_obj->rip = elf_obj->e_entry;                   // fill "entry_point" field
    ts_obj->mm->mmap = NULL;               // initialize VMA 

    elf_phdr = (struct Elf64_Phdr *)((uint64_t)elf_obj + elf_obj->e_phoff);     // get program header table address

    for (i = 0; i < elf_obj->e_phnum; ++i) {
        switch(elf_phdr->p_type) {
            case PT_LOAD: 
                LOG("elf header type: PT_LOAD\n"); 
                load_PT_LOAD(elf_obj, elf_phdr, ts_obj);
                break;
            //case PT_PHDR: printf("p_type: PT_PHDR\n"); break;
            //case GNU_STACK: printf("p_type: GNU_STACK\n"); break;
            //default: printf("Its probably GNU_STACK %lx\n",(uint64_t)&elf_phdr->p_type); 
        }
        elf_phdr++; 
    }
    load_heap(ts_obj);    // create vma for heap
    load_stack(ts_obj);   // create vma for stack
    return ts_obj;
}


void load_PT_LOAD(struct Elf64_Ehdr *elf_obj, struct Elf64_Phdr *elf_phdr, struct task_struct *ts_obj)
{
	LOG("LOAD_PT_LOAD: \n");
    /* create vma */
    struct vm_area_struct *vma = (struct vm_area_struct *)kmalloc(sizeof(struct vm_area_struct),1);   //alloc memory for VMA
	LOG("vma address is: %p\n", vma);
    if(ts_obj->mm->mmap == NULL) {   // if first vma
        ts_obj->mm->mmap = vma;
        ts_obj->mm->vm_last = vma;
    }
    else {
        ts_obj->mm->vm_last->vm_next = vma;        // link new vma
        ts_obj->mm->vm_last = vma;              //update last vma in mm_struct
    }

    vma->vm_mm = ts_obj->mm;
    vma->vm_start = elf_phdr->p_vaddr;
    vma->vm_end = elf_phdr->p_vaddr + elf_phdr->p_memsz;
    vma->vm_flags = elf_phdr->p_flags;
    vma->vm_type = VM_OTHER;
    vma->grows_down = 0;                         // set grows down
    vma->vm_file = NULL;
    vma->vm_next = NULL;

    if(elf_phdr->p_flags == (PF_R + PF_X) )    // If  .text segment 
    {              
        ts_obj->mm->start_code = elf_phdr->p_vaddr;
        ts_obj->mm->end_code = elf_phdr->p_vaddr + elf_phdr->p_memsz;

        vma->vm_file = (struct file *)kmalloc(sizeof(struct file),1);  
        vma->vm_file->file_start = (uint64_t)elf_obj;
        vma->vm_file->vm_pgoff = elf_phdr->p_offset;
        vma->vm_file->vm_sz = elf_phdr->p_filesz;
        vma->vm_file->bss_size = 0;
    }
    else if(elf_phdr->p_flags == (PF_R + PF_W) ){      // If  .data .bss segment
        ts_obj->mm->start_data = elf_phdr->p_vaddr;
        ts_obj->mm->end_data = elf_phdr->p_vaddr + elf_phdr->p_memsz;
//printf("filesz: %lx memsz: %lx\n",elf_phdr->p_filesz,elf_phdr->p_memsz);
        vma->vm_file = (struct file *)kmalloc(sizeof(struct file),1);  
        vma->vm_file->file_start = (uint64_t)elf_obj;
        vma->vm_file->vm_pgoff = elf_phdr->p_offset;
        vma->vm_file->vm_sz = elf_phdr->p_filesz;
        vma->vm_file->bss_size = elf_phdr->p_memsz - elf_phdr->p_filesz;
    }
}

void load_stack(struct task_struct *ts_obj){
	LOG("LOAD_STACK: \n");
    struct vm_area_struct *vma = (struct  vm_area_struct *)kmalloc(sizeof(struct vm_area_struct),1);   //alloc memory for VMA
    if(ts_obj->mm->mmap == NULL)   // if first vma
    {
        ts_obj->mm->mmap = vma;
        ts_obj->mm->vm_last = vma;
    }
    else{
        ts_obj->mm->vm_last->vm_next = vma;        // link new vma
        ts_obj->mm->vm_last = vma;              //update last vma in mm_struct
    }
    vma->vm_mm = ts_obj->mm;
    vma->vm_start = USER_STACK_ADDR - PAGE_SIZE;
    vma->vm_end = USER_STACK_ADDR;
    vma->vm_flags = (PF_R + PF_W);
    vma->vm_type = VM_STACK;
    vma->grows_down = 1;                         // set grows down
    vma->vm_file = NULL;
    vma->vm_next = NULL;
    ts_obj->rsp =  USER_STACK_ADDR ;          // user stack grows down
}

void load_heap(struct task_struct *ts_obj){
    LOG("LOAD_HEAP: \n");
    struct vm_area_struct *vma = (struct  vm_area_struct *)kmalloc(sizeof(struct vm_area_struct),1);   //alloc memory for VMA
    if(ts_obj->mm->mmap == NULL)   // if first vma
    {
        LOG("Error!! Heap cannot be first program segment\n");
        return ;
    }
    else{
        ts_obj->mm->vm_last->vm_next = vma;        // link new vma
        ts_obj->mm->vm_last = vma;              //update last vma in mm_struct
    }
    vma->vm_mm = ts_obj->mm;
    if(!ts_obj->mm->end_data)
        return;                                 //end_data is not available
    vma->vm_start = mmap_begin;
    vma->vm_end = mmap_begin + PAGE_SIZE;
    vma->vm_flags = (PF_R + PF_W);               // heap memory is readable && writable
    vma->vm_type = VM_HEAP;
    vma->grows_down = 0;                         // set grows down
    vma->vm_file = NULL;
    vma->vm_next = NULL;
}

/* Look up the first VMA which satisfies  addr < vm_end,  NULL if none. */
struct vm_area_struct *find_vma(struct mm_struct *mm, unsigned long addr)
{
	LOG("FIND_VMA: \n");
    struct vm_area_struct *vma = NULL;
    if (mm) {
	    LOG("mm address is %p\n",mm);
        struct vm_area_struct * vma_tmp;
        vma_tmp = mm->mmap;

        while (vma_tmp) {
	        if (vma_tmp && vma_tmp->vm_end > addr && vma_tmp->vm_start <= addr) {
                    vma = vma_tmp;
                    break;
                }
                else {
                    vma_tmp = vma_tmp->vm_next;
			}
        }
    }
    return vma;
}

void print_parsed_elf(struct task_struct *ts_obj){
    struct mm_struct *mm = ts_obj->mm;
    struct vm_area_struct *vma = mm->mmap;

    printf("mm_struct :\n");
    printf("rip: %lx start code: %lx end code: %lx\n",ts_obj->rip, mm->start_code, mm->end_code);
    printf("start data: %lx end data: %lx\n",mm->start_data, mm->end_data);
    while (vma){
        printf("vma start: %lx vma end: %lx vma flags :%x file: %lx\n",vma->vm_start,vma->vm_end,(uint32_t)vma->vm_flags,vma->vm_file);
        vma = vma->vm_next;
    }
}
						      
struct vm_area_struct * grow_stack(){
    //struct vm_area_struct *vma_stack = current_task->mm->vm_last;          //I assume Stack_vma is always last vma for a process
    struct vm_area_struct *vma_prev = current_task->mm->mmap;

    while(vma_prev!=NULL && vma_prev->vm_type!=VM_STACK)
        vma_prev = vma_prev->vm_next;        
    if(vma_prev!=NULL && (vma_prev->vm_start - PAGE_SIZE > mmap_end) && ((vma_prev->vm_end - vma_prev->vm_start - PAGE_SIZE) <= current_task->stack_limit)){
        vma_prev->vm_start = vma_prev->vm_start-PAGE_SIZE;
        return vma_prev;
    }
    else
        return NULL;
}

int grow_heap(uint64_t size){
    struct vm_area_struct *vma = current_task->mm->mmap;

    while(vma!=NULL && vma->vm_type!=VM_HEAP)
        vma = vma->vm_next;

    uint64_t rsize = ROUNDUP(size,PAGE_SIZE);
    
    if(vma!=NULL && (vma->vm_end + rsize < mmap_end)){
        vma->vm_end = vma->vm_end + rsize;
        return 1;
    }
    else
        return -1;

}

int access_ok(uint64_t addr, uint64_t count){
    struct vm_area_struct *temp;
    temp = find_vma(current_task->mm, addr);
    if(temp==NULL)
        return -1;
    else if(temp->vm_end >= (addr+count))
        return 1;
    else
        return -1;
}
