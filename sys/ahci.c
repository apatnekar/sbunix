#include <sys/ahci.h>
#include <sys/kprintf.h>
#include <sys/virt_mem_mgr.h>

void init_ahci()
{
    uint64_t *ahci_phys_addr;
    ahci_phys_addr = (uint64_t *)0xfebf0000;
    uint64_t *ahci_virt_page = get_free_page();
    map(ahci_phys_addr, ahci_virt_page, 1, 1);

    struct tagHBA_MEM *hba_mem = (struct tagHBA_MEM *)ahci_virt_page;

    printf("cap: %x\t", hba_mem->cap);
    printf("ghc: %x\t", hba_mem->ghc);
    printf("is: %u\t", hba_mem->is);
    printf("pi: %u\t", hba_mem->pi);
    printf("vs: %x\n", hba_mem->vs);
    printf("ccc_ctl: %u\t", hba_mem->ccc_ctl);
    printf("ccc_pts: %u\t", hba_mem->ccc_pts);
    printf("em_loc: %u\t", hba_mem->em_loc);
    printf("em_ctl: %u\t", hba_mem->em_ctl);
    printf("cap2: %u\n", hba_mem->cap2);
    printf("bohc %u\n", hba_mem->bohc);

}

