#include <defs.h>
#include <sys/gdt.h>
#include <sys/kprintf.h>
#include <sys/interrupt.h>
#include <sys/pages.h>
#include <sys/kern_macros.h>
#include <sys/slob_allocator.h>
#include <sys/phys_mem_mgr.h>
#include <sys/virt_mem_mgr.h>
#include <sys/process.h>
#include <sys/tarfs.h>
#include <sys/loader.h>
#include <sys/kutil.h>
#include <sys/syscall.h>
#include <sys/fs.h>
#include <sys/ahci.h>
#include <sys/inittarfs.h>

#define INITIAL_STACK_SIZE 4096

char stack[INITIAL_STACK_SIZE];
uint32_t* loader_stack;
extern char kernmem, physbase;
struct tss_t tss;

void start(uint32_t* modulep, void* physbase, void* physfree)
{//printf("size %lu\n",sizeof(struct task_struct) );while(1);
	KERNMEM = ((uint64_t)&kernmem);
	PHYSFREE = (uint64_t)physfree; 
	PHYSBASE = (uint64_t)physbase;

	init_free_page_list(modulep, physbase, physfree);
	mem_init();
	init_slob_allocator();	

	init_syscalls();
	init_tasking();
	init_tarfs();
	//navigate_tarfs();
	timer_ticks = 0;
	//print_tarfs();

	//printf("........ %lu...\n",sizeof(struct task_struct) );while(1);
	//init_ahci();
	//print_tarfs();
	struct task_struct *ts_init = create_process("/bin/init");
    	current_task->next = ts_init;
	ts_init->next = (struct task_struct*)current_task;
	current_task = ts_init;
	create_stdio_desc(ts_init);
//    printf("ts_init desc %lx %lx %lx \n",ts_init->fd[0], ts_init->fd[1], ts_init->fd[2]);
	foreground_task = ts_init;
	switch_to_user_mode(ts_init);

	__asm__ __volatile__ ("sti");
	while(1);
}

void boot(void)
{
	// note: function changes rsp, local stack variables can't be practically used
	register char *temp1, *temp2;
	__asm__(
		"movq %%rsp, %0;"
		"movq %1, %%rsp;"
		:"=g"(loader_stack)
		:"r"(&stack[INITIAL_STACK_SIZE])
	);
	
	reload_gdt();
	tss_flush();
	setup_tss();
	load_idt();
	init_console();
	isrs_install();
	irq_install();
	timer_install();
	keyboard_install();

	start(
		(uint32_t*)((char*)(uint64_t)loader_stack[3] + (uint64_t)&kernmem - (uint64_t)&physbase),
		&physbase,
		(void*)(uint64_t)loader_stack[4]
	);
	
	for(
		temp1 = "!!!!! start() returned !!!!!", temp2 = (char*)0xb8000;
		*temp1;
		temp1 += 1, temp2 += 2
	) *temp2 = *temp1;
	while(1);
}
