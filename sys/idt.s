# 
# idt.s
# Created On: Sep 11, 2013
#

.text

###
# load a new idt
# param : address of idtr

.global _x86_64_asm_lidt
_x86_64_asm_lidt:
	
	lidt (%rdi)
	retq 
