#include <sys/inittarfs.h>
#include <sys/kutil.h>
#include <sys/tarfs.h>
#include <sys/fs.h>
#include <sys/process.h>
#include <sys/slob_allocator.h>
#include <sys/virt_mem_mgr.h>

uint32_t find_nfiles(){
    int j,k;
    uint32_t *find_tar_end;
    uint64_t decimal;
    struct posix_header_ustar *tarfs = (struct posix_header_ustar *)&_binary_tarfs_start;
    uint32_t nfiles = 0;
    find_tar_end = (uint32_t *)tarfs;

    while(*find_tar_end++ || *find_tar_end++ || *find_tar_end){
        k=0;
        decimal=0;
        for(j=10;j>=0;--j)
            decimal = decimal + (tarfs->size[j]-48) * pow(8,k++);
        
        if(decimal%512!=0){
            decimal=(decimal/512)*512;
            decimal+=512;
        }
        tarfs = (struct posix_header_ustar *)((uint64_t)tarfs + decimal + sizeof(struct posix_header_ustar));  
        nfiles++;
        find_tar_end = (uint32_t *)tarfs; 
    }
    return nfiles;
}

void create_root_node(){
    int i=0;
    strcpy(root_nodes[i].name, "/");

    root_nodes[i].start = 0;
    root_nodes[i].length = 0;
    root_nodes[i].inode = i;
    root_nodes[i].flags = FS_DIRECTORY;

    root_nodes[i].write = (write_type_t)0;
    root_nodes[i].open = (open_type_t)(0);
    root_nodes[i].read = (read_type_t)(0);
    root_nodes[i].close = (close_type_t)(0);

    root_nodes[i].readdir = (readdir_type_t)(&tarfs_readdir);
    root_nodes[i].opendir = (opendir_type_t)(&tarfs_opendir);
    root_nodes[i].closedir = (closedir_type_t)(&tarfs_closedir);
    memset(root_nodes[i].f_child,0,sizeof(uint64_t)*MAXENTRY);
}

void create_stdio_nodes(){
    int i=nroot_nodes+0;
    strcpy(root_nodes[i].name, "/dev/stdin");

    root_nodes[i].start = 0;
    root_nodes[i].length = 0;
    root_nodes[i].inode = i;
    root_nodes[i].flags = FS_FILE;
    
    root_nodes[i].write = (write_type_t)0;
    root_nodes[i].open = (open_type_t)(&stdio_open);
    root_nodes[i].read = (read_type_t)(&stdio_read);
    root_nodes[i].close = (close_type_t)(&stdio_close);

    root_nodes[i].readdir = (readdir_type_t)(0);
    root_nodes[i].opendir = (opendir_type_t)(0);
    root_nodes[i].closedir = (closedir_type_t)(0);
    memset(root_nodes[i].f_child,0,sizeof(uint64_t)*MAXENTRY);
    
    i=nroot_nodes+1;
    strcpy(root_nodes[i].name, "/dev/stdout");

    root_nodes[i].start = 0;
    root_nodes[i].length = 0;
    root_nodes[i].inode = i;
    root_nodes[i].flags = FS_FILE;
    
    root_nodes[i].write = (write_type_t)(&stdio_write);
    root_nodes[i].open = (open_type_t)(&stdio_open);
    root_nodes[i].read = (read_type_t)(0);
    root_nodes[i].close = (close_type_t)(&stdio_close);

    root_nodes[i].readdir = (readdir_type_t)(0);
    root_nodes[i].opendir = (opendir_type_t)(0);
    root_nodes[i].closedir = (closedir_type_t)(0);
    memset(root_nodes[i].f_child,0,sizeof(uint64_t)*MAXENTRY);
    
    //STDERR
    i=nroot_nodes+2;
    strcpy(root_nodes[i].name, "/dev/stderr");

    root_nodes[i].start = 0;
    root_nodes[i].length = 0;
    root_nodes[i].inode = i;
    root_nodes[i].flags = FS_FILE;
    
    root_nodes[i].write = (write_type_t)(&stdio_write);
    root_nodes[i].open = (open_type_t)(&stdio_open);
    root_nodes[i].read = (read_type_t)(0);
    root_nodes[i].close = (close_type_t)(&stdio_close);

    root_nodes[i].readdir = (readdir_type_t)(0);
    root_nodes[i].opendir = (opendir_type_t)(0);
    root_nodes[i].closedir = (closedir_type_t)(0);
    memset(root_nodes[i].f_child,0,sizeof(uint64_t)*MAXENTRY);
}

void init_tarfs(){
    // Initialise the main and file header pointers and populate the root directory.
    int j,k;
    uint64_t decimal;
    struct posix_header_ustar *tarfs = (struct posix_header_ustar *)&_binary_tarfs_start;

    // Initialise the root directory.
 //   printf("size of fs_node_t is %lu\n",sizeof(fs_node_t) );

    nroot_nodes = find_nfiles();
    nroot_nodes=nroot_nodes+1;
    uint64_t root_nodes_size = sizeof(fs_node_t)*(nroot_nodes + 3); // 3 for stdin, stderr, stdout
    root_nodes_size = ROUNDUP(root_nodes_size,PAGE_SIZE);
    uint32_t tot_pages = root_nodes_size / PAGE_SIZE;
    root_nodes = (fs_node_t*)get_free_pages(tot_pages);
    memset((void*)root_nodes,0,tot_pages*PAGE_SIZE);
    //printf("addr of root_nodes %lx\n",root_nodes);
    create_root_node();
    create_stdio_nodes();
    // For every file...
    int i;
    for (i = 1; i < (nroot_nodes); i++){
        k=0;
        decimal=0;
        for(j=10;j>=0;--j)
            decimal = decimal + (tarfs->size[j]-48) * pow(8,k++);
        // Create a new file node.
        root_nodes[i].name[0] = '/';
        strcpy(&(root_nodes[i].name[1]), tarfs->name);
        
        root_nodes[i].start = (uint64_t)tarfs + sizeof(struct posix_header_ustar);
        root_nodes[i].length = decimal;
        root_nodes[i].inode = i;
        root_nodes[i].flags = (tarfs->typeflag[0]=='5'?FS_DIRECTORY:FS_FILE);
        
        root_nodes[i].write = (write_type_t)0;
        root_nodes[i].open = (open_type_t)(tarfs->typeflag[0]=='5' ? 0 : &tarfs_open);
        root_nodes[i].read = (read_type_t)(tarfs->typeflag[0]=='5' ? 0 : &tarfs_read);
        root_nodes[i].close = (close_type_t)(tarfs->typeflag[0]=='5' ? 0 : &tarfs_close);

        root_nodes[i].readdir = (readdir_type_t)(tarfs->typeflag[0]=='5' ? &tarfs_readdir : 0);
        root_nodes[i].opendir = (opendir_type_t)(tarfs->typeflag[0]=='5' ? &tarfs_opendir : 0);
        root_nodes[i].closedir = (closedir_type_t)(tarfs->typeflag[0]=='5' ? &tarfs_closedir : 0);

        memset(root_nodes[i].f_child,0,sizeof(uint64_t)*MAXENTRY);
        if(decimal%512 != 0){
            decimal=(decimal/512)*512;
            decimal+=512;   
        }
        tarfs = (struct posix_header_ustar *)((uint64_t)tarfs + decimal + sizeof(struct posix_header_ustar));      
    }
    return;
}

int is_dir_in_root(char *path, int level) {
    if(path[0] != '/')
        return 0;
    int i=1;
    while(1) {
        if(path[i++] == '/') level--;
        if(!level) break;
    }
    if(path[i+1] != '\0')
        return 0;
    return 1;
}

int is_file(char *path) {
    int i=0;
    while(path[i++]);
    if(path[i-2] == '/')    // this mean it is a directory
        return 0;
    return 1;
}

int is_immidiate_child(fs_node_t *this_node, fs_node_t *check_for_child) {
    if(this_node == check_for_child)
        return 0;
    int i=0;        // complete path for this_node_path should be a prefix of check_for_child_path
    char *this_node_path = this_node->name;
    char *check_for_child_path = check_for_child->name;
    while(this_node_path[i]) {
        if(this_node_path[i] == check_for_child_path[i])
            i++;
        else return 0;    
    }
    int num_of_levels = 0;
    while(check_for_child_path[i]) {
        if(check_for_child_path[i] == '/')
            num_of_levels++;
        i++;
    }
    if(num_of_levels == 0)                                          // immidiate file
        return 1;
    else if(num_of_levels == 1 && check_for_child_path[i-1] == '/') // immidiate directory
        return 1;
    else return 0;
}

void assign_f_child(fs_node_t *this_node, fs_node_t *parent_node, int level) {
    // point to self
    this_node->f_child[0] = (uint64_t)&this_node;
    // point to parent
    this_node->f_child[1] = (uint64_t)&parent_node;
    int i;
    for(i=0;i<nroot_nodes;i++) {
        if(!is_file(root_nodes[i].name) && is_immidiate_child(this_node, &root_nodes[i])) {
            this_node->f_child[i+2] = (uint64_t)&root_nodes[i];
//            assign_f_child(root_nodes+i, this_node, level+1);
        }
    }
}

void navigate_tarfs() {

    inittarfs_root = (fs_node_t*)kmalloc(sizeof(fs_node_t),1);
    strcpy(inittarfs_root->name, "/");
    inittarfs_root->flags = FS_DIRECTORY;
    inittarfs_root->inode = inittarfs_root->start = inittarfs_root->length = 0;
    inittarfs_root->read = 0;
    inittarfs_root->write = 0;
    inittarfs_root->open = 0;
    inittarfs_root->close = 0;
    inittarfs_root->readdir = (readdir_type_t)&tarfs_readdir;
    inittarfs_root->opendir = (opendir_type_t)&tarfs_opendir;
    inittarfs_root->closedir = (closedir_type_t)&tarfs_closedir;
    inittarfs_root->ptr = NULL;
    // f_child[0] - point to self
    inittarfs_root->f_child[0] = (uint64_t)&(inittarfs_root);
    // f_child[1] - point to parent - here no parent - so point to self
    inittarfs_root->f_child[1] = (uint64_t)&(inittarfs_root);
    // now iteratively store next directory poitners
    print_tarfs();
    int i;  printf("\n");
    for(i=0;i<nroot_nodes;i++) {
        if(is_dir_in_root(root_nodes[i].name, 1)) {
            inittarfs_root->f_child[i+2] = (uint64_t)&root_nodes[i];
            printf("name: %s\t", root_nodes[i].name);
            assign_f_child(root_nodes+i, inittarfs_root, 2);
        }
    }
}

int do_cd(char *path) {
    strcpy((char *)current_task->pwd,path);
    return 0;
}

void print_tarfs(){
    int i;
    for (i = 0; i < nroot_nodes + 3; i++){
        printf("name %s \t",root_nodes[i].name);
//        printf("Start:%lx length:%lx flags:%d ",root_nodes[i].start, root_nodes[i].length, root_nodes[i].flags);
  //      printf("Write:%lx Open:%lx Read:%lx Close:%lx\n",root_nodes[i].write, root_nodes[i].open,root_nodes[i].read, root_nodes[i].close );
    //    printf("Readdir:%lx, Opendir:%lx, Closedir:%lx\n",root_nodes[i].readdir, root_nodes[i].opendir, root_nodes[i].closedir);
    }
}

int search_rootnodes(char* name){
    int i;
    for (i = 0; i < nroot_nodes; i++){
        if(strcmp(root_nodes[i].name,name)==0){
            return i;
        }
    }
    return -1;
}

