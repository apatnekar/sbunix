#include <defs.h>
#include <sys/kprintf.h>
#include <sys/cpuid.h>

void cpuid(unsigned info, unsigned *rax,
              unsigned *rbx,
              unsigned *rcx,
              unsigned *rdx) {
    __asm__("xchg %%rbx, %%rdi;"
        "cpuid;"
        "xchg %%rbx, %%rdi;"
        :"=a" (*rax), "=D" (*rbx), "=c" (*rcx), "=d" (*rdx)
        :"0" (info)
    );
}

void print_regs(int rax, int rbx, int rcx, int rdx) {
    int j;
    char string[17];
    string[16] = '\0';
    for(j=0;j<4;j++) {
        string[j] = rax >> (8*j);
        string[j+4] = rbx >> (8*j);
        string[j+8] = rcx >> (8*j);
        string[j+12] = rdx >> (8*j);
    }   printf("%s", string);
}

void amd_info() {
    unsigned int extended, rax, rbx, rcx, rdx, unused;
    int family, model, stepping, reserved;

    printf("AMD specific features:\n");
    cpuid(1, &rax, &unused, &unused, &unused);

    model = (rax >>4) & 0xf;
    family = (rax >>8) & 0xf;
    stepping = rax &0xf;
    reserved = rax >> 12;

    printf("Family: %d Model: %d [", family, model);

    switch(family) {
        case 4:
            printf("486 Model %d", model);
            break;
        case 5:
            switch(model) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 6:
                case 7:
                    printf("K6 Model %d", model);
                    break;
                case 8:
                    printf("K6-2 Model 8");
                    break;
                case 9:
                    printf("K6-III Model 9");
                    break;
                default:
                    printf("K5/K6 Model %d", model);
                    break;
            }
        break;
        case 6:
            switch(model) {
                case 1:
                case 2:
                case 4:
                    printf("Athlon Model %d", model);
                    break;
                case 3:
                    printf("Duron Model 3");
                    break;
                case 6:
                    printf("Athlon MP/Mobile Athlon Model 6");
                    break;
                case 7:
                    printf("Mobile Duron Model 7");
                    break;
                default:
                    printf("Duron/Athlon Model %d", model);
                    break;
            }
        break;
    }
    printf("]\n");

    cpuid(0x80000000, &extended, &rbx, &rcx, &rdx);//unused, unused, unused);
    if(extended == 0)
        return;
    if(extended >= 0x80000002) {
        unsigned int j;
        printf("Detected Processor Name: ");
        for(j = 0x80000002; j <= 0x80000004; j++) {
            cpuid(j, &rax, &rbx, &rcx, &rdx);
            print_regs(rax, rbx, rcx, rdx);
        } printf("\n");
    }
    if(extended >= 0x80000007) {
        cpuid(0x80000007, &unused, &unused, &unused, &rdx);
        if(rdx & 1)
            printf("Temperature Sensing Diode Detected!\n");
    }

    printf("Stepping: %d, Reserved: %d\n", stepping, reserved);
}

void print_cpu_info() {
    amd_info();
/*  int a,b;
    int j;
    unsigned int rax, rbx, rcx, rdx;

    for(a=0;a<5;a++) {
        __asm__("cpuid"
                :"=a"(b)
                :"0"(a)
                :"%rbx","%rcx","%rdx");
        printf("The code %x gives %x\n", a, b);
    }

    for(j=0;j<6;j++) {
        cpuid(j, &rax, &rbx, &rcx, &rdx);
        printf("rax=%d: %x %x %x %x\n", j, rax, rbx, rcx, rdx);
    }
    for(j=0;j<6;j++) {
        cpuid(j, &rax, &rbx, &rcx, &rdx);
        printf("rax=%d: %c %c %c %c\n", j, rax, rbx, rcx, rdx);
    }*/
}

