#include <sys/process.h>
#include <sys/interrupt.h>
#include <sys/kprintf.h>

/* flags for shift and control */
uint8_t shift_key = 0;
uint8_t control_key = 0;
char terminal_buf[1024]={};
uint32_t term_start = 0;
uint32_t term_end = 0;
volatile uint32_t enter_encountered = 0;
/* US Keyboard Layout scancode table*/
unsigned char kbdus[256] =
{
    0,  27, '1', '2', '3', '4', '5', '6', '7', '8', /* 9 */
  '9', '0', '-', '=', '\b', /* Backspace */
  '\t',         /* Tab */
  'q', 'w', 'e', 'r',   /* 19 */
  't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\n', /* Enter key */
    0,          /* 29   - Control */
  'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', /* 39 */
 '\'', '`',   0,        /* Left shift */
 '\\', 'z', 'x', 'c', 'v', 'b', 'n',            /* 49 */
  'm', ',', '.', '/',   0,              /* Right shift */
  '*',
    0,  /* Alt */
  ' ',  /* Space bar */
    0,  /* Caps lock */
    0,  /* 59 - F1 key ... > */
    0,   0,   0,   0,   0,   0,   0,   0,
    0,  /* < ... F10 */
    0,  /* 69 - Num lock*/
    0,  /* Scroll Lock */
    0,  /* Home key */
    0,  /* Up Arrow */
    0,  /* Page Up */
  '-',
    0,  /* Left Arrow */
    0,
    0,  /* Right Arrow */
  '+',
    0,  /* 79 - End key*/
    0,  /* Down Arrow */
    0,  /* Page Down */
    0,  /* Insert Key */
    0,  /* Delete Key */
    0,   0,   0,
    0,  /* 87 - F11 Key */
    0,  /* 88 - F12 Key */
    0,  /* All other keys are undefined */
    0,  /*90*/
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* 105*/
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, /* 120*/
    0,0,0,0,0,0,0,      /*128*/
//*****************keymap for shift + ********


    0,  27, '!', '@', '#', '$', '%', '^', '&', '*', /* 9 */
  '(', ')', '_', '+', '\b', /* Backspace */
  '\t',         /* Tab */
  'Q', 'W', 'E', 'R',   /* 19 */
  'T', 'Y', 'U', 'I', 'O', 'P', '{', '}', '\n', /* Enter key */
    0,          /* 29   - Control */
  'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ':', /* 39 */
 '\"', '~',   0,        /* Left shift */
 '\\', 'Z', 'X', 'C', 'V', 'B', 'N',            /* 49 */
  'M', '<', '>', '?',   0,              /* Right shift */
  '*',
    0,  /* Alt */
  ' ',  /* Space bar */
    0,  /* Caps lock */
    0,  /* 59 - F1 key ... > */
    0,   0,   0,   0,   0,   0,   0,   0,
    0,  /* < ... F10 */
    0,  /* 69 - Num lock*/
    0,  /* Scroll Lock */
    0,  /* Home key */
    0,  /* Up Arrow */
    0,  /* Page Up */
  '-',
    0,  /* Left Arrow */
    0,
    0,  /* Right Arrow */
  '+',
    0,  /* 79 - End key*/
    0,  /* Down Arrow */
    0,  /* Page Down */
    0,  /* Insert Key */
    0,  /* Delete Key */
    0,   0,   0,
    0,  /* F11 Key */
    0,  /* F12 Key */
    0,  /* All other keys are undefined */
};

void resume_waiting_task(){
        LOG("In check list\n");
        volatile struct task_struct *cur = wait_for_read;

	if(wait_for_read != NULL){
        	wait_for_read = wait_for_read->next;
	        cur->next = current_task->next;
	        current_task->next = (struct task_struct*)cur;
        }
        //outb(0x20, 0x20);
        return; //no task to be inserted in the current_task_list
}


/* Handles the keyboard interrupt */
void keyboard_handler()
{
    unsigned char scancode;
    char c; 
    uint32_t temp_end;
    /* Read from the keyboard's data buffer */
    scancode = inb(0x60);

    /* If the top bit of the byte we read from the keyboard is
    *  set, that means that a key has just been released */
    if (scancode & 0x80)
    {
        /* You can use this one to see if the user released the
        *  shift, alt, or control keys... */
        /* scancode 42 and 54 stand for left and right shift key 
        *  so we unset the shift flag */
        if((scancode & 0x7F)==42 || (scancode & 0x7F)==54){
            shift_key = 0;  
        }
        /* control stands for 29 */
        if((scancode & 0x7F) == 29){
                control_key = 0;
        }   
    }
    else
    {
        switch(scancode){
            /* 42 and 54 are shift keys */
            case 42:
            case 54:
                shift_key = 1;
            break;
            case 29:
                control_key = 1;
            break;
            default:
                   
                if(shift_key==1){
                    c = kbdus[scancode+128];
		    //printf("%c",kbdus[scancode+128]);
                    //print_kbd(control_key,kbdus[scancode+128]);
                    //print_shell_cmd(control_key,kbdus[scancode+128]);
                }
                else{
                    c = kbdus[scancode];
		    //printf("%c",kbdus[scancode]);
		     //       print_kbd(control_key,kbdus[scancode]);
                    //print_shell_cmd(control_key,kbdus[scancode]);
                }
                temp_end = (term_end+1)%1024;
                
                if(temp_end!=term_start){
			if(c == '\b' && term_end==term_start)
				return;
                        if(c== '\b' && term_end!=term_start){
                        	    printf("%c",c);
			            terminal_buf[term_end]='\0';
                                    if(term_end==0)
                                        term_end = 1023;
                                    else
                                        --term_end;
                                    return;
                    }
			 if(c!='\b')
	 			printf("%c",c);
                        terminal_buf[term_end] = c;
                        term_end = temp_end;
                        if(c=='\n'){
                            enter_encountered++;    
			    resume_waiting_task();
			}
                }
                    
        }
    }
}

int get_kbd_data(char* buf, uint32_t count){
    uint32_t bytes_read=0;
    int i;

    //while(enter_encountered!=1);

    for(i=0;i<count;i++){
        if(term_start==term_end)
            return bytes_read;
        buf[bytes_read++] = terminal_buf[term_start];
        term_start= (term_start+1)%1024;
        if(buf[bytes_read-1]=='\n'){
            enter_encountered--;
            return bytes_read;
        }
    }
    return bytes_read;
}

void keyboard_install(){
    irq_install_handler(1, keyboard_handler);
}

