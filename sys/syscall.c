#include <sys/fs.h>
#include <sys/gdt.h>
#include <sys/syscall.h>
#include <sys/interrupt.h>
#include <sys/process.h>
#include <sys/slob_allocator.h>
#include <sys/virt_mem_mgr.h>
#include <sys/loader.h>
#include <sys/inittarfs.h>

static void *syscalls[NUM_SYSCALLS] =
{
    &schedule,
    &puts,
    &do_fork,
    &do_execve,
    &do_exit,
    &do_getpid,
    &do_getpwd,
    &do_setpwd,
    &do_cd,
    &do_wait,
    &do_waitpid,
    &do_sleep,
    &do_ps,
    &do_setfg,//&read_stdin,
    &do_ulimit,
    &do_write,
    &do_opendir,
    &do_readdir,
    &do_closedir,
    &do_read,
    &do_open,
    &do_close,
    &do_mmap,
    &do_munmap,
    &is_entry_present,
    &dup_fd,
};

void init_syscalls() {
   // Register our syscall handler.
   idt_set_gate(0x80, (uint64_t)sys_call, 0x08, 0xEE, 0);   
}


void syscall_handler(struct regs *r) {
    // Firstly, check if the requested syscall number is valid.
    // The syscall number is found in EAX.
    LOG("inside syscall_handler\n");
    if (r->rax >= NUM_SYSCALLS){
        printf("syscall not present !!!\n");
        return;
    }
    LOG("from syscall: rip%lx, interrupt number: %d\n", r->rip, r->int_no);
//if(r->rax == 4)    
//LOG("before syscall: rdi %lx  rsi %lx, syscall number: %d for task pid %d\n", r->rdi, r->rsi, r->rax,current_task->pid);
    //printf("inside kernels syscall hanlder , syscal no %d\n",r->rax);
    // Get the required syscall location.
    void *location = syscalls[r->rax];
    //printf("before syscall: rdi %lx  rsi %lx, rbx %lx rcx %lx rdx %lx interrupt number: %d\n", r->rdi, r->rsi, r->rbx, r->rcx, r->rdx, r->int_no);
    

    LOG("rip: %lx\n", r->rip);
    // We don't know how many parameters the function wants, so we just
    // push them all onto the stack in the correct order. The function will
    // use all the parameters it wants, and we can pop them all back off afterwards.
    uint64_t ret;
    asm volatile (  
     "pushq %%rdi;"
     "pushq %%rsi;"
     "pushq %%rbx;"
     "pushq %%rcx;"
     "pushq %%rdx;"
     "movq %1, %%rdi;"
     "movq %2, %%rsi;"
     "movq %3, %%rdx;"
     "movq %4, %%rcx;"
     "movq %5, %%r8;"
     "callq *%6;" 
     "popq %%rdx;"
     "popq %%rcx;"
     "popq %%rbx;"
     "popq %%rsi;"
     "popq %%rdi;" 
     : "=a" (ret) 
     : "r" (r->rdi), "r" (r->rsi),  "r" (r->rbx),"r" (r->rcx), "r" (r->rdx), "r" (location)
     : "rsi","rdi","rbx","rcx","rdx"
     );

//if(current_task->pid == 4)
  //  printf("after syscall:  %lx\n", *((int64_t*)current_task->initial_kernel_stack - 9));
    
    *((int64_t*)current_task->initial_kernel_stack - 9) = ret;
    //printf("end of syscall_handler\n");
   // if(current_task->pid==3)
   // while(1);
}

