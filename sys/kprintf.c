#include <sys/stdarg.h>
#include <sys/kprintf.h>
#include <sys/kern_macros.h>

static const size_t VGA_WIDTH = 80;
static const size_t VGA_HEIGHT = 24; 

void print_time(uint32_t time_in_sec){
    uint32_t ones_digit, tens_digit;
    char tens_char,ones_char;
    uint32_t sec=0, min=0, hour=0;
    size_t row = VGA_HEIGHT; //on the 24th row
    size_t column = VGA_WIDTH -1;

    hour = time_in_sec/3600;
    time_in_sec = time_in_sec%3600;

    min = time_in_sec/60;
    time_in_sec = time_in_sec%60;

    sec = time_in_sec;

    //printing sec to console
    ones_digit = sec%10;
    tens_digit = sec/10;
    ones_char = (char)(((int)'0') + ones_digit);
    tens_char = (char)(((int)'0') + tens_digit);

    putentry_at_console(ones_char, colorConsole, column , row); // 78th column
    putentry_at_console(tens_char, colorConsole, column-1 , row); // 77th column

    //printing min to console
    ones_digit = min%10;
    tens_digit = min/10;
    ones_char = (char)(((int)'0') + ones_digit);
    tens_char = (char)(((int)'0') + tens_digit);

    putentry_at_console(':', colorConsole, column-2 , row); // 76th column
    putentry_at_console(ones_char, colorConsole, column-3, row); // 75th column
    putentry_at_console(tens_char, colorConsole, column-4, row); // 74th column

    //printing hour to console
    ones_digit = hour%10;
    tens_digit= hour/10;
    ones_char = (char)(((int)'0') + ones_digit);
    tens_char = (char)(((int)'0') + tens_digit);

    putentry_at_console(':', colorConsole, column-5 , row); // 73rd column
    putentry_at_console(ones_char, colorConsole, column-6 , row); // 72nd column
    putentry_at_console(tens_char, colorConsole, column-7 , row); // 71st column
}

int printf(const char * fmt, ...) {
    char printf_buf[1024];
    va_list args;
    int printed;

    va_start(args, fmt);
    printed = vsprintf(printf_buf, fmt, args);
    va_end(args);
    puts(printf_buf);

    return printed;
}

int vsprintf(char *buf, const char *fmt, va_list args) {
    int base;
    char *str;
    const char *s;
    uint64_t num_uint64;
    long int num_int64;
    int i,len;

    for (str = buf; *fmt; ++fmt) {
        if (*fmt != '%') {
            *str++ = *fmt;
            continue;
        }

        ++fmt;          /* this also skips first '%' */

        /* default base */
        base = 10;

        switch (*fmt) {
            case 'c':
                *str++ = (unsigned char)va_arg(args, int);
                continue;
            case 's':
                s = va_arg(args, char *);
                len = strlen(s);

                for (i = 0; i < len; ++i)
                    *str++ = *s++;
                continue;
            case 'p':
                str = itoa((unsigned long)va_arg(args, void *), str, 16, 0);
                continue;
            case 'x':
            case 'X':
                base = 16;
		num_uint64 = va_arg(args, uint32_t);
		str = itoa(num_uint64 ,str , base, 0);
		break;
            case 'd':
		num_int64 = va_arg(args, int);
		if(num_int64<0)
		str = itoa(-num_int64, str, base, 1);
		else
		str = itoa(num_int64, str, base, 0);
		break;
	    case 'u':
		num_uint64 = va_arg(args, uint32_t);
		str = itoa(num_uint64 ,str , base, 0);
		break;
	    case 'l':
		switch(*++fmt){
		case 'd':
			num_int64 = va_arg(args, long int);
			if(num_int64<0)
				str = itoa(-num_int64, str, base, 1);
			else
			str = itoa(num_int64, str, base, 0);
			break;
	    	case 'u':
			num_uint64 = va_arg(args, uint64_t);
			str = itoa(num_uint64 ,str , base, 0);
			break;
		case 'x':
			base = 16;
			num_uint64 = va_arg(args, uint64_t);
			str = itoa(num_uint64 ,str , base, 0);
			break;

		}
        }
     }
     *str = '\0';
     return str - buf;
}

uint8_t make_color(enum vga_color fg, enum vga_color bg){
    return fg | bg << 4;
}

uint16_t make_vgaentry(char c, uint8_t color){
    uint16_t c16 = c;
    uint16_t color16 = color;
    return c16 | color16 << 8;
}

size_t strlen(const char* str){
    size_t ret = 0;
    while ( str[ret] != 0 )
        ret++;
    return ret;
}

void init_console(){
    rowConsole = 0;
    columnConsole = 0;
    rowShell = VGA_HEIGHT;
    columnShell = VGA_WIDTH - 80;
    colorConsole = make_color(COLOR_LIGHT_GREEN, COLOR_BLACK);
    bufferConsole = (uint16_t*) VIDEO_BUFFER_VADDR;
    clear_console();
    update_cursor();
}

void clear_console(){
    size_t x,y;
    for (  y = 0; y < VGA_HEIGHT; y++ ) {
        for (  x = 0; x < VGA_WIDTH; x++ ) {
            const size_t index = y * VGA_WIDTH + x;
            bufferConsole[index] = make_vgaentry(' ', colorConsole);
        }
    }
}

void setcolor_console(uint8_t color)
{
    colorConsole = color;
}

void putentry_at_console(char c, uint8_t color, size_t x, size_t y)
{
    const size_t index = y * VGA_WIDTH + x;
    switch(c) {
        case '\n': columnConsole=79;  return;
        case '\t': if((columnConsole+4)>79) 
			columnConsole=79;
		   else 
		   	columnConsole+=4;
			return;
	case '\b': if(columnConsole == 0 && rowConsole == 0) {
			columnConsole--;
			return;
		}
		else if(columnConsole == 0) {
			columnConsole = 79;
			rowConsole--;
		}
		else{
		    columnConsole--;
		}
		columnConsole--;
		bufferConsole[index-1] = make_vgaentry(' ', color);
		return; 
	} 
	bufferConsole[index] = make_vgaentry(c, color);
} 

void putentry_at_console_kbd(char c, uint8_t color, size_t x, size_t y) { const size_t index = y * VGA_WIDTH + x;
    switch(c) {
        case '\n': c = ' '; break;
        case '\t': c = ' '; break;
        case '\b': c = ' '; break;
    }
    bufferConsole[index] = make_vgaentry(c, color);
}

void putchar(char c)
{
    putentry_at_console(c, colorConsole, columnConsole, rowConsole);
    if ( ++columnConsole == VGA_WIDTH ) {
        columnConsole = 0;
        if ( ++rowConsole == VGA_HEIGHT ) {
            scroll();
            --rowConsole;
        }
    }
    update_cursor(); 
}

void puts(const char* data)
{
    size_t i;
    size_t datalen = strlen(data);
    for (  i = 0; i < datalen; i++ )
        putchar(data[i]);
}

void scroll()
{
    size_t index, x, y;
    for (  y = 0; y < VGA_HEIGHT-1; y++ ) {
        for (  x = 0; x < VGA_WIDTH; x++ ) {
            index = y * VGA_WIDTH + x;
            bufferConsole[index] = bufferConsole[index+80];
        }
    }
    for (  x = 0; x < VGA_WIDTH; x++ ) {
        index = y * VGA_WIDTH + x;
        bufferConsole[index] = make_vgaentry(' ', colorConsole);
    }
}

void update_cursor()
{
    unsigned short position=(rowConsole*80) + columnConsole;
 
    // cursor LOW port to vga INDEX register
    outb(0x3D4, 0x0F);
    outb(0x3D5, (unsigned char)(position&0xFF));
    // cursor HIGH port to vga INDEX register
    outb(0x3D4, 0x0E);
    outb(0x3D5, (unsigned char )((position>>8)&0xFF));   
}

void outb(unsigned short _port, unsigned char _data)
{
    asm volatile ("outb %1, %0" : : "dN" (_port), "a" (_data));
} 

unsigned char inb(unsigned short _port)
{
    unsigned char ret;
    asm volatile ("inb %1, %0" : "=a"(ret) : "Nd"(_port));
    return ret;
} 

void print_kbd(int ctrl, char c)
{
    size_t row = VGA_HEIGHT;
    size_t column = VGA_WIDTH - 11;
    putentry_at_console_kbd(' ', colorConsole, column , row);
    putentry_at_console_kbd(' ', colorConsole, column+1 , row);
    if(ctrl==1){
    	putentry_at_console_kbd('^', colorConsole, column , row);
    	putentry_at_console_kbd(c, colorConsole, ++column , row);
    } else {
    	putentry_at_console_kbd(' ', colorConsole, column , row);
	putentry_at_console_kbd(c, colorConsole, ++column, row);
    }
}

void print_shell_cmd(int ctrl, char c)
{
//    putentry_at_console_kbd(' ', colorConsole, columnShell , rowShell);
  //  putentry_at_console_kbd(' ', colorConsole, columnShell+1 , rowShell);
    if(ctrl==1){
    	putentry_at_console_kbd('^', colorConsole, columnShell , rowShell);
    	putentry_at_console_kbd(c, colorConsole, ++columnShell , rowShell);
    } else {
    	//putentry_at_console_kbd(' ', colorConsole, columnShell , rowShell);
	putentry_at_console_kbd(c, colorConsole, ++columnShell, rowShell);
    }
}

char * itoa( uint64_t value, char * str, int base, int sign_flag ){
    char * ptr;
    char * low;
    char * high;

    if ( base < 2 || base > 36 ) {
        *str++ = '\0';
        return str;
    }
    ptr = str;
    
    if ( sign_flag ==1 && base == 10 ) {
        *ptr++ = '-';
    }
    if (base == 16) {
        *ptr++ = '0';
        *ptr++ = 'x';
    }
    low = ptr;
    do {
        // Modulo is negative for negative value. This trick makes abs() unnecessary.
        *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz"[35 + value % base];
        value /= base;
    } while ( value );
        // Terminating the string.
    *ptr-- = '\0';

    high = ptr+1;
    // Invert the numbers.
    while ( low < ptr ) {
        char tmp = *low;
        *low++ = *ptr;
        *ptr-- = tmp;
    }
    return high;
}

void testPrintf()
{
#ifdef scroll_test
    unsigned int i;
#endif
    init_console(); 
    printf("Yes!! this is working.........%d....%d...%u...%u.....%ld...%ld.....%lu....%lu!!",6,-6,6,-6,6,-6,6,-6 );
    printf("I am eating %c%c %s...%x...\n",'a','n',"apple",0x7fff);

    
#ifdef scroll_test
    for(i=0;i<29;i++)
    {
    printf("this is the test to check is page scrolling works..\n");
    }
    printf("patnekar\n");

    for(i=0;i<37;i++)
    {
    printf("Stony Brook-");
    }
#endif
    //while(1);
//  printf("this is exception =%d",e/d);
    
    printf("ameya- %s %s %s","lalit-","aakrati-","shaoli");

  //printf("this is exception =%d",9/0);
}

